package com.myzcare.qa.util;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;

import com.myezcare.qa.base.TestBase;

public class Helper 
{
	public static String captureScreenshot(WebDriver driver)
	{
	File src=((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
	System.out.println("User dir");
	String screenshotPath=System.getProperty("user.dir")+"/Screenshot/Myzcare"+Helper.getCurrentDateTime()+ ".png";
	try	
		{
			FileHandler.copy(src,new File(screenshotPath));
			System.out.println("Screenshot Captured");
		}
		catch (IOException e)
		{	
		System.out.println("Unable to Captured Screenshot" + e.getMessage());
		}
		return screenshotPath;
	}
	
	public static String getCurrentDateTime()
	{
		DateFormat customFormat=new SimpleDateFormat("MM_dd_yyyy_HH_mm_ss");
		
		Date currentDate=new Date();
		
		return customFormat.format(currentDate);
	}
}
