/*import java.io.File;

import org.testng.Reporter;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.myzcare.qa.util.Helper;

package com.qa.ExtentReportListener;


import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.testng.IReporter;
import org.testng.IResultMap;
import org.testng.ISuite;
import org.testng.ISuiteResult;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.xml.XmlSuite;

import com.aventstack.extentreports.ExtentReporter;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;



public class ExtentReporterNG implements IReporter {
	public static ExtentReports report;
	public ExtentTest logger;
	public static ExtentHtmlReporter htmlReporter;
	@BeforeSuite
	public void setUpSuite()
	{	
		Reporter.log("Setting Up the report and Test is getting ready",true);
		ExtentHtmlReporter ext=new ExtentHtmlReporter(new File(System.getProperty("user.dir")+"/Report/Myzcare_"+Helper.getCurrentDateTime()+".html"));
		
		ext.config().setReportName("Production Environment-Alex");
		//ExtentHtmlReporter ext=new ExtentHtmlReporter("./Report/Myzcare.html");
		report=new ExtentReports();
		report.attachReporter(ext);
		//htmlReporter.config().setDocumentTitle("Test Environment");
		Reporter.log("Setting is done-Test can be started",true);
		
		
	}

		report.flush();
		extent.close();
	}

	private void buildTestNodes(IResultMap tests, LogStatus status) {
		ExtentTest test;

		if (tests.size() > 0) {
			for (ITestResult result : tests.getAllResults()) {
				test = extent.startTest(result.getMethod().getMethodName());

				test.setStartedTime(getTime(result.getStartMillis()));
				test.setEndedTime(getTime(result.getEndMillis()));

				for (String group : result.getMethod().getGroups())
					test.assignCategory(group);

				if (result.getThrowable() != null) {
					test.log(status, result.getThrowable());
				} else {
					test.log(status, "Alex Environment " + status.toString().toLowerCase()
							+ "ed");
				}

				extent.endTest(test);
			}
		}
	}

	private Date getTime(long millis) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTimeInMillis(millis);
		return calendar.getTime();
	
}*/