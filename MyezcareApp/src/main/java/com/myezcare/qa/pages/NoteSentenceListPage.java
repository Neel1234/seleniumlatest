package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class NoteSentenceListPage extends TestBase
{
	public NoteSentenceListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateNoteSentenceListPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[17]/a/span[1]")
	WebElement NoteSentence;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[17]/ul/li[2]/a/span")
	WebElement List;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[2]/button")
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]")
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/a")
	WebElement AddNoteSentence;
	
	@FindBy(xpath = "//*[@id=\"SearchNoteSentenceListPage_NoteSentenceTitle\"]")
	WebElement NoteSentenceTitle;
	
	@FindBy(xpath = "//*[@id=\"SearchNoteSentenceListPage_NoteSentenceDetails\"]")
	WebElement NoteSentenceDetails;
	
	@FindBy(xpath = "//*[@id=\"frmNoteSentenceList \"]/div/table/tbody/tr/td[4]/input")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmNoteSentenceList \"]/div/table/tbody/tr/td[4]/a")
	WebElement Reset;
	
	
	public void noteSentenceList(String noteTitle,String noteDetail, String active)throws InterruptedException
	{
		Refresh.click();
		Thread.sleep(7000);
		Select select1= new Select(driver.findElement(By.xpath("//*[@id=\\\"IsDeleted\\\"]")));
		select1.selectByVisibleText(active);
		NoteSentenceTitle.sendKeys(noteTitle);
		NoteSentenceDetails.sendKeys(noteDetail);
		Thread.sleep(7000);
		Search.click();
		Thread.sleep(7000);
		Reset.click();
	}
	
	
}
