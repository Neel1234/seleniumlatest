package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class DocumentManagementPage extends TestBase
{
	public DocumentManagementPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a" ) 
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[4]/a" )
	WebElement DocumentManagement;
	
	//Inside Page
	
	@FindBy(xpath = "//*[@id=\"Compliance_UserType\"]" )
	WebElement UserType;
	
	@FindBy(xpath = "//*[@id=\"Compliance_DocumentationType\"]" )
	WebElement DocumentationType;
	
	@FindBy(xpath = "//*[@id=\"Compliance_Type\"]" )
	WebElement Folder;
	
	@FindBy(xpath = "//*[@id=\"Compliance_DocumentName\"]" )
	WebElement Name;
	
	@FindBy(xpath = "//*[@id=\"frmCompliance\"]/div/div/div[5]/div/div/button" )
	WebElement Role;
	
	@FindBy(xpath = "//*[@id=\"Compliance_IsTimeBased\"]" )
	WebElement TimeBased;
	
	@FindBy(xpath = "//*[@id=\"frmCompliance\"]/div/div/div[7]/button" )
	WebElement MapForm;
	
	@FindBy(xpath = "//*[@id=\"frmCompliance\"]/div/div/div[8]/div/button" )
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmCompliance\"]/div/div/div[8]/div/a" )
	WebElement Cancel;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div/div/div/div[2]/div[1]/div[1]/div[1]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "//*[@id=\"SelectAllCompliance\"]" )
	WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id=\"UserType\"]" )
	WebElement UserType1;
	
	@FindBy(xpath = "//*[@id=\"SearchComplianceListPage_DocumentationType\"]" )
	WebElement DocumentationType1;
	
	@FindBy(xpath = "//*[@id=\"SearchComplianceListPage_Type\"]" )
	WebElement Type;
	
	@FindBy(xpath = "//*[@id=\"SearchComplianceListPage_DocumentName\"]" )
	WebElement Name1;
	
	@FindBy(xpath = "//*[@id=\"SearchComplianceListPage_IsTimeBased\"]" )
	WebElement IsTimeBased;
	
	@FindBy(xpath = "//*[@id=\"FrmComplianceList\"]/div/div/div/table/tbody/tr[1]/td[7]/input" )
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"FrmComplianceList\"]/div/div/div/table/tbody/tr[1]/td[7]/a" )
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"FrmComplianceList\"]/div/div/div/table/tbody/tr[2]/td[7]/a/i" )
	WebElement Edit;
	
	//Actions
	public static String validateDocumentManagementPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyDocumentManagementListLabel()
	{
		return DocumentManagement.isDisplayed();
	}
	public void selectMultipleselectDocumentManagement() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String Documentation_Type) throws Exception
	{
		Select type=new Select(DocumentationType1);
		type.selectByVisibleText(Documentation_Type);
		Thread.sleep(5000);
		Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
		
	}
	public void DocumentManagementForm(String user_type, String documentation_type, String folder, String name, 
			String role, String timeBased) throws InterruptedException
	{
		UserType.sendKeys(user_type);
		DocumentationType.sendKeys(documentation_type);
		Folder.sendKeys(folder);
		Name.sendKeys(name);
		Role.sendKeys(role);
		TimeBased.sendKeys(timeBased);
		Save.click();
		Thread.sleep(6000);
		Cancel.click();
	}
	public void validateResetBtn(String name, String timeBased) throws Exception
	{
		Name1.sendKeys(name);
		Thread.sleep(3000);
		IsTimeBased.sendKeys(timeBased);
		Thread.sleep(5000);
		Reset.click();			
	}
	public void validateEditBtn(String name) throws Exception 
	{
		Name1.sendKeys(name);
		Thread.sleep(5000);
		Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
