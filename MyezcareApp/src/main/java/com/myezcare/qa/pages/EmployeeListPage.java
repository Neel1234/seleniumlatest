package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class EmployeeListPage extends TestBase
{
	@FindBy(xpath="html/body/div[1]/div[6]/div[2]/div/div/div[1]/h3")
	WebElement EmployeeListLabel;
	
	@FindBy(xpath="//input[@id='SelectAllEmployee']")
	WebElement MultilpleSelectBtn;
	
	@FindBy(xpath="//select[@id='SearchEmployeeModel_RoleID']")
	WebElement SelectRole;
	
	@FindBy(xpath="//input[@type='submit' and @class='btn btn-default grey btn-xs float-left']")
	WebElement searchBtn;
	
	@FindBy(xpath="//*[@name='SearchEmployeeModel.IsDeleted']")
	WebElement activeDeletedDrop;
	
	@FindBy(xpath="//select[@ng-change='BulkScheduleModel()']")
	WebElement bulkSchedule;
	
	@FindBy(xpath="//button[@class='letter-space btn btn-sm green-jungle' and @name='AddEmployee']")
	WebElement addEmpBtn;
	
	@FindBy(xpath="//input[@id='SearchEmployeeModel_EmployeeUniqueID']")
	WebElement empID;
	
	@FindBy(xpath="//input[@id='SearchEmployeeModel_Name']")
	WebElement empName;
	
	@FindBy(xpath="//input[@id='SearchEmployeeModel_Email']")
	WebElement empEmail;
	
	@FindBy(xpath="//input[@id='SearchEmployeeModel_MobileNumber']")
	WebElement empMobile;
	
	@FindBy(xpath="//input[@id='SearchEmployeeModel_Address']")
	WebElement empAddress;
	
	@FindBy(xpath="//a[@data-ng-click='ResetSearchFilter()' and @class='btn btn-default grey btn-xs']")
	WebElement resetBtn;
	
	@FindBy(xpath="//button[@data-ng-click='EmployeeEditModel(employee.EncryptedEmployeeID)' and @name='Edit']//i[@class='fa fa-edit']")
	WebElement editBtn;

	// Initializing the Page Objects:
	public EmployeeListPage() 
	{
		PageFactory.initElements(driver, this);
		
	}
	
	//Actions
		public static String validateEmployeeListpageTitle(){
			return driver.getTitle();
		}
		public boolean verifyEmployeeListLabel()
		{
			return EmployeeListLabel.isDisplayed();
		}
		public void selectMultipleselectEmp() throws Exception
		{
			Thread.sleep(5000);
			MultilpleSelectBtn.click();
		}
		public void verifySearchBtn(String Emp_role) throws Exception
		{
			Select role=new Select(SelectRole);
			role.selectByVisibleText(Emp_role);
			Thread.sleep(5000);
			searchBtn.click();
		}
		public void validateDeleteActiveRecord(String record)
		{
			Select actdel=new Select(activeDeletedDrop);
			actdel.selectByVisibleText(record);
			
		}
		public void selectBulkschDropdown(String bulk) throws Exception
		{
			Select bulkSch=new Select(bulkSchedule);
			Thread.sleep(5000);
			bulkSch.selectByVisibleText(bulk);
			
		}
		public EmployeeAddPage validateAddEmpBtn() throws Exception
		{
			Thread.sleep(5000);
			addEmpBtn.click();
			return new EmployeeAddPage();
			
		}
		public void validateResetBtn(String empId,String name,String emailId,String mobile,String address) throws Exception
		{
			empID.sendKeys(empId);
			Thread.sleep(3000);
			empName.sendKeys(name);
			Thread.sleep(3000);
			empEmail.sendKeys(emailId);
			Thread.sleep(3000);
			empMobile.sendKeys(mobile);
			Thread.sleep(3000);
			empAddress.sendKeys(address);
			Thread.sleep(5000);
			resetBtn.click();			
		}
		public void validaetEditBtn(String Empname) throws Exception 
		{
			empName.sendKeys(Empname);
			Thread.sleep(5000);
			searchBtn.click();
			Thread.sleep(5000);
			editBtn.click();
			
			
			
		}
		
}
