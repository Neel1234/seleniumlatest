package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class ScheduleMasterPage extends TestBase 
{
	public ScheduleMasterPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateScheduleMasterTitle()
	{
		return driver.getTitle();
	}
}
