package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class DropdownEditorPage extends TestBase
{
	public DropdownEditorPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[6]/a" )
	WebElement DropdownEditor;
	
	//Inside Page
	
	@FindBy(xpath = "//*[@id=\"DDMaster_ItemType\"]" )
	WebElement ItemType;
	
	@FindBy(xpath = "//*[@id=\"DDMaster_Title\"]" )
	WebElement Title;
	
	@FindBy(xpath = "//*[@id=\"frmDDMaster\"]/div/div/div[4]/div/button" )
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmDDMaster\"]/div/div/div[4]/div/a" )
	WebElement Cancel;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div/div/div/div/div/div/div[1]/div[1]/div[1]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "//*[@id=\"SelectAllDDMaster\"]" )
	WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id=\"SearchDDMasterListPage_ItemType\"]" )
	WebElement ItemType1;
	
	@FindBy(xpath = "//*[@id=\"SearchDDMasterListPage_Title\"]" )
	WebElement Title1;
	
	@FindBy(xpath = "//*[@id=\"SearchDDMasterListPage_Value\"]" )
	WebElement Value;
	
	@FindBy(xpath = "//*[@id=\"FrmDDMasterList\"]/div/div/div/table/tbody/tr[1]/td[5]/input" )
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"FrmDDMasterList\"]/div/div/div/table/tbody/tr[1]/td[5]/a" )
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"FrmDDMasterList\"]/div/div/div/table/tbody/tr[2]/td[5]/a[1]/i" )
	WebElement Edit;
	
	//Actions
	public static String validateDropdownEditorPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyDropdownEditorListLabel()
	{
		return DropdownEditor.isDisplayed();
	}
	public void selectMultipleselectDropdownEditor() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String Item_Type) throws Exception
	{
		Select type=new Select(ItemType1);
		type.selectByVisibleText(Item_Type);
		Thread.sleep(5000);
		Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
		
	}
	public void DropdownnForm(String item_type, String title) throws InterruptedException
	{
		ItemType.sendKeys(item_type);
		Title.sendKeys(title);
		Save.click();
		Thread.sleep(6000);
		Cancel.click();
	}
	public void validateResetBtn(String title, String value) throws Exception
	{
		Title1.sendKeys(title);
		Thread.sleep(3000);
		Value.sendKeys(value);
		Thread.sleep(5000);
		Reset.click();			
	}
	public void validateEditBtn(String title) throws Exception 
	{
		Title1.sendKeys(title);
		Thread.sleep(5000);
		Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
