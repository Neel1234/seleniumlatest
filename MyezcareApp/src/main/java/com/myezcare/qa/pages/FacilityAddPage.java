package com.myezcare.qa.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class FacilityAddPage extends TestBase
{
	
	public FacilityAddPage()
	{
		//PageFactory.initElements(driver, this);
		
			
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[3]/a/span[1]")
	WebElement FacilityHouse;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[3]/ul/li[1]/a")
	WebElement Add;
	
	/* In the form field */
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/h3/input[2]")
	WebElement Cancel1;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/h3/input[1]")
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_FacilityName\"]")
	WebElement FacilityName;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_Address\"]")
	WebElement Address;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_City\"]")
	WebElement City;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_State\"]")
	WebElement state;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_ZipCode\"]")
	WebElement ZipCode;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_Phone\"]")
	WebElement Phone;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_Cell\"]")
	WebElement Cell ;
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_RegionID\"]")
	WebElement Region;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_County\"]")
	WebElement Country;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_GSA\"]")
	WebElement GSA;
	
	@FindBy(xpath = "//*[@id=\"token-input-ProductTagTkn\"]")
	WebElement Equipment;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_BadCapacity\"]")
	WebElement HouseCapacity;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_PrivateRoomCount\"]")
	WebElement PrivateRoom;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_SiteType\"]")
	WebElement SiteType;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_ProviderType\"]")
	WebElement ProviderType;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_Licensure\"]")
	WebElement Licensure;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_LicensureRenewalDate\"]")
	WebElement LicensureRenewalDate;
	
	@FindBy(xpath = "//*[@id=\"FacilityHouseModel_FirePermitDate\"]")
	WebElement FirePermitDate;
	
	@FindBy(xpath = "//*[@id=\"NPI\"]")
	WebElement NPI;
	
	@FindBy(xpath = "//*[@id=\"AHCCCS_ID\"]")
	WebElement AHCCCS_ID;
	
	@FindBy(xpath = "//*[@id=\"EIN\"]")
	WebElement EIN;
	
	@FindBy(xpath = "//*[@id=\"frmaddFacilityId\"]/div/div/div[3]/input[1]")
	WebElement Submit;
	
	@FindBy(xpath = "//*[@id=\"frmaddFacilityId\"]/div/div/div[3]/input[2]")
	WebElement Cancel;
	
	/*public static String validateFacilityAddPageTitle()
	{
		return driver.getTitle();
	}*/
	
	public void facilityAdd(String fName, String address, String city, String zip, String phone, String cell, String country,
			String gsa, String equipment, String houseCapacity, String privateRoom, String siteType, String providerType,
			String licensure, String licensureRenualDate, String firePermitDate, String npi, String AHCCCSId, String ein)throws InterruptedException
	{
		FacilityName.sendKeys(fName);
		Address.sendKeys(address);
		City.sendKeys(city);
		ZipCode.sendKeys(zip);
		Phone.sendKeys(phone);
		Cell.sendKeys(cell);
		Country.sendKeys(country);
		GSA.sendKeys(gsa);
		Equipment.sendKeys(equipment);
		HouseCapacity.sendKeys(houseCapacity);
		PrivateRoom.sendKeys(privateRoom);
		SiteType.sendKeys(siteType);
		ProviderType.sendKeys(providerType);
		Licensure.sendKeys(licensure);
		LicensureRenewalDate.sendKeys(licensureRenualDate);
		FirePermitDate.sendKeys(firePermitDate);
		NPI.sendKeys(npi);
		AHCCCS_ID.sendKeys(AHCCCSId);
		EIN.sendKeys(ein);
	}
}
