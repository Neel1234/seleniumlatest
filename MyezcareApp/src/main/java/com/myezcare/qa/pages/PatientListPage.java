package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PatientListPage extends TestBase 
{

	@FindBy(xpath="/html/body/div[1]/div[5]/div[2]/div/div[1]/div[1]/h3")
	WebElement patientListLabel;
	public PatientListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public boolean verifyPatientListLabel()
	{
		return patientListLabel.isDisplayed();
	}
	
}
