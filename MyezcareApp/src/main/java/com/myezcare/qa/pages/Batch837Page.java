package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class Batch837Page extends TestBase
{
	public Batch837Page()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateBatch837PageTitle()
	{
		return driver.getTitle();
	}
}
