package com.myezcare.qa.pages;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class HomePage extends TestBase
{
	@FindBy(xpath="/html/body/div[1]/div[1]/div/div[1]/a/img")
	WebElement myzlogo;
	
	@FindBy(xpath="//a[@href='https://support.myezcare.com']")
	WebElement supportLink;
	
	@FindBy(xpath="//img[@src='/Assets/images/Notification.png']")
	WebElement notificationLink;
//============================Admin Module Page Factory===========================
	@FindBy(xpath="//*[@class='username username-hide-on-mobile' and contains(text(),'Admin')]")
	WebElement adminLink;
	
	@FindBy(xpath="//li[@data-selectmenu='SMI_EditProfile']")
	WebElement adminMyprofileLink;
	
	@FindBy(xpath="//a[@href='/hc/security/rolepermission']")
	WebElement adminRolesPermissionLink;
	
	@FindBy(xpath="//a[@href='/hc/setting/organizationsetting']")
	WebElement adminOrganizationSettingLink;
	
	@FindBy(xpath="//a[@href='/hc/invoice/CompanyClientInvoice']")
	WebElement adminBillingLink;

	public void adminMouseOver() throws InterruptedException
	{
		Actions action = new Actions(driver);
		action.moveToElement(adminLink).build().perform();
		Thread.sleep(6000);
		adminLink.click();
		Thread.sleep(6000);
	}
//====================Employee Module Page Factory=================
	
	//@FindBy(xpath="//a[@href='javascript:;' and @class='nav-link nav-toggle']//span[@class='title' and text()='Employee']")
	@FindBy(xpath="/html/body/div[1]/div[6]/div[1]/div/ul/li[4]/a/span[1]")
	WebElement employeeLink;
	
	@FindBy(xpath="//*[@href='/hc/employee/employeelist']")
	WebElement employeeListLink;
	
	
	@FindBy(xpath="//*[@href='/hc/employee/addemployee']")
	WebElement employeeAddLink;
	
	
	@FindBy(xpath="//a[@href='/hc/employee/employeecalender']")
	WebElement employeeCalendarLink;
	
	@FindBy(xpath="//a[@href='/hc/employee/employeetimeslots']")
	WebElement employeeScheduleLink;
	
	@FindBy(xpath="//a[@href='/hc/employee/employeedayoff']")
	WebElement employeePTOLink;
	
	public void employeeMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(employeeLink).build().perform();
		Thread.sleep(6000);
		employeeLink.click();
		Thread.sleep(5000);
		
	}
//=====================Patient module page Factory==================
	@FindBy(xpath="//*[contains(text(),'Patient Intake')]")
	WebElement patientLink;
	
	@FindBy(xpath="//a[@href='/hc/referral/addreferral/']")
	WebElement patientAddLink;
	
	@FindBy(xpath="//a[@href='/hc/referral/referrallist/']")
	WebElement patientListLink;
	
	@FindBy(xpath="//a[@href='/hc/referral/referralcalender']")
	WebElement patientCalendarLink;
	
	@FindBy(xpath="//a[@href='/hc/referral/referraltimeslots']")
	WebElement patientScheduleLink;
	
	public void patientMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(patientLink).build().perform();
		Thread.sleep(6000);
		patientLink.click();
		Thread.sleep(3000);
		
	}
//==============Scheduling Module page Factory============================
	@FindBy(xpath="//*[contains(text(),'Scheduling')]")
	WebElement schedulingLink;
	
	@FindBy(xpath="//*[contains(text(),'Schedule Master')]")
	WebElement schedulemMsterLink;
	
	@FindBy(xpath="//*[contains(text(),'Schedule Log')]")
	WebElement ScheduleLogLink;
	
	@FindBy(xpath="//*[contains(text(),'Pending Schedules')]")
	WebElement pendingScheduleLink;
	
	
	public void SchedulingMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(schedulingLink).build().perform();
		Thread.sleep(7000);
		schedulingLink.click();
		Thread.sleep(5000);
	}
//======================Claim Processing Page Factory=====================================
	@FindBy(xpath="//*[contains(text(),'Claim Processing')]")
	WebElement claimProcessingLink;
	
	@FindBy(xpath="//*[contains(text(),'Batch 837')]")
	WebElement batch837Link;
	
	@FindBy(xpath="//*[contains(text(),'Reconcile - 835 / EOB')]")
	WebElement reconcile835Link;
	
	@FindBy(xpath="//*[contains(text(),'EDI File Logs')]")
	WebElement ediFileLogLink;
	
	@FindBy(xpath="//*[contains(text(),'Eligibility - 270 / 271')]")
	WebElement eligibility270Link;
	
	@FindBy(xpath="//*[contains(text(),'Manage Claims')]")
	WebElement manageClaimsLink;
	
	@FindBy(xpath="//*[contains(text(),'Manage 835')]")
	WebElement manage835Link;
	
	@FindBy(xpath="//*[contains(text(),'Upload 835')]")
	WebElement upload835Link;
	
	@FindBy(xpath="//*[contains(text(),'Latest ERA')]")
	WebElement latestERALink;
	
	public void Manage835MouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(manage835Link).build().perform();
		Thread.sleep(7000);
		manage835Link.click();
		Thread.sleep(5000);
	}
	

	public void ClaimProcessingMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(claimProcessingLink).build().perform();
		Thread.sleep(5000);
		claimProcessingLink.click();
		Thread.sleep(5000);
	}
//=======================Message module Page Factory=============================
	@FindBy(xpath="//*[contains(text(),'Received Messages')]")
	WebElement receivedMessageLink;
	
	@FindBy(xpath="//*[contains(text(),'Sent Messages')]")
	WebElement sentMessageLink;
	
	@FindBy(xpath="//*[contains(text(),'Group SMS')]")
	WebElement groupMessageLink;
	
	@FindBy(xpath="//*[contains(text(),'Broadcast Notifications')]")
	WebElement broadcastNotificationLink;
	
	@FindBy(xpath="//a[@class='nav-link nav-toggle']//i[@class='fa fa-envelope']")
	WebElement MessageLink;
	
	public void MessageMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(MessageLink).build().perform();
		Thread.sleep(5000);
		MessageLink.click();
		Thread.sleep(5000);
	}
//========================Reports Page Factory========================================
	@FindBy(xpath="//a[@class='nav-link nav-toggle']//i[@class='fa fa-bar-chart']")
	WebElement ReportsLink;
	
	@FindBy(xpath="//*[contains(text(),'Employee Billing Report')]")
	WebElement EmployeeBillingReportLink;
	
	@FindBy(xpath="//*[contains(text(),'Employee Visit Reports')]")
	WebElement EmployeeVisitReportLink;
	
	@FindBy(xpath="//a[@href='/hc/invoice/invoicelist']")
	WebElement InvoiceLink;
	
	@FindBy(xpath="//a[@href='/hc/report/DMAS_90Form/']")
	WebElement DMAS90FormLink;
	
	@FindBy(xpath="//a[@href='/hc/report/DMAS_90Form/']")
	WebElement WeeklyTimeSheetLink;
	
	@FindBy(xpath="//a[@href='/hc/report/reportmaster']")
	WebElement ReportLink;
	
	
	public void ReportsMouseOver() throws InterruptedException
	{
		Thread.sleep(7000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(ReportsLink).build().perform();
		Thread.sleep(6000);
		ReportsLink.click();
		Thread.sleep(5000);
	}
//=====================Setting Page Factory===================
	@FindBy(xpath="//*[ text()='Settings']")
	WebElement SettingLink;
	
	
	@FindBy(xpath="//*[text()='Agency']")
	WebElement AgencyLink;
	
	@FindBy(xpath="//a[@href='/hc/agency/addagency']")
	WebElement AgencyAddLink;
	
	@FindBy(xpath="//a[@href='/hc/agency/agencylist']")
	WebElement AgencyListLink;
	
	@FindBy(xpath="//*[contains(text(),'Document Management')]")
	WebElement DocumentManagementLink;
	
	@FindBy(xpath="//*[contains(text(),'DX Code ')]")
	WebElement DXCodeLink;
	
	@FindBy(xpath="//a[@href='/hc/dxcode/adddxcode/']")
	WebElement DXCodeAddLink;
	
	@FindBy(xpath="//a[@href='/hc/dxcode/dxcodelist']")
	WebElement DXCodeListLink;
	
	@FindBy(xpath="//*[contains(text(),'Facility House')]")
	WebElement FacilityHouseLink;
	
	@FindBy(xpath="/hc/facilityhouse/addfacilityhouse/")
	WebElement FacilityHouseAddLink;
	
	@FindBy(xpath="//a[@href='/hc/facilityhouse/facilityhouselist/']")
	WebElement FacilityHouseListLink;
	
	@FindBy(xpath="//*[contains(text(),'Dropdown Editor')]")
	WebElement DropDowneditorLink;
	
	@FindBy(xpath="//*[contains(text(),'Organization Forms')]")
	WebElement OrganizationFormsLink;
	
	@FindBy(xpath="//*[contains(text(),'Payor')]")
	WebElement PayorLink;
	
	@FindBy(xpath="//a[@href='/hc/payor/addpayor/']")
	WebElement PayorAddLink;
	
	@FindBy(xpath="//a[@href='/hc/payor/payorlist/']")
	WebElement PayorListLink;
	
	@FindBy(xpath="//*[contains(text(),'Physician')]")
	WebElement PhysicianLink;
	
	@FindBy(xpath="//a[@href='/hc/physician/addphysician']")
	WebElement PhysicianAddLink;
	
	@FindBy(xpath="//a[@href='/hc/physician/physicianlist']")
	WebElement PhysicianListLink;
	
	@FindBy(xpath="//*[contains(text(),'Preference/Skill')]")
	WebElement PreferenceSkillLink;
	
	@FindBy(xpath="//a[@href='/hc/preference/addpreference/']")
	WebElement PreferenceSkillAddLink;
	
	@FindBy(xpath="//a[@href='/hc/preference/preferencelist']")
	WebElement PreferenceSkillListLink;
	
	@FindBy(xpath="//*[contains(text(),'Service Code')]")
	WebElement ServiceCodeLink;
	
	@FindBy(xpath="//a[@href='/hc/servicecode/addservicecode/']")
	WebElement ServiceCodeAddLink;
	
	@FindBy(xpath="//a[@href='/hc/servicecode/servicecodelist/']")
	WebElement ServiceCodeListLink;
	
	@FindBy(xpath="//*[contains(text(),'Visit Task')]")
	WebElement VisitTaskLink;
	
	@FindBy(xpath="//a[@href='/hc/visittask/addvisittask/']")
	WebElement VisitTaskAddLink;
	
	@FindBy(xpath="//a[@href='/hc/visittask/visittasklist']")
	WebElement VisitTaskListLink;
	
	@FindBy(xpath="//a[@class='nav-link nav-toggle']//*[contains(text(),'Note Sentence')]")
	WebElement NoteSentenceLink;
	
	@FindBy(xpath="//a[@href='/notesentence/addnotesentence/']")
	WebElement NoteSentenceAddLink;
	
	@FindBy(xpath="//a[@href='/notesentence/notesentencelist']")
	WebElement NoteSentenceListLink;
	
	@FindBy(xpath="//a[@class='nav-link nav-toggle']//*[contains(text(),'Case Manager')]")
	WebElement CaseManagerLink;
	
	
	@FindBy(xpath="//a[@href='/casemanager/addcasemanager/']")
	WebElement CaseManagerAddLink;
	
	@FindBy(xpath="//a[@href='/casemanager/casemanagerlist']")
	WebElement CaseManagerListLink;
	
	@FindBy(xpath="//img[@src='/Assets/images/notes.png']")
	WebElement CaptureCallLink;
	
	@FindBy(xpath="//a[@data-ng-click='CallCaptureModel(1)']")
	WebElement CaptureCallAddLink;
	
	@FindBy(xpath="//a[@data-ng-click='CallCaptureModel(2)']")
	WebElement CaptureCallListLink;
	
	
	/*============================Email Template Link========================*/
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[12]/ul/li[18]/a/span[1]")
	WebElement EmailTemplateLink;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[12]/ul/li[18]/ul/li[1]/a/span")
	WebElement EmailTemplateAddLink;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[12]/ul/li[18]/ul/li[2]/a/span")
	WebElement EmailTemplateListLink;
	
	
	public void EmailTemplateMouseOver()throws InterruptedException
	{
		Thread.sleep(7000);
		Actions action = new Actions(driver);
		action.moveToElement(EmailTemplateLink).build().perform();
		Thread.sleep(7000);
		EmailTemplateLink.click();
		Thread.sleep(8000);
	}

	public void CaptureCallMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(CaptureCallLink).build().perform();
		Thread.sleep(5000);
		CaptureCallLink.click();
		Thread.sleep(5000);
	}
	
	public void NoteSentenceMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(NoteSentenceLink).build().perform();
		Thread.sleep(5000);
		NoteSentenceLink.click();
		Thread.sleep(5000);
	}
	
	public void CaseManagerMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(CaseManagerLink).build().perform();
		Thread.sleep(5000);
		CaseManagerLink.click();
		Thread.sleep(5000);
	}
	
	public void VisitTaskMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(VisitTaskLink).build().perform();
		Thread.sleep(5000);
		VisitTaskLink.click();
		Thread.sleep(5000);
	}
	
	public void ServiceCodeMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(ServiceCodeLink).build().perform();
		Thread.sleep(5000);
		ServiceCodeLink.click();
		Thread.sleep(5000);
	}
	
	public void PreferenceSkillMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(PreferenceSkillLink).build().perform();
		Thread.sleep(5000);
		PreferenceSkillLink.click();
		Thread.sleep(5000);
	}
	
	public void PayourMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(PayorLink).build().perform();
		Thread.sleep(5000);
		PayorLink.click();
		Thread.sleep(5000);
	}
	public void PhysicianMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(PhysicianLink).build().perform();
		Thread.sleep(5000);
		PhysicianLink.click();
		Thread.sleep(5000);
	}
	
	public void AgencyMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(AgencyLink).build().perform();
		Thread.sleep(5000);
		AgencyLink.click();
		Thread.sleep(5000);
	}
	public void DXCodeMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(DXCodeLink).build().perform();
		Thread.sleep(5000);
		DXCodeLink.click();
		Thread.sleep(5000);
	}
	public void FacilityHouseMouseOver() throws InterruptedException
	{
		Thread.sleep(8000);
		Actions action = new Actions(driver);
		action.moveToElement(FacilityHouseLink).build().perform();
		Thread.sleep(5000);
		FacilityHouseLink.click();
		Thread.sleep(5000);
	}
	
	public void SettingMouseOver() throws InterruptedException
	{
		
		Thread.sleep(8000);
		((JavascriptExecutor)driver).executeScript("scroll(0,-400)");
		Actions action = new Actions(driver);
		action.moveToElement(SettingLink).build().perform();
		Thread.sleep(5000);
		SettingLink.click();
		Thread.sleep(5000);
	}
	// Initializing the Page Objects:
		public HomePage() 
		{
			PageFactory.initElements(driver, this);
		}
		
		public boolean validatemyzcarelogo()
		{
			return myzlogo.isDisplayed();
		}
		
		public boolean validatesupportlink()
		{
			return supportLink.isDisplayed();
		}
		
		public boolean validatenotificationLink()
		{
			return notificationLink.isDisplayed();
		}
//==============================Admin====================================		
		public MyprofilePage clickOnAdminMyprofileLink() throws InterruptedException
		{
			adminMouseOver();
			adminMyprofileLink.click();
			return new MyprofilePage();
		}
		public RolesAndPermissionPage clickOnRolePermissionLink() throws InterruptedException
		{
			adminMouseOver();
			adminRolesPermissionLink.click();
			return new RolesAndPermissionPage();
		}
		public OrganizationSettingPage clickOnOrganizationSettingLink() throws InterruptedException
		{
			adminMouseOver();
			adminOrganizationSettingLink.click();
			return new OrganizationSettingPage();
		}
		public BillingPage clickOnBillingLink() throws InterruptedException
		{
			adminMouseOver();
			adminBillingLink.click();
			return new BillingPage();
		}
//=========================Employee Module===================================		
		
		public EmployeeAddPage clickOnEmployeeAddLink() throws InterruptedException 
		{
			employeeMouseOver();
			employeeAddLink.click();
			return new EmployeeAddPage();
		}
		public EmployeeListPage clickOnEmployeeListLink() throws InterruptedException 
		{
			employeeMouseOver();
			employeeListLink.click();
			return new EmployeeListPage();
		}
		public EmployeeCalendarPage clickOnEmployeeCalendarLink() throws InterruptedException 
		{
			employeeMouseOver();
			employeeCalendarLink.click();
			return new EmployeeCalendarPage();
		}
		public EmployeeSchedulePage clickOnEmployeeScheduleLink() throws InterruptedException 
		{
			employeeMouseOver();
			employeeScheduleLink.click();
			return new EmployeeSchedulePage();
		}
		public EmployeePTOPage clickOnEmployeePTOLink() throws InterruptedException 
		{
			employeeMouseOver();
			employeePTOLink.click();
			return new EmployeePTOPage();
		}
//======================================Patient Module=====================
		
		public PatientAddPage clickOnPatientAddLink() throws InterruptedException 
		{
			patientMouseOver();
			patientAddLink.click();
			return new PatientAddPage();
		}
		public PatientListPage clickOnPatientListLink() throws InterruptedException 
		{
			patientMouseOver();
			patientListLink.click();
			return new PatientListPage();
		}
		public PatientCalendarPage clickOnPatientCalendarLink() throws InterruptedException 
		{
			patientMouseOver();
			patientCalendarLink.click();
			return new PatientCalendarPage();
		}
		public PatientSchedulePage clickOnPatientScheduleLink() throws InterruptedException 
		{
			patientMouseOver();
			patientScheduleLink.click();
			return new PatientSchedulePage();
		}
		
//==========================Email Template Module================================
		
		public EmailTemplateAddPage clickOnEmailTempalteAddLink()throws InterruptedException
		{
			EmailTemplateMouseOver();
			EmailTemplateAddLink.click();
			return new EmailTemplateAddPage();
		}
		
		public EmailTemplateListPage clickOnEmailTemplateListLink()throws InterruptedException
		{
			EmailTemplateMouseOver();
			EmailTemplateListLink.click();
			return new EmailTemplateListPage();
		}
		
//==========================Scheduling Module================================
		public ScheduleMasterPage clickOnSchedulingMasterLink() throws InterruptedException 
		{
			SchedulingMouseOver();
			schedulemMsterLink.click();
			return new ScheduleMasterPage();
		}
		public ScheduleLogPage clickOnScheduleLogLink() throws InterruptedException 
		{
			SchedulingMouseOver();
			ScheduleLogLink.click();
			return new ScheduleLogPage();
		}
		public PendingSchedulePage clickOnPendingScheduleLink() throws InterruptedException 
		{
			SchedulingMouseOver();
			pendingScheduleLink.click();
			return new PendingSchedulePage();
		}
		
//================================Claim Processing=================
		public Batch837Page clickonBatch837Link() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			batch837Link.click();
			return new Batch837Page();
		}
		public Upload835Page clickonUpload835Link() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			Manage835MouseOver();
			upload835Link.click();
			return new Upload835Page();
		}
		public LatestERAPage clickonLatestERALink() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			Manage835MouseOver();
			latestERALink.click();
			return new LatestERAPage();
		}
		public Reconcile835Page clickonReconcile835Link() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			reconcile835Link.click();
			return new Reconcile835Page();
		}
		public EDIFileLogsPage clickonEdiFileLogsLink() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			ediFileLogLink.click();
			return new EDIFileLogsPage();
		}
		public Eligibility270Page clickonEligibility270PageLink() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			Thread.sleep(4000);
			eligibility270Link.click();
			return new Eligibility270Page();
		}
		public ManageClaimsPage clickonManageClaimsPageLink() throws InterruptedException 
		{
			ClaimProcessingMouseOver();
			manageClaimsLink.click();
			return new ManageClaimsPage();
		}
//=======================================Message======================
		public ReceivedMessagePage clickonReceivedMessageLink() throws InterruptedException 
		{
			MessageMouseOver();
			receivedMessageLink.click();
			return new ReceivedMessagePage();
		}
		public SentMessagePage clickonSentMessageLink() throws InterruptedException 
		{
			MessageMouseOver();
			sentMessageLink.click();
			return new SentMessagePage();
		}
		public GroupSMSPage clickonGroupSMSLink() throws InterruptedException 
		{
			MessageMouseOver();
			groupMessageLink.click();
			return new GroupSMSPage();
		}
		public BroadcastNotoficationPage clickonBroadcastNotoficationLink() throws InterruptedException 
		{
			MessageMouseOver();
			broadcastNotificationLink.click();
			return new BroadcastNotoficationPage();
		}
//=======================Reports=========================
		public EmployeeBillingreportPage clickonEmployeeBillingreportLink() throws InterruptedException 
		{
			ReportsMouseOver();
			EmployeeBillingReportLink.click();
			return new EmployeeBillingreportPage();
		}
		public EmployeeVisitreportPage clickonEmployeeVisitreportLink() throws InterruptedException 
		{
			ReportsMouseOver();
			EmployeeVisitReportLink.click();
			return new EmployeeVisitreportPage();
		}
		public InvoicePage clickonInvoiceoLink() throws InterruptedException 
		{
			ReportsMouseOver();
			InvoiceLink.click();
			return new InvoicePage();
		}
		public DMAS90FormPage clickonDMAS90FormLink() throws InterruptedException 
		{
			ReportsMouseOver();
			DMAS90FormLink.click();
			return new DMAS90FormPage();
		}
		public WeeklyTimeSheetPage clickonWeeklyTimeSheetLink() throws InterruptedException 
		{
			ReportsMouseOver();
			WeeklyTimeSheetLink.click();
			return new WeeklyTimeSheetPage();
		}
		public ReportPage clickonReportLink() throws InterruptedException 
		{
			ReportsMouseOver();
			ReportLink.click();
			return new ReportPage();
		}
//========================Setting=================
		public AgencyAddPage clickonAgencyAddLink() throws InterruptedException 
		{
			SettingMouseOver();
			AgencyMouseOver();
			AgencyAddLink.click();
			return new AgencyAddPage();
		}
		public AgencyListPage clickonAgencyListLink() throws InterruptedException 
		{
			SettingMouseOver();
			AgencyMouseOver();
			AgencyListLink.click();
			return new AgencyListPage();
		}
		public DocumentManagementPage clickonDocumentManagementLink() throws InterruptedException 
		{
			SettingMouseOver();
			
			DocumentManagementLink.click();
			return new DocumentManagementPage();
		}
		public DXCodeAddPage clickonDXcodeAddLink() throws InterruptedException 
		{
			SettingMouseOver();
			DXCodeMouseOver();
			DXCodeAddLink.click();
			return new DXCodeAddPage();
		}
		public DXCodeListPage clickonDXcodeListLink() throws InterruptedException 
		{
			SettingMouseOver();
			DXCodeMouseOver();
			DXCodeListLink.click();
			return new DXCodeListPage();
		}
		public FacilityAddPage clickonFacilityAddLink() throws InterruptedException 
		{
			SettingMouseOver();
			FacilityHouseMouseOver();
			FacilityHouseAddLink.click();
			return new FacilityAddPage();
		}
		public FacilityListPage clickonFacilityListLink() throws InterruptedException 
		{
			SettingMouseOver();
			FacilityHouseMouseOver();
			FacilityHouseListLink.click();
			return new FacilityListPage();
		}
		public DropdownEditorPage clickonDropdownEditorLink() throws InterruptedException 
		{
			SettingMouseOver();
			DropDowneditorLink.click();
			return new DropdownEditorPage();
		}
		public OrganizationFormsPage clickonOrganiztionFormsLink() throws InterruptedException 
		{
			SettingMouseOver();
			OrganizationFormsLink.click();
			return new OrganizationFormsPage();
		}
		public PayorAddPage clickonPayorAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PayourMouseOver();
			PayorAddLink.click();
			return new PayorAddPage();
		}
		public PayorListPage clickonPayorListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PayourMouseOver();
			PayorListLink.click();
			return new PayorListPage();
		}
		public PhysicianAddPage clickonPhysicianAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PhysicianMouseOver();
			PhysicianAddLink.click();
			return new PhysicianAddPage();
		}
		public PhysicianListPage clickonPhysicianListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PhysicianMouseOver();
			PhysicianListLink.click();
			return new PhysicianListPage();
		}
		public PreferenceAddPage clickonPreferenceAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PreferenceSkillMouseOver();
			PreferenceSkillAddLink.click();
			return new PreferenceAddPage();
		}
		public PreferenceListPage clickonPreferenceListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			PreferenceSkillMouseOver();
			PreferenceSkillListLink.click();
			return new PreferenceListPage();
		}
		public ServiceCodeAddPage clickonServiceCodeAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			ServiceCodeMouseOver();
			ServiceCodeAddLink.click();
			return new ServiceCodeAddPage();
		}
		public ServiceCodeListPage clickonServiceCodeListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			ServiceCodeMouseOver();
			ServiceCodeListLink.click();
			return new ServiceCodeListPage();
		}
		public VisitTaskAddPage clickonVisitTaskAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			VisitTaskMouseOver();
			VisitTaskAddLink.click();
			return new VisitTaskAddPage();
		}
		public VisitTaskListPage clickonVisitTaskListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			VisitTaskMouseOver();
			VisitTaskListLink.click();
			return new VisitTaskListPage();
		}
		public NoteSentenceAddPage clickonNoteSentenceAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			NoteSentenceMouseOver();
			NoteSentenceAddLink.click();
			return new NoteSentenceAddPage();
		}
		public NoteSentenceListPage clickonNoteSentenceListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			NoteSentenceMouseOver();
			NoteSentenceListLink.click();
			return new NoteSentenceListPage();
		}
		public CaseManagerAddPage clickonCaseManagerAddPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			CaseManagerMouseOver();
			CaseManagerAddLink.click();
			return new CaseManagerAddPage();
		}
		public CaseManagerListPage clickonCaseManagerListPageLink() throws InterruptedException 
		{
			SettingMouseOver();
			CaseManagerMouseOver();
			CaseManagerListLink.click();
			return new CaseManagerListPage();
		}
		public CaptureCallAddPage clickonCaptureCallAddPageLink() throws InterruptedException 
		{
			//SettingMouseOver();
			CaptureCallMouseOver();
			CaptureCallAddLink.click();
			return new CaptureCallAddPage();
		}
		public CaptureCallListPage clickonCaptureCallListPageLink() throws InterruptedException 
		{
			//SettingMouseOver();
			CaptureCallMouseOver();
			CaptureCallListLink.click();
			return new CaptureCallListPage();
		}
}
