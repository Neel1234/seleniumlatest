package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class DXCodeListPage extends TestBase
{
	public DXCodeListPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[5]/a" )
	WebElement DXCode;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[5]/ul/li[2]/a" )
	WebElement List;
	
	//Form Section
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[1]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[3]" )
	WebElement DXCode1;
	
	@FindBy(xpath = "//*[@id=\"uniform-SelectAllDxCode\"]" )
	WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id=\"SearchDxCodeListPage_DXCodeName\"]" )
	WebElement DXCodeType;
	
	@FindBy(xpath = "//*[@id=\"SearchDxCodeListPage_DXCodeName\"]" )
	WebElement DXCode2;
	
	@FindBy(xpath = "//*[@id=\"SearchDxCodeListPage_Description\"]" )
	WebElement Description;
	
	@FindBy(xpath = "//*[@id=\"frmDxCodeList \"]/div/table/tbody/tr[1]/td[5]/input" )
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmDxCodeList \"]/div/table/tbody/tr[1]/td[5]/a" )
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"frmDxCodeList \"]/div/table/tbody/tr[2]/td[5]/a[1]/i" )
	WebElement Edit;
	
	//Actions
	public static String validateDXCodeListPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyDXCodeListLabel()
	{
		return List.isDisplayed();
	}
	public void selectMultipleselectDXCode() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String DXCode_Type) throws Exception
	{
		Select type=new Select(DXCodeType);
		type.selectByVisibleText(DXCode_Type);
		Thread.sleep(5000);
		Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
		
	}
	public DXCodeAddPage validateAddDXCodeBtn() throws Exception
	{
		Thread.sleep(5000);
		DXCode1.click();
		return new DXCodeAddPage();
	}
	public void validateResetBtn(String type, String code, String description) throws Exception
	{
		DXCodeType.sendKeys(type);
		Thread.sleep(3000);
		DXCode2.sendKeys(code);
		Thread.sleep(3000);
		Description.sendKeys(description);
		Thread.sleep(5000);
		Reset.click();			
	}
	public void validateEditBtn(String code) throws Exception 
	{
		DXCode2.sendKeys(code);
		Thread.sleep(5000);
		Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
