package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class ServiceCodeAddPage extends TestBase
{
	public ServiceCodeAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateServiceCodeAddPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[12]/a/span[1]")
	WebElement ServiceCode;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[12]/ul/li[1]/a/span")
	WebElement Add;
	
	@FindBy(xpath = "//*[@id=\"ServiceCode\"]")
	WebElement ServiceCodeTab;
	
	@FindBy(xpath = "//*[@id=\"ServiceCodes_ServiceCode\"]")
	WebElement ServiceCodeInput;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCode\"]/div/div[1]/div/div/div/div[2]/div/div[2]/div[1]/div/button/span[1]")
	WebElement Modifier;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCode\"]/div/div[1]/div/div/div/div[2]/div/div[2]/div[2]/button")
	WebElement ModifierAdd;
	
	@FindBy(xpath = "//*[@id=\"ServiceCodes_ServiceName\"]")
	WebElement ServiceName;
	
	@FindBy(xpath = "//*[@id=\"ServiceCodes_Description\"]")
	WebElement Description;
	
	@FindBy(xpath = "//*[@id=\"Billable\"]")
	WebElement IsBillable;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCode\"]/div/div[2]/div/input")
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCode\"]/div/div[2]/div/a/input")
	WebElement Cancel;
	
	
	
	@FindBy(xpath = "//*[@id=\"ServiceCodeMapping\"]")
	WebElement ServiceCodeMapping;
	
	@FindBy(xpath = "//*[@id=\"FrmServiceCodeMapping\"]/div/div[1]/div/div/div[1]/button")
	WebElement AddServiceCodeMapping;
	
	public void serviceCodeAdd(String sCodeInput, String modifier, String modifierAdd, String serviceName,String description)throws InterruptedException
	{
		ServiceCodeInput.sendKeys(sCodeInput);
		Modifier.sendKeys(modifier);
		ModifierAdd.sendKeys(modifierAdd);
		ServiceName.sendKeys(serviceName);
		Description.sendKeys(serviceName);
		IsBillable.isSelected();
		Save.click();
		Thread.sleep(7000);
		Cancel.click();
	}
}
