package com.myezcare.qa.pages;

 

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

 

import com.myezcare.qa.base.TestBase;

 

public class AgencyListPage extends TestBase
{
    public AgencyListPage()
    {
        PageFactory.initElements(driver, this);
    }
    
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a" )
    WebElement Settings;    
    
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[1]/a" )
    WebElement Agency;
    
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[1]/ul/li[2]/a" )
    WebElement List;
    
    //Inside Page
    
    @FindBy(xpath = "//*[@id=\"SelectAllAgency\"]" )
    WebElement Checkbox;
    
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div[2]/button" )
    WebElement Refresh;
    
    @FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
    WebElement Active;
    
    @FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/a" )
    WebElement Agency1;
    
    @FindBy(xpath = "//*[@id=\"AgencyType\"]" )
    WebElement AgencyType;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_NickName\"]" )
    WebElement AgencyName;
    
    @FindBy(xpath = "//*[@id=\"RegionID\"]" )
    WebElement Region;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_TIN\"]" )
    WebElement TIN;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_EIN\"]" )
    WebElement EIN;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_Mobile\"]" )
    WebElement MobileNumber;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_Phone\"]" )
    WebElement Phone;
    
    @FindBy(xpath = "//*[@id=\"SearchAgencyListPage_Address\"]" )
    WebElement Address;
    
    @FindBy(xpath = "//*[@id=\"frmAgencyList\"]/table/tbody/tr[1]/td[12]/input" )
    WebElement Search;
    
    @FindBy(xpath = "//*[@id=\"frmAgencyList\"]/table/tbody/tr[1]/td[12]/a" )
    WebElement Reset;
    
    @FindBy(xpath = "//*[@id=\"frmAgencyList\"]/table/tbody/tr[2]/td[12]/a[1]/i" )
    WebElement Edit;

 

    //Actions

 

    public static String validateAgencyListPageTitle()
    {
        return driver.getTitle();
    }
    public boolean verifyAgencyListLabel()
    {
        return List.isDisplayed();
    }
    public void selectMultipleselectAgency() throws Exception
    {
        Thread.sleep(5000);
        Checkbox.click();
    }
    public void verifySearchBtn(String Agency_Type) throws Exception
    {
        Select type=new Select(AgencyType);
        type.selectByVisibleText(Agency_Type);
        Thread.sleep(5000);
        Search.click();
    }
    public void validateDeleteActiveRecord(String record)
    {
        Select active=new Select(Active);
        active.selectByVisibleText(record);
        
    }
    public AgencyAddPage validateAddAgencyBtn() throws Exception
    {
        Thread.sleep(5000);
        Agency1.click();
        return new AgencyAddPage();
    }
    public void validateResetBtn(String aName, String tin, String ein, String mobile, String phone, 
            String address) throws Exception
    {
        AgencyName.sendKeys(aName);
        Thread.sleep(3000);
        TIN.sendKeys(tin);
        Thread.sleep(3000);
        EIN.sendKeys(ein);
        Thread.sleep(3000);
        MobileNumber.sendKeys(mobile);
        Thread.sleep(3000);
        Phone.sendKeys(phone);
        Thread.sleep(3000);
        Address.sendKeys(address);
        Thread.sleep(5000);
        Reset.click();            
    }
    public void validateEditBtn(String aName) throws Exception 
    {
        AgencyName.sendKeys(aName);
        Thread.sleep(5000);
        Search.click();
        Thread.sleep(5000);
        Edit.click();
    }
}
 