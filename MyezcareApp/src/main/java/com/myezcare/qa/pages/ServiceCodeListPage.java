package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class ServiceCodeListPage extends TestBase
{
	public ServiceCodeListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateServiceCodeListPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[12]/a/span[1]")
	WebElement ServiceCode;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[12]/ul/li[2]/a/span")
	WebElement List;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div[1]/button")
	WebElement Refresh;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div[2]/a")
	WebElement ServiceCodeAdd;
	
	@FindBy(xpath = "//*[@id=\"SearchServiceCodeListPage_ServiceCode\"]")
	WebElement SCode;
	
	@FindBy(xpath = "//*[@id=\"SearchServiceCodeListPage_ModifierName\"]")
	WebElement ModifierName;
	
	@FindBy(xpath = "//*[@id=\"SearchServiceCodeListPage_ServiceName\"]")
	WebElement ServiceName;
	
	@FindBy(xpath = "//*[@id=\"IsBillable\"]")
	WebElement Billable;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCodeList\"]/table/tbody/tr/td[6]/input")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmServiceCodeList\"]/table/tbody/tr/td[6]/a")
	WebElement Reset;
	
	public void serviceCodeList(String serviceCode,String modifierName, String serviceName) throws InterruptedException
	{
		Refresh.click();
		Thread.sleep(7000);
		SCode.sendKeys(serviceCode);
		ModifierName.sendKeys(modifierName);
		ServiceName.sendKeys(serviceName);
		Billable.isSelected();
		Search.click();
		Thread.sleep(7000);
		Reset.click();
	}
}
