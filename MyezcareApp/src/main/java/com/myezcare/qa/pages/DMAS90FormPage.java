package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class DMAS90FormPage extends TestBase{
	public DMAS90FormPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateDMAS90FormPageTitle()
	{
		return driver.getTitle();
	}
}
