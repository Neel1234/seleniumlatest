package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class LatestERAPage extends TestBase
{
	public LatestERAPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateLatestERAPageTitle()
	{
		return driver.getTitle();
	}
}
