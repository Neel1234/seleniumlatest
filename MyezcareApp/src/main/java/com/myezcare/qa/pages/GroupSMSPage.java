package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class GroupSMSPage extends TestBase
{
	public GroupSMSPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateGroupSMSPageTitle()
	{
		return driver.getTitle();
	}
}
