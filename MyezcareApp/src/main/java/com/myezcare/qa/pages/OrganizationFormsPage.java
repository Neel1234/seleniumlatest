package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class OrganizationFormsPage extends TestBase 
{
	public OrganizationFormsPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateOrganizationFormsPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[8]/a/span")
	WebElement OrganizationForm;
	
	//Available forms..
	
	@FindBy(xpath = "//*[@id=\"MarketID\"]")
	WebElement Markets;
	
	@FindBy(xpath = "//*[@id=\"FormCategoryID\"]")
	WebElement Category;
	
	@FindBy(xpath = "//*[@id=\"SearchFormModel_FormNumber\"]")
	WebElement FormNo;
	
	@FindBy(xpath = "//*[@id=\"SearchFormModel_FormName\"]")
	WebElement FormName;
	
	@FindBy(xpath = "//*[@id=\"frmList \"]/div/table/tbody/tr[1]/td/a[1]")
	WebElement btnSelectAll;
	
	@FindBy(xpath = "//*[@id=\"frmList \"]/div/table/tbody/tr[1]/td/a[2]")
	WebElement btnDeselectAll;
	
	@FindBy(xpath = "//*[@id=\"frmList \"]/div/table/tbody/tr[2]/td[1]/div/span")
	WebElement AvailableCheckbox;
	
	@FindBy(xpath = "//*[@id=\"frmList \"]/div/table/tbody/tr[2]/td[4]/a")
	WebElement btnFormPreview;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/div[1]/div[2]/button[1]")
	WebElement btnGreaterthanArrow;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/div[1]/div[2]/button[2]")
	WebElement btnLessthanArrow;
	
	//Selected Form....
	
	@FindBy(xpath = "//*[@id=\"OrgMarketID\"]")
	WebElement SFMarket;
	
	@FindBy(xpath = "//*[@id=\"OrgFormCategoryID\"]")
	WebElement SFCategory;
	
	@FindBy(xpath = "//*[@id=\"OrgFormNumber\"]")
	WebElement SFFormNo;
	
	@FindBy(xpath = "//*[@id=\"OrFormName\"]")
	WebElement SFFormName;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[1]/td/a[1]")
	WebElement btnSFSelectAll;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[1]/td/a[2]")
	WebElement btnSFDeselectAll;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[2]/td[1]/div/span/input")
	WebElement SFChkBox;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[2]/td[4]/a[1]")
	WebElement btnSFPreviewForm;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[2]/td[4]/a[2]")
	WebElement btnEditFormName;
	
	@FindBy(xpath = "//*[@id=\"frmOrgList \"]/div/table/tbody/tr[2]/td[4]/a[3]")
	WebElement btnViewTag;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/div[2]/button")
	WebElement btnSave;
	
	public void orgForm(String fNo, String fName, String sfFNo, String sfFname)throws InterruptedException
	{
		//Available Form Action
		
		Markets.isSelected();
		Category.isSelected();
		FormNo.sendKeys(fNo);
		FormName.sendKeys(fName);
		btnSelectAll.click();
		Thread.sleep(7000);
		btnDeselectAll.click();
		Thread.sleep(7000);
		AvailableCheckbox.isSelected();
		btnFormPreview.click();
		Thread.sleep(7000);
		btnGreaterthanArrow.click();
		Thread.sleep(7000);
		btnLessthanArrow.click();
		Thread.sleep(7000);
		
		//Selected Form Action
		SFMarket.isSelected();
		SFCategory.isSelected();
		SFFormNo.sendKeys(sfFNo);
		SFFormName.sendKeys(sfFname);
		btnSFSelectAll.click();
		Thread.sleep(7000);
		btnSFDeselectAll.click();
		Thread.sleep(7000);
		SFChkBox.isSelected();
		btnSFPreviewForm.click();
		Thread.sleep(7000);
		btnEditFormName.click();
		Thread.sleep(7000);
		btnViewTag.click();
		Thread.sleep(7000);
		btnSave.click();
		Thread.sleep(7000);
	}
}
