package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class EDIFileLogsPage extends TestBase {
	public EDIFileLogsPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateEDIFileLogsPageTitle()
	{
		return driver.getTitle();
	}

}
