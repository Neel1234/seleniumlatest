package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class DXCodeAddPage extends TestBase
{
	public static String validateAgencyListPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[5]/a" )
	WebElement DXCode;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[5]/ul/li[1]/a" )
	WebElement Add;
	
	//Form Section
	
	@FindBy(xpath = "//*[@id=\"DxCode_DxCodeType\"]" )
	WebElement DXCodeType;
	
	@FindBy(xpath = "//*[@id=\"DxCode_DXCodeWithoutDot\"]" )
	WebElement DXCodeWithoutDot;
	
	@FindBy(xpath = "//*[@id=\"DxCode_DXCodeName\"]" )
	WebElement DXCode1;
	
	@FindBy(xpath = "//*[@id=\"DxCode_Description\"]" )
	WebElement Description;
	
	@FindBy(xpath = "//*[@id=\"frmDxCode\"]/div[2]/div/div[2]/input" )
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmDxCode\"]/div[2]/div/div[2]/a/input" )
	WebElement Cancel;
	
	// Initializing the Page Objects:
	
				public DXCodeAddPage()
				{
					PageFactory.initElements(driver, this);
				}
				
	//Actions
				
				public void DXCodeAdd(String dxCodeType, String dxCodeTypeWithoutDot, String dxcode1, 
						String description) throws InterruptedException
				{
					DXCodeType.sendKeys(dxCodeType);
					DXCodeWithoutDot.sendKeys(dxCodeTypeWithoutDot);
					DXCode1.sendKeys(dxcode1);
					Description.sendKeys(description);
					Save.click();
					Thread.sleep(6000);
					Cancel.click();
				}
}
