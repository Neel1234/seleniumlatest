package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class RolesAndPermissionPage extends TestBase
{
	@FindBy(xpath="//h3[@class='page-title' and contains(text(),'Role Permission')]")
	WebElement rolePermiisionLabel;
	
	// Initializing the Page Objects:
				public RolesAndPermissionPage() 
				{
					PageFactory.initElements(driver, this);
				}
				public boolean verifyRolePermiisionLabel()
				{
					return rolePermiisionLabel.isDisplayed();
				}
				
				@FindBy(xpath = "/html/body/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a/span")
				WebElement Master;
				
				@FindBy(xpath = "/html/body/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/ul/li[2]/a")
				WebElement RolesPermissions;
				
				@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/h3/span/a[2]")
				WebElement btnSave;
				
				@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[1]/h3/span/a[1]")
				WebElement btnAddRole;
				
				@FindBy(xpath = "//*[@id=\"mySelect\"]")
				WebElement RoleName;
				
				
				
				
				/*............Web Permission.........*/
				
				@FindBy(xpath = "//*[@id=\"3021\"]/i")
				WebElement btnRecordAccess;
				
				@FindBy(xpath = "//*[@id=\"2037\"]/i")
				WebElement btnAdditionalPermission;
				
				@FindBy(xpath = "//*[@id=\"2001\"]/i")
				WebElement btnDashboard;
				
				@FindBy(xpath = "//*[@id=\"2008\"]/i")
				WebElement btnMasters;
				
				@FindBy(xpath = "//*[@id=\"2085\"]/i")
				WebElement btnAgency;
				
				@FindBy(xpath = "//*[@id=\"3054\"]/i")
				WebElement btnBillingInfo;
				
				@FindBy(xpath = "//*[@id=\"3056\"]/i")
				WebElement btnNoteSentences;
				
				@FindBy(xpath = "//*[@id=\"3115\"]/i")
				WebElement btnEmailTemplate;
				
				@FindBy(xpath = "//*[@id=\"2073\"]/i")
				WebElement btnCompliance;
				
				@FindBy(xpath = "//*[@id=\"2055\"]/i")
				WebElement btnDXCode;
				
				@FindBy(xpath = "//*[@id=\"2069\"]/i")
				WebElement btnFacilityHouse;
				
				@FindBy(xpath = "//*[@id=\"2050\"]/i")
				WebElement btnGeneralMaster;
				
				@FindBy(xpath = "//*[@id=\"2044\"]/i")
				WebElement btnPayer;
				
				@FindBy(xpath = "//*[@id=\"2059\"]/i")
				WebElement btnPhysician;
				
				@FindBy(xpath = "//*[@id=\"2010\"]/i")
				WebElement btnPrefrenceSkill;
				
				@FindBy(xpath = "//*[@id=\"2011\"]/i")
				WebElement btnServiceCode;
				
				@FindBy(xpath = "//*[@id=\"3051\"]/i")
				WebElement btnCaseManager;
				
				@FindBy(xpath = "//*[@id=\"3063\"]/i")
				WebElement btnNotificationCofig;
				
				@FindBy(xpath = "//*[@id=\"2012\"]/i")
				WebElement btnVisitTask;
				
				
				@FindBy(xpath = "//*[@id=\"2009\"]/i")
				WebElement btnEmployee;
				
				
				@FindBy(xpath = "//*[@id=\"3047\"]/i")
				WebElement btnSSN;
				
				
				@FindBy(xpath = "//*[@id=\"2013\"]/i")
				WebElement btnAddUpdate;
				
				
				@FindBy(xpath = "//*[@id=\"3060\"]/i")
				WebElement btnNotificationPrefrences;
				
				@FindBy(xpath = "//*[@id=\"2139\"]/i")
				WebElement btnEmployeeInfo;
				
				@FindBy(xpath = "//*[@id=\"2140\"]/i")
				WebElement btnEmployeeDocuments;
				
				@FindBy(xpath = "//*[@id=\"2146\"]/i")
				WebElement btnEmployeeSchedule;
				
				@FindBy(xpath = "//*[@id=\"2149\"]/i")
				WebElement btnPersonalTimeOff;
				
				@FindBy(xpath = "//*[@id=\"2154\"]/i")
				WebElement btnTimeSheet;
				
				@FindBy(xpath = "//*[@id=\"2158\"]/i")
				WebElement btnNotes;
				
				@FindBy(xpath = "//*[@id=\"2162\"]/i")
				WebElement btnCertificate;
				
				@FindBy(xpath = "//*[@id=\"2163\"]/i")
				WebElement btnChecklist;
				
				@FindBy(xpath = "//*[@id=\"2024\"]/i")
				WebElement btnPatientIntake;
				
				@FindBy(xpath = "//*[@id=\"3043\"]/i")
				WebElement btnPatientSSN;
				
				@FindBy(xpath = "//*[@id=\"2025\"]/i")
				WebElement btnPatientAddUpdate;
				
				@FindBy(xpath = "//*[@id=\"2089\"]/i")
				WebElement btnPatientDetails;
				
				@FindBy(xpath = "//*[@id=\"3090\"]/i")
				WebElement btnReferralHistory;
				
				@FindBy(xpath = "//*[@id=\"3094\"]/i")
				WebElement btnPatientPhysician;
				
				@FindBy(xpath = "//*[@id=\"3098\"]/i")
				WebElement btnPatientDXCode;
				
				@FindBy(xpath = "//*[@id=\"3102\"]/i")
				WebElement btnMedication;
				
				@FindBy(xpath = "//*[@id=\"3107\"]/i")
				WebElement btnAllergy;
				
				@FindBy(xpath = "//*[@id=\"2092\"]/i")
				WebElement btnDocuments;
				
				@FindBy(xpath = "//*[@id=\"2098\"]/i")
				WebElement btnBilling;
				
				@FindBy(xpath = "//*[@id=\"2099\"]/i")
				WebElement btnPatientPayer;
				
				@FindBy(xpath = "//*[@id=\"2103\"]/i")
				WebElement btnBillingDetail;		
				
				@FindBy(xpath = "//*[@id=\"2107\"]/i")
				WebElement btnPriorAuth;
				
				@FindBy(xpath = "//*[@id=\"2111\"]/i")
				WebElement btnCarePlan;
				
				@FindBy(xpath = "//*[@id=\"2112\"]/i")
				WebElement btnTaskMapping;
				
				@FindBy(xpath = "//*[@id=\"2116\"]/i")
				WebElement btnPatientSchedule;
				
				@FindBy(xpath = "//*[@id=\"2168\"]/i")
				WebElement btnFrequency;
				
				@FindBy(xpath = "//*[@id=\"2172\"]/i")
				WebElement btnCaseLoad;
				
				@FindBy(xpath = "//*[@id=\"2120\"]/i")
				WebElement btnBlockEmployee;
				
				@FindBy(xpath = "//*[@id=\"2124\"]/i")
				WebElement btnInternalMessaging;
				
				@FindBy(xpath = "//*[@id=\"2128\"]/i")
				WebElement btnPatientTimeSheet;
				
				@FindBy(xpath = "//*[@id=\"2132\"]/i")
				WebElement btnPNotes;
				
				@FindBy(xpath = "//*[@id=\"2136\"]/i")
				WebElement btnForms;
				
				@FindBy(xpath = "//*[@id=\"3078\"]/i")
				WebElement btnDMAS97;
				
				@FindBy(xpath = "//*[@id=\"2029\"]/i")
				WebElement btnScheduling;
				
				@FindBy(xpath = "//*[@id=\"2063\"]/i")
				WebElement btnBillingClaimProcessing;
				
				@FindBy(xpath = "//*[@id=\"2032\"]/i")
				WebElement btnMessages;
				
				@FindBy(xpath = "//*[@id=\"2036\"]/i")
				WebElement btnReports;
				
				
				
				public void webPermission()
				{
					WebElement AdministrativePermission = driver.findElement(By.xpath("//*[@id=\"2999_anchor\"]/i[1]"));
					AdministrativePermission.click();
					btnRecordAccess.click();
					
					WebElement RecordAccesss = driver.findElement(By.xpath("//*[@id=\"3021_anchor\"]/i[1]"));
					RecordAccesss.click();
					
					WebElement AllRecord = driver.findElement(By.xpath("//*[@id=\"3022_anchor\"]/i[1]"));
					AllRecord.click();
					
					WebElement LimitedRecord = driver.findElement(By.xpath("//*[@id=\"3023_anchor\"]/i[1]"));
					LimitedRecord.click();
					
					btnAdditionalPermission.click();
					
					WebElement AdditionalPermission = driver.findElement(By.xpath("//*[@id=\"2037_anchor\"]/i[1]"));
					AdditionalPermission.click();
					
					WebElement RolePermission = driver.findElement(By.xpath("//*[@id=\"2038_anchor\"]/i[1]"));
					RolePermission.click();
					
					WebElement organizationSetting = driver.findElement(By.xpath("//*[@id=\"2054_anchor\"]/i[1]"));
					organizationSetting.click();
					
					btnDashboard.click();
					
					WebElement Dashboard = driver.findElement(By.xpath("//*[@id=\"2001_anchor\"]/i[1]"));
					Dashboard.click();
					
					WebElement EmpDidNotCInOut = driver.findElement(By.xpath("//*[@id=\"2002_anchor\"]/i[1]"));
					EmpDidNotCInOut.click();
					
					WebElement EmpOverTimeLastSevenDay = driver.findElement(By.xpath("//*[@id=\"2003_anchor\"]/i[1]"));
					EmpOverTimeLastSevenDay.click();
					
					WebElement NewPatient = driver.findElement(By.xpath("//*[@id=\"2004_anchor\"]/i[1]"));
					NewPatient.click();
					
					
					WebElement PatientFullNotSchedule = driver.findElement(By.xpath("//*[@id=\"2005_anchor\"]/i[1]"));
					PatientFullNotSchedule.click();
					
					WebElement ReceiveInternalMsg = driver.findElement(By.xpath("//*[@id=\"2006_anchor\"]/i[1]"));
					ReceiveInternalMsg.click();
					
					WebElement SendInternalMsg = driver.findElement(By.xpath("//*[@id=\"2007_anchor\"]/i[1]"));
					SendInternalMsg.click();
					
					WebElement EmpAvgDelays = driver.findElement(By.xpath("//*[@id=\"2039_anchor\"]/i[1]"));
					EmpAvgDelays.click();
					
					WebElement WebNotificaion = driver.findElement(By.xpath("//*[@id=\"3066_anchor\"]/i[1]"));
					WebNotificaion.click();
					
					btnMasters.click();
					
					WebElement Masters = driver.findElement(By.xpath("//*[@id=\"2008_anchor\"]/i[1]"));
					Masters.click();
					
					btnAgency.click();
					
					WebElement Agency = driver.findElement(By.xpath("//*[@id=\"2085_anchor\"]/i[1]"));
					Agency.click();
					
					WebElement AddUpdate = driver.findElement(By.xpath("//*[@id=\"2086_anchor\"]/i[1]"));
					AddUpdate.click();
					
					WebElement Delete = driver.findElement(By.xpath("//*[@id=\"2087_anchor\"]/i[1]"));
					Delete.click();
					
					WebElement List = driver.findElement(By.xpath("//*[@id=\"2088_anchor\"]/i[1]"));
					List.click();
					
					btnBillingInfo.click();
					
					WebElement BillingInfo = driver.findElement(By.xpath("//*[@id=\"3054_anchor\"]/i[1]"));
					BillingInfo.click();
					
					WebElement ViewBillingInfo = driver.findElement(By.xpath("//*[@id=\"3055_anchor\"]/i[1]"));
					ViewBillingInfo.click();
					
					WebElement NoteSentences = driver.findElement(By.xpath("//*[@id=\"3056_anchor\"]/i[1]"));
					NoteSentences.click();
					
					btnNoteSentences.click();
					
					WebElement NSAddUpdate = driver.findElement(By.xpath("//*[@id=\"3057_anchor\"]/i[1]"));
					NSAddUpdate.click();
					
					WebElement NSList = driver.findElement(By.xpath("//*[@id=\"3058_anchor\"]/i[1]"));
					NSList.click();
					
					WebElement EmailTemplate = driver.findElement(By.xpath("//*[@id=\"3115_anchor\"]/i[1]"));
					EmailTemplate.click();
					
					btnEmailTemplate.click();
					
					WebElement AddEmailTemplate = driver.findElement(By.xpath("//*[@id=\"3116_anchor\"]/i[1]"));
					AddEmailTemplate.click();
					
					WebElement EmailTemplateList = driver.findElement(By.xpath("//*[@id=\"3117_anchor\"]/i[1]"));
					EmailTemplateList.click();
					
					WebElement EditEmailTemplate = driver.findElement(By.xpath("//*[@id=\"3118_anchor\"]/i[1]"));
					EditEmailTemplate.click();
					
					WebElement HideEmailTemplate = driver.findElement(By.xpath("//*[@id=\"3119_anchor\"]/i[1]"));
					HideEmailTemplate.click();
					
					WebElement OrganizationPreference = driver.findElement(By.xpath("//*[@id=\"3112_anchor\"]/i[1]"));
					OrganizationPreference.click();
					
					btnCompliance.click();
					
					WebElement Compliance = driver.findElement(By.xpath("//*[@id=\"2073_anchor\"]/i[1]"));
					Compliance.click();
					
					WebElement CAddUpdate = driver.findElement(By.xpath("//*[@id=\"2076_anchor\"]/i[1]"));
					CAddUpdate.click();
					
					WebElement CDelete = driver.findElement(By.xpath("//*[@id=\"2077_anchor\"]/i[1]"));
					CDelete.click();
					
					WebElement CList = driver.findElement(By.xpath("//*[@id=\"2078_anchor\"]/i[1]"));
					CList.click();
					
					btnDXCode.click();
					
					WebElement DXCode = driver.findElement(By.xpath("//*[@id=\"2055_anchor\"]/i[1]"));
					DXCode.click();
					
					WebElement DXAddUpdate = driver.findElement(By.xpath("//*[@id=\"2056_anchor\"]/i[1]"));
					DXAddUpdate.click();
					
					WebElement DXDelete = driver.findElement(By.xpath("//*[@id=\"2057_anchor\"]/i[1]"));
					DXDelete.click();
					
					WebElement DXList = driver.findElement(By.xpath("//*[@id=\"2058_anchor\"]/i[1]"));
					DXList.click();
					
					btnFacilityHouse.click();
					
					WebElement FacilityHouse = driver.findElement(By.xpath("//*[@id=\"2069_anchor\"]/i[1]"));
					FacilityHouse.click();
					
					WebElement FHAddUpdate = driver.findElement(By.xpath("//*[@id=\"2070_anchor\"]/i[1]"));
					FHAddUpdate.click();
					
					WebElement FHDelete = driver.findElement(By.xpath("//*[@id=\"2071_anchor\"]/i[1]"));
					FHDelete.click();
					
					WebElement FHList = driver.findElement(By.xpath("//*[@id=\"2072_anchor\"]/i[1]"));
					FHList.click();
					
					btnGeneralMaster.click();
					
					WebElement GeneralMaster = driver.findElement(By.xpath("//*[@id=\"2050_anchor\"]/i[1]"));
					GeneralMaster.click();
					
					WebElement GMAddUpdate = driver.findElement(By.xpath("//*[@id=\"2051_anchor\"]/i[1]"));
					GMAddUpdate.click();
					
					WebElement GMDelete = driver.findElement(By.xpath("//*[@id=\"2052_anchor\"]/i[1]"));
					GMDelete.click();
					
					WebElement GMList = driver.findElement(By.xpath("//*[@id=\"2053_anchor\"]/i[1]"));
					GMList.click();
					
					btnPayer.click();
					
					WebElement Payer = driver.findElement(By.xpath("//*[@id=\"2044_anchor\"]/i[1]"));
					Payer.click();
					
					WebElement PayerAddUpdate = driver.findElement(By.xpath("//*[@id=\"2045_anchor\"]/i[1]"));
					PayerAddUpdate.click();
					
					WebElement PayerDelete = driver.findElement(By.xpath("//*[@id=\"2046_anchor\"]/i[1]"));
					PayerDelete.click();
					
					WebElement PayerList = driver.findElement(By.xpath("//*[@id=\"2047_anchor\"]/i[1]"));
					PayerList.click();
					
					WebElement PayerServiceCodeMapping = driver.findElement(By.xpath("//*[@id=\"2048_anchor\"]/i[1]"));
					PayerServiceCodeMapping.click();
					
					btnPhysician.click();
					
					WebElement Physician = driver.findElement(By.xpath("//*[@id=\"2059_anchor\"]/i[1]"));
					Physician.click();
					
					WebElement PhysicianAddUpdate = driver.findElement(By.xpath("//*[@id=\"2060_anchor\"]/i[1]"));
					PhysicianAddUpdate.click();
					
					WebElement PhysicianDelete = driver.findElement(By.xpath("//*[@id=\"2061_anchor\"]/i[1]"));
					PhysicianDelete.click();
					
					WebElement PhysicianList = driver.findElement(By.xpath("//*[@id=\"2062_anchor\"]/i[1]"));
					PhysicianList.click();
					
					btnPrefrenceSkill.click();
					
					WebElement PrefrenceSkill = driver.findElement(By.xpath("//*[@id=\"2010_anchor\"]/i[1]"));
					PrefrenceSkill.click();
					
					WebElement PSAddUpdate = driver.findElement(By.xpath("//*[@id=\"2018_anchor\"]/i[1]"));
					PSAddUpdate.click();
					
					WebElement PSDelete = driver.findElement(By.xpath("//*[@id=\"2042_anchor\"]/i[1]"));
					PSDelete.click();
					
					WebElement PSList = driver.findElement(By.xpath("//*[@id=\"2019_anchor\"]/i[1]"));
					PSList.click();
					
					btnServiceCode.click();
					
					WebElement ServiceCode = driver.findElement(By.xpath("//*[@id=\"2011_anchor\"]/i[1]"));
					ServiceCode.click();
					
					WebElement SCAddUpdate = driver.findElement(By.xpath("//*[@id=\"2020_anchor\"]/i[1]"));
					SCAddUpdate.click();
					
					WebElement SCList = driver.findElement(By.xpath("//*[@id=\"2021_anchor\"]/i[1]"));
					SCList.click();
					
					btnCaseManager.click();
					
					WebElement CaseManager = driver.findElement(By.xpath("//*[@id=\"3051_anchor\"]/i[1]"));
					CaseManager.click();
					
					WebElement CMAddUpdate = driver.findElement(By.xpath("//*[@id=\"3052_anchor\"]/i[1]"));
					CMAddUpdate.click();
					
					WebElement CMList = driver.findElement(By.xpath("//*[@id=\"3053_anchor\"]/i[1]"));
					CMList.click();
					
					btnNotificationCofig.click();
					
					WebElement NotificationConfig = driver.findElement(By.xpath("//*[@id=\"3063_anchor\"]/i[1]"));
					NotificationConfig.click();
					
					WebElement NCUpdate = driver.findElement(By.xpath("//*[@id=\"3064_anchor\"]/i[1]"));
					NCUpdate.click();
					
					WebElement NCViewOnly = driver.findElement(By.xpath("//*[@id=\"3065_anchor\"]/i[1]"));
					NCViewOnly.click();
					
					btnVisitTask.click();
					
					WebElement VisitTask = driver.findElement(By.xpath("//*[@id=\"2012_anchor\"]/i[1]"));
					VisitTask.click();
					
					WebElement VTAddUpdate = driver.findElement(By.xpath("//*[@id=\"2022_anchor\"]/i[1]"));
					VTAddUpdate.click();
					
					WebElement VTDelete = driver.findElement(By.xpath("//*[@id=\"2040_anchor\"]/i[1]"));
					VTDelete.click();
					
					WebElement VTList = driver.findElement(By.xpath("//*[@id=\"2023_anchor\"]/i[1]"));
					VTList.click();
					
					btnEmployee.click();
					
					WebElement Employee = driver.findElement(By.xpath("//*[@id=\"2009_anchor\"]/i[1]"));
					Employee.click();
					
					btnSSN.click();
					
					WebElement SSN = driver.findElement(By.xpath("//*[@id=\"3047_anchor\"]/i[1]"));
					SSN.click();
					
					WebElement CanSee = driver.findElement(By.xpath("//*[@id=\"3048_anchor\"]/i[1]"));
					CanSee.click();
					
					WebElement CannotSee = driver.findElement(By.xpath("//*[@id=\"3049_anchor\"]/i[1]"));
					CannotSee.click();
					
					WebElement CannotSeeLastFourDigit = driver.findElement(By.xpath("//*[@id=\"3050_anchor\"]/i[1]"));
					CannotSeeLastFourDigit.click();
					
					btnAddUpdate.click();
					btnNotificationPrefrences.click();
					
					WebElement NotificationPrefrences = driver.findElement(By.xpath("//*[@id=\"3060_anchor\"]/i[1]"));
					NotificationPrefrences.click();
					
					WebElement NPUpdate = driver.findElement(By.xpath("//*[@id=\"3061_anchor\"]/i[1]"));
					NPUpdate.click();
					
					WebElement NPViewOnly = driver.findElement(By.xpath("//*[@id=\"3062_anchor\"]/i[1]"));
					NPViewOnly.click();
					
					WebElement EmailSign = driver.findElement(By.xpath("//*[@id=\"3120_anchor\"]/i[1]"));
					EmailSign.click();
					
					btnEmployeeInfo.click();
					
					WebElement EmployeeInfo = driver.findElement(By.xpath("//*[@id=\"2139_anchor\"]/i[1]"));
					EmployeeInfo.click();
					
					WebElement GroupAddUpdate = driver.findElement(By.xpath("//*[@id=\"3072_anchor\"]/i[1]"));
					GroupAddUpdate.click();
					
					WebElement EmpViewOnly = driver.findElement(By.xpath("//*[@id=\"2166_anchor\"]/i[1]"));
					EmpViewOnly.click();
					
					WebElement EmpAddUpdate = driver.findElement(By.xpath("//*[@id=\"2167_anchor\"]/i[1]"));
					EmpAddUpdate.click();
					
					btnEmployeeDocuments.click();
					
					WebElement EmployeeDocs = driver.findElement(By.xpath("//*[@id=\"2140_anchor\"]/i[1]"));
					EmployeeDocs.click();
					
					WebElement AddSection = driver.findElement(By.xpath("//*[@id=\"2141_anchor\"]/i[1]"));
					AddSection.click();
					
					WebElement AddSubsection = driver.findElement(By.xpath("//*[@id=\"2142_anchor\"]/i[1]"));
					AddSubsection.click();
					
					WebElement ViewDocuments = driver.findElement(By.xpath("//*[@id=\"2143_anchor\"]/i[1]"));
					ViewDocuments.click();
					
					WebElement AddUpdateDocs = driver.findElement(By.xpath("//*[@id=\"2144_anchor\"]/i[1]"));
					AddUpdateDocs.click();
					
					WebElement DeleteDocs = driver.findElement(By.xpath("//*[@id=\"2145_anchor\"]/i[1]"));
					DeleteDocs.click();
					
					btnEmployeeSchedule.click();
					
					WebElement EmployeeSchedule = driver.findElement(By.xpath("//*[@id=\"2146_anchor\"]/i[1]"));
					EmployeeSchedule.click();
					
					WebElement ESViewOnly = driver.findElement(By.xpath("//*[@id=\"2147_anchor\"]/i[1]"));
					ESViewOnly.click();
					
					WebElement ESAddUpdate = driver.findElement(By.xpath("//*[@id=\"2148_anchor\"]/i[1]"));
					ESAddUpdate.click();
					
					btnPersonalTimeOff.click();
					
					WebElement PersonalTimeOff = driver.findElement(By.xpath("//*[@id=\"2149_anchor\"]/i[1]"));
					PersonalTimeOff.click();
					
					WebElement PTOViewOnly = driver.findElement(By.xpath("//*[@id=\"2150_anchor\"]/i[1]"));
					PTOViewOnly.click();
					
					WebElement PTOAddUpdate = driver.findElement(By.xpath("//*[@id=\"2151_anchor\"]/i[1]"));
					PTOAddUpdate.click();
					
					WebElement PTODelete = driver.findElement(By.xpath("//*[@id=\"2152_anchor\"]/i[1]"));
					PTODelete.click();
					
					WebElement Calander = driver.findElement(By.xpath("//*[@id=\"2153_anchor\"]/i[1]"));
					Calander.click();
					
					btnTimeSheet.click();
					
					WebElement TimeSheet = driver.findElement(By.xpath("//*[@id=\"2154_anchor\"]/i[1]"));
					TimeSheet.click();
					
					WebElement TSViewOnly = driver.findElement(By.xpath("//*[@id=\"2155_anchor\"]/i[1]"));
					TSViewOnly.click();
					
					WebElement TSAddUpdate = driver.findElement(By.xpath("//*[@id=\"2156_anchor\"]/i[1]"));
					TSAddUpdate.click();
					
					WebElement TSDelete = driver.findElement(By.xpath("//*[@id=\"2157_anchor\"]/i[1]"));
					TSDelete.click();
					
					btnNotes.click();
					
					WebElement Notes = driver.findElement(By.xpath("//*[@id=\"2158_anchor\"]/i[1]"));
					Notes.click();
					
					WebElement NotesViewOnly = driver.findElement(By.xpath("//*[@id=\"2159_anchor\"]/i[1]"));
					NotesViewOnly.click();
					
					WebElement NotesAddUpdate = driver.findElement(By.xpath("//*[@id=\"2160_anchor\"]/i[1]"));
					NotesAddUpdate.click();
					
					WebElement NotesDelete = driver.findElement(By.xpath("//*[@id=\"2161_anchor\"]/i[1]"));
					NotesDelete.click();
					
					btnCertificate.click();
					
					WebElement Certificate = driver.findElement(By.xpath("//*[@id=\"2162_anchor\"]/i[1]"));
					Certificate.click();
					
					WebElement CertificateViewOnly = driver.findElement(By.xpath("//*[@id=\"3074_anchor\"]/i[1]"));
					CertificateViewOnly.click();
					
					WebElement CertificateAddUpdate = driver.findElement(By.xpath("//*[@id=\"3075_anchor\"]/i[1]"));
					CertificateAddUpdate.click();
					
					WebElement CertificateDelete = driver.findElement(By.xpath("//*[@id=\"3076_anchor\"]/i[1]"));
					CertificateDelete.click();
					
					btnChecklist.click();
					
					WebElement Checklist = driver.findElement(By.xpath("//*[@id=\"2163_anchor\"]/i[1]"));
					Checklist.click();
					
					WebElement ChecklistViewOnly = driver.findElement(By.xpath("//*[@id=\"2164_anchor\"]/i[1]"));
					ChecklistViewOnly.click();
					
					WebElement ChecklistAddUpdate = driver.findElement(By.xpath("//*[@id=\"2165_anchor\"]/i[1]"));
					ChecklistAddUpdate.click();
					
					WebElement EmployeeDelete = driver.findElement(By.xpath("//*[@id=\"2041_anchor\"]/i[1]"));
					EmployeeDelete.click();
					
					WebElement EmployeeList = driver.findElement(By.xpath("//*[@id=\"2014_anchor\"]/i[1]"));
					EmployeeList.click();
					
					WebElement EmployeeCalander = driver.findElement(By.xpath("//*[@id=\"2015_anchor\"]/i[1]"));
					EmployeeCalander.click();
					
					WebElement EmpSchedule = driver.findElement(By.xpath("//*[@id=\"2016_anchor\"]/i[1]"));
					EmpSchedule.click();
					
					WebElement EmpPTO = driver.findElement(By.xpath("//*[@id=\"2017_anchor\"]/i[1]"));
					EmpPTO.click();
					
					btnPatientIntake.click();
					
					WebElement PatientIntake = driver.findElement(By.xpath("//*[@id=\"2024_anchor\"]/i[1]"));
					PatientIntake.click();
					
					WebElement PatientChart = driver.findElement(By.xpath("//*[@id=\"3111_anchor\"]/i[1]"));
					PatientChart.click();
					
					WebElement IncidentReport = driver.findElement(By.xpath("//*[@id=\"3113_anchor\"]/i[1]"));
					IncidentReport.click();
					
					WebElement AddNotes = driver.findElement(By.xpath("//*[@id=\"3121_anchor\"]/i[1]"));
					AddNotes.click();
					
					btnPatientSSN.click();
					
					WebElement PatientSSN = driver.findElement(By.xpath("//*[@id=\"3043_anchor\"]/i[1]"));
					PatientSSN.click();
					
					WebElement PCanSee = driver.findElement(By.xpath("//*[@id=\"3044_anchor\"]/i[1]"));
					PCanSee.click();
					
					WebElement PCannotSee = driver.findElement(By.xpath("//*[@id=\"3045_anchor\"]/i[1]"));
					PCannotSee.click();
					
					WebElement PCanSeeLastFourDigit = driver.findElement(By.xpath("//*[@id=\"3046_anchor\"]/i[1]"));
					PCanSeeLastFourDigit.click();
					
					btnPatientAddUpdate.click();
					
					WebElement PatientAddUpdate = driver.findElement(By.xpath("//*[@id=\"2025_anchor\"]/i[1]"));
					PatientAddUpdate.click();
					
					btnPatientDetails.click();
					
					WebElement PatientDetails = driver.findElement(By.xpath("//*[@id=\"2089_anchor\"]/i[1]"));
					PatientDetails.click();
					
					btnReferralHistory.click();
					
					WebElement ReferralHistory = driver.findElement(By.xpath("//*[@id=\"3090_anchor\"]/i[1]"));
					ReferralHistory.click();
					
					WebElement RHViewOnly = driver.findElement(By.xpath("//*[@id=\"3091_anchor\"]/i[1]"));
					RHViewOnly.click();
					
					WebElement RHAddUpdate = driver.findElement(By.xpath("//*[@id=\"3092_anchor\"]/i[1]"));
					RHAddUpdate.click();
					
					WebElement RHDelete = driver.findElement(By.xpath("//*[@id=\"3093_anchor\"]/i[1]"));
					RHDelete.click();
					
					btnPatientPhysician.click();
					
					WebElement PPhysician = driver.findElement(By.xpath("//*[@id=\"3094_anchor\"]/i[1]"));
					PPhysician.click();
					
					WebElement PPViewOnly = driver.findElement(By.xpath("//*[@id=\"3095_anchor\"]/i[1]"));
					PPViewOnly.click();
					
					WebElement PPAddUpdate = driver.findElement(By.xpath("//*[@id=\"3096_anchor\"]/i[1]"));
					PPAddUpdate.click();
					
					WebElement PPDelete = driver.findElement(By.xpath("//*[@id=\"3097_anchor\"]/i[1]"));
					PPDelete.click();
					
					btnPatientDXCode.click();
					
					WebElement PDXCodeViewOnly = driver.findElement(By.xpath("//*[@id=\"3099_anchor\"]/i[1]"));
					PDXCodeViewOnly.click();
					
					WebElement PDXCodeAddUpdate = driver.findElement(By.xpath("//*[@id=\"3100_anchor\"]/i[1]"));
					PDXCodeAddUpdate.click();
					
					WebElement PDXCodeDelete = driver.findElement(By.xpath("//*[@id=\"3101_anchor\"]/i[1]"));
					PDXCodeDelete.click();
					
					btnMedication.click();
					
					WebElement Medication = driver.findElement(By.xpath("//*[@id=\"3102_anchor\"]/i[1]"));
					Medication.click();
					
					WebElement MViewOnly = driver.findElement(By.xpath("//*[@id=\"3103_anchor\"]/i[1]"));
					MViewOnly.click();
					
					WebElement MAddUpdate = driver.findElement(By.xpath("//*[@id=\"3104_anchor\"]/i[1]"));
					MAddUpdate.click();
					
					WebElement ViewAuditLog = driver.findElement(By.xpath("//*[@id=\"3106_anchor\"]/i[1]"));
					ViewAuditLog.click();
					
					btnAllergy.click();
					
					WebElement Allergy = driver.findElement(By.xpath("//*[@id=\"3107_anchor\"]/i[1]"));
					Allergy.click();
					
					WebElement AllergyViewOnly = driver.findElement(By.xpath("//*[@id=\"3108_anchor\"]/i[1]"));
					AllergyViewOnly.click();
					
					WebElement AllergyAddUpdate = driver.findElement(By.xpath("//*[@id=\"3109_anchor\"]/i[1]"));
					AllergyAddUpdate.click();
					
					WebElement AllergyDelete = driver.findElement(By.xpath("//*[@id=\"3110_anchor\"]/i[1]"));
					AllergyDelete.click();
					
					WebElement PGroupAddUpdate = driver.findElement(By.xpath("//*[@id=\"3073_anchor\"]/i[1]"));
					PGroupAddUpdate.click();
					
					WebElement PViewOnly = driver.findElement(By.xpath("//*[@id=\"2090_anchor\"]/i[1]"));
					PViewOnly.click();
					
					WebElement PAddUpdate = driver.findElement(By.xpath("//*[@id=\"2091_anchor\"]/i[1]"));
					PAddUpdate.click();
					
					btnDocuments.click();
					
					WebElement Documents = driver.findElement(By.xpath("//*[@id=\"2092_anchor\"]/i[1]"));
					Documents.click();
					
					WebElement DocumentEmail = driver.findElement(By.xpath("//*[@id=\"3114_anchor\"]/i[1]"));
					DocumentEmail.click();
					
					WebElement DAddSection = driver.findElement(By.xpath("//*[@id=\"2093_anchor\"]/i[1]"));
					DAddSection.click();
					
					WebElement DAddSubsection = driver.findElement(By.xpath("//*[@id=\"2094_anchor\"]/i[1]"));
					DAddSubsection.click();
					
					WebElement ViewDocs = driver.findElement(By.xpath("//*[@id=\"2095_anchor\"]/i[1]"));
					ViewDocs.click();
					
					WebElement AddUpdatedDocs = driver.findElement(By.xpath("//*[@id=\"2096_anchor\"]/i[1]"));
					AddUpdatedDocs.click();
					
					WebElement DeleteDocuments = driver.findElement(By.xpath("//*[@id=\"2097_anchor\"]/i[1]"));
					DeleteDocuments.click();
					
					btnBilling.click();
					
					WebElement Billing = driver.findElement(By.xpath("//*[@id=\"2098_anchor\"]/i[1]"));
					Billing.click();
					
					btnPatientPayer.click();
					
					WebElement PatientPayer = driver.findElement(By.xpath("//*[@id=\"2099_anchor\"]/i[1]"));
					PatientPayer.click();
					
					WebElement PayerViewOnly = driver.findElement(By.xpath("//*[@id=\"2100_anchor\"]/i[1]"));
					PayerViewOnly.click();
					
					WebElement PPayerAddUpdate = driver.findElement(By.xpath("//*[@id=\"2101_anchor\"]/i[1]"));
					PPayerAddUpdate.click();
					
					WebElement PPayerDelete = driver.findElement(By.xpath("//*[@id=\"2102_anchor\"]/i[1]"));
					PPayerDelete.click();
					
					btnBillingDetail.click();
					
					WebElement BilingDetail = driver.findElement(By.xpath("//*[@id=\"2103_anchor\"]/i[1]"));
					BilingDetail.click();
					
					WebElement BDViewOnly = driver.findElement(By.xpath("//*[@id=\"2104_anchor\"]/i[1]"));
					BDViewOnly.click();
					
					WebElement BDAddUpdate = driver.findElement(By.xpath("//*[@id=\"2105_anchor\"]/i[1]"));
					BDAddUpdate.click();
					
					WebElement BDDelete = driver.findElement(By.xpath("//*[@id=\"2106_anchor\"]/i[1]"));
					BDDelete.click();
					
					btnPriorAuth.click();
					
					WebElement PriorAuth = driver.findElement(By.xpath("//*[@id=\"2107_anchor\"]/i[1]"));
					PriorAuth.click();
					
					WebElement PAViewOnly = driver.findElement(By.xpath("//*[@id=\"2108_anchor\"]/i[1]"));
					PAViewOnly.click();
					
					WebElement PAAddUpdate= driver.findElement(By.xpath("//*[@id=\"2109_anchor\"]/i[1]"));
					PAAddUpdate.click();
					
					WebElement PADelete = driver.findElement(By.xpath("//*[@id=\"2110_anchor\"]/i[1]"));
					PADelete.click();
					
					btnCarePlan.click();
					
					WebElement CarePlan = driver.findElement(By.xpath("//*[@id=\"2111_anchor\"]/i[1]"));
					CarePlan.click();
					
					btnTaskMapping.click();
					
					WebElement TaskMapping = driver.findElement(By.xpath("//*[@id=\"2112_anchor\"]/i[1]"));
					TaskMapping.click();
					
					WebElement TMViewOnly = driver.findElement(By.xpath("//*[@id=\"2113_anchor\"]/i[1]"));
					TMViewOnly.click();
					
					WebElement TMAddUpdate = driver.findElement(By.xpath("//*[@id=\"2114_anchor\"]/i[1]"));
					TMAddUpdate.click();
					
					WebElement TMDelete = driver.findElement(By.xpath("//*[@id=\"2115_anchor\"]/i[1]"));
					TMDelete.click();
					
					btnPatientSchedule.click();
					
					WebElement PatientSchedule = driver.findElement(By.xpath("//*[@id=\"2116_anchor\"]/i[1]"));
					PatientSchedule.click();
					
					WebElement PSViewOnly = driver.findElement(By.xpath("//*[@id=\"2117_anchor\"]/i[1]"));
					PSViewOnly.click();
					
					WebElement PSAddUpdates = driver.findElement(By.xpath("//*[@id=\"2118_anchor\"]/i[1]"));
					PSAddUpdates.click();
					
					btnFrequency.click();
					
					WebElement Frequency = driver.findElement(By.xpath("//*[@id=\"2168_anchor\"]/i[1]"));
					Frequency.click();
					
					WebElement FViewOnly = driver.findElement(By.xpath("//*[@id=\"2169_anchor\"]/i[1]"));
					FViewOnly.click();
					
					WebElement FAddUpdate = driver.findElement(By.xpath("//*[@id=\"2170_anchor\"]/i[1]"));
					FAddUpdate.click();
					
					WebElement FDelete = driver.findElement(By.xpath("//*[@id=\"2171_anchor\"]/i[1]"));
					FDelete.click();
					
					btnCaseLoad.click();
					
					WebElement CaseLoad = driver.findElement(By.xpath("//*[@id=\"2172_anchor\"]/i[1]"));
					CaseLoad.click();
					
					WebElement CLViewOnly = driver.findElement(By.xpath("//*[@id=\"2173_anchor\"]/i[1]"));
					CLViewOnly.click();
					
					WebElement CLAddUpdate = driver.findElement(By.xpath("//*[@id=\"2174_anchor\"]/i[1]"));
					CLAddUpdate.click();
					
					WebElement CLDelete = driver.findElement(By.xpath("//*[@id=\"2175_anchor\"]/i[1]"));
					CLDelete.click();
					
					WebElement Calanders = driver.findElement(By.xpath("//*[@id=\"2119_anchor\"]/i[1]"));
					Calanders.click();
					
					btnBlockEmployee.click();
					
					WebElement BlockEmployee = driver.findElement(By.xpath("//*[@id=\"2120_anchor\"]/i[1]"));
					BlockEmployee.click();
					
					WebElement BEViewOnly = driver.findElement(By.xpath("//*[@id=\"2121_anchor\"]/i[1]"));
					BEViewOnly.click();
					
					WebElement BEAddUpdate = driver.findElement(By.xpath("//*[@id=\"2122_anchor\"]/i[1]"));
					BEAddUpdate.click();
					
					WebElement BEDelete = driver.findElement(By.xpath("//*[@id=\"2123_anchor\"]/i[1]"));
					BEDelete.click();
					
					btnInternalMessaging.click();
					
					WebElement InternalMassage = driver.findElement(By.xpath("//*[@id=\"2124_anchor\"]/i[1]"));
					InternalMassage.click();
					
					WebElement IMViewOnly = driver.findElement(By.xpath("//*[@id=\"2125_anchor\"]/i[1]"));
					IMViewOnly.click();
					
					WebElement IMAddUpdate = driver.findElement(By.xpath("//*[@id=\"2126_anchor\"]/i[1]"));
					IMAddUpdate.click();
					
					WebElement IMDelete = driver.findElement(By.xpath("//*[@id=\"2127_anchor\"]/i[1]"));
					IMDelete.click();
					
					btnPatientTimeSheet.click();
					
					WebElement PTimeSheet = driver.findElement(By.xpath("//*[@id=\"2128_anchor\"]/i[1]"));
					PTimeSheet.click();
					
					WebElement PTSViewOnly = driver.findElement(By.xpath("//*[@id=\"2129_anchor\"]/i[1]"));
					PTSViewOnly.click();
					
					WebElement PTSAddUpdate = driver.findElement(By.xpath("//*[@id=\"2130_anchor\"]/i[1]"));
					PTSAddUpdate.click();
					
					WebElement PTSDelete = driver.findElement(By.xpath("//*[@id=\"2131_anchor\"]/i[1]"));
					PTSDelete.click();
					
					btnPNotes.click();
					
					WebElement PNotes = driver.findElement(By.xpath("//*[@id=\"2132_anchor\"]/i[1]"));
					PNotes.click();
					
					WebElement PNViewOnly = driver.findElement(By.xpath("//*[@id=\"2133_anchor\"]/i[1]"));
					PNViewOnly.click();
					
					WebElement PNAddUpdate = driver.findElement(By.xpath("//*[@id=\"2134_anchor\"]/i[1]"));
					PNAddUpdate.click();
					
					WebElement PNDelete = driver.findElement(By.xpath("//*[@id=\"2135_anchor\"]/i[1]"));
					PNDelete.click();
					
					btnForms.click();
					
					WebElement Forms = driver.findElement(By.xpath("//*[@id=\"2136_anchor\"]/i[1]"));
					Forms.click();
					
					btnDMAS97.click();
					
					WebElement DMAS97 = driver.findElement(By.xpath("//*[@id=\"3078_anchor\"]/i[1]"));
					DMAS97.click();
					
					WebElement DMASViewOnly = driver.findElement(By.xpath("//*[@id=\"3079_anchor\"]/i[1]"));
					DMASViewOnly.click();
					
					WebElement DMASAddUpdate = driver.findElement(By.xpath("//*[@id=\"3080_anchor\"]/i[1]"));
					DMASAddUpdate.click();
					
					WebElement DMASDelete = driver.findElement(By.xpath("//*[@id=\"3081_anchor\"]/i[1]"));
					DMASDelete.click();
					
					
					WebElement FormsViewOnly = driver.findElement(By.xpath("//*[@id=\"2137_anchor\"]/i[1]"));
					FormsViewOnly.click();
					
					WebElement FormsAddUpdate = driver.findElement(By.xpath("//*[@id=\"2138_anchor\"]/i[1]"));
					FormsAddUpdate.click();
					
					WebElement PIDelete = driver.findElement(By.xpath("//*[@id=\"2043_anchor\"]/i[1]"));
					PIDelete.click();
					
					WebElement PIList = driver.findElement(By.xpath("//*[@id=\"2026_anchor\"]/i[1]"));
					PIList.click();
					
					WebElement PICalander = driver.findElement(By.xpath("//*[@id=\"2027_anchor\"]/i[1]"));
					PICalander.click();
					
					WebElement PISchedule = driver.findElement(By.xpath("//*[@id=\"2028_anchor\"]/i[1]"));
					PISchedule.click();
					
					btnScheduling.click();
					
					WebElement Scheduling = driver.findElement(By.xpath("//*[@id=\"2029_anchor\"]/i[1]"));
					Scheduling.click();
					
					WebElement VisiterAttendence = driver.findElement(By.xpath("//*[@id=\"3077_anchor\"]/i[1]"));
					VisiterAttendence.click();
					
					WebElement VirtualVisits = driver.findElement(By.xpath("//*[@id=\"3122_anchor\"]/i[1]"));
					VirtualVisits.click();
					
					WebElement ScheduleMaster = driver.findElement(By.xpath("//*[@id=\"2030_anchor\"]/i[1]"));
					ScheduleMaster.click();
					
					
					WebElement ScheduleLog = driver.findElement(By.xpath("//*[@id=\"2031_anchor\"]/i[1]"));
					ScheduleLog.click();
					
					WebElement PendingSchedule = driver.findElement(By.xpath("//*[@id=\"2079_anchor\"]/i[1]"));
					PendingSchedule.click();
					
					btnBillingClaimProcessing.click();
					
					WebElement Batch837 = driver.findElement(By.xpath("//*[@id=\"2064_anchor\"]/i[1]"));
					Batch837.click();
					
					WebElement Upload835 = driver.findElement(By.xpath("//*[@id=\"2065_anchor\"]/i[1]"));
					Upload835.click();
					
					WebElement ReconcilEOB = driver.findElement(By.xpath("//*[@id=\"2066_anchor\"]/i[1]"));
					ReconcilEOB.click();
					
					WebElement EDIFileLog = driver.findElement(By.xpath("//*[@id=\"2067_anchor\"]/i[1]"));
					EDIFileLog.click();
					
					WebElement myEZcareClearingHouse = driver.findElement(By.xpath("//*[@id=\"3067_anchor\"]/i[1]"));
					myEZcareClearingHouse.click();
					
					WebElement Eligbility = driver.findElement(By.xpath("//*[@id=\"2084_anchor\"]/i[1]"));
					Eligbility.click();
					
					btnMessages.click();
					
					WebElement Messages = driver.findElement(By.xpath("//*[@id=\"2032_anchor\"]/i[1]"));
					Messages.click();
					
					WebElement ReceivedMessage = driver.findElement(By.xpath("//*[@id=\"2033_anchor\"]/i[1]"));
					ReceivedMessage.click();
					
					WebElement SendMsg = driver.findElement(By.xpath("//*[@id=\"2034_anchor\"]/i[1]"));
					SendMsg.click();
					
					WebElement GroupSMS = driver.findElement(By.xpath("//*[@id=\"2035_anchor\"]/i[1]"));
					GroupSMS.click();
					
					WebElement BroadcastNotification = driver.findElement(By.xpath("//*[@id=\"2068_anchor\"]/i[1]"));
					BroadcastNotification.click();
					
					btnReports.click();
					
					WebElement Reports = driver.findElement(By.xpath("//*[@id=\"2036_anchor\"]/i[1]"));
					Reports.click();
					
					WebElement Report = driver.findElement(By.xpath("//*[@id=\"3059_anchor\"]/i[1]"));
					Report.click();
					
					WebElement EmpBillingReports = driver.findElement(By.xpath("//*[@id=\"2074_anchor\"]/i[1]"));
					EmpBillingReports.click();
					
					WebElement EmpVisitReports = driver.findElement(By.xpath("//*[@id=\"2075_anchor\"]"));
					EmpVisitReports.click();
					
					WebElement DMAS = driver.findElement(By.xpath("//*[@id=\"3019_anchor\"]/i[1]"));
					DMAS.click();
					
					WebElement WeeklyTimeSheet = driver.findElement(By.xpath("//*[@id=\"3020_anchor\"]/i[1]"));
					WeeklyTimeSheet.click();
					
					WebElement ActivePatient = driver.findElement(By.xpath("//*[@id=\"3038_anchor\"]/i[1]"));
					ActivePatient.click();
					
					WebElement EmpVisitReport = driver.findElement(By.xpath("//*[@id=\"3039_anchor\"]/i[1]"));
					EmpVisitReport.click();
					
					WebElement PatientHoursSummaryReport = driver.findElement(By.xpath("//*[@id=\"3040_anchor\"]/i[1]"));
					PatientHoursSummaryReport.click();
					
					WebElement EmpVisitRept = driver.findElement(By.xpath("//*[@id=\"3041_anchor\"]/i[1]"));
					EmpVisitRept.click();
					
					WebElement PatientVisitReport = driver.findElement(By.xpath("//*[@id=\"3042_anchor\"]/i[1]"));
					PatientVisitReport.click();
					
					WebElement CanApproveBypassClockinout = driver.findElement(By.xpath("//*[@id=\"2080_anchor\"]/i[1]"));
					CanApproveBypassClockinout.click();
					
					
				}
				
			/*..........Mobile Permission...........*/
				
				@FindBy(xpath="//h3[@class='page-title' and contains(text(),'Role Permission')]")
				WebElement rolePermisionLabel;

				@FindBy(xpath = "//*[@id=\"3024\"]/i")
				WebElement BtnEarlyClockin;

				@FindBy(xpath = "//*[@id=\"3024_anchor\"]/i[1]")
				WebElement EarlyClockin;
				
				@FindBy(xpath = "//*[@id=\"3025_anchor\"]/i[1]")
				WebElement fifteen_min;
				
				@FindBy(xpath = "//*[@id=\"3026_anchor\"]/i[1]")
				WebElement thirty_min;
				
				@FindBy(xpath = "//*[@id=\"3027_anchor\"]/i[1]")
				WebElement fortyfive_min;
				
				@FindBy(xpath = "//*[@id=\"3028_anchor\"]/i[1]")
				WebElement sixty_min;
				
				@FindBy(xpath = "//*[@id=\"3029_anchor\"]/i[1]")
				WebElement ninety_min;
				
				@FindBy(xpath = "//*[@id=\"3030_anchor\"]/i[1]")
				WebElement onetwenty_min;
				
				@FindBy(xpath = "//*[@id=\"3037_anchor\"]/i[1]")
				WebElement NoEarlyClockin;
				
				@FindBy(xpath = "//*[@id=\"3034_anchor\"]/i[1]")
				WebElement NoTimeLimit;
				
				@FindBy(xpath = "//*[@id=\"3031\"]/i")
				WebElement BtnTaskEntryType;
				
				@FindBy(xpath = "//*[@id=\"3031_anchor\"]/i[1]")
				WebElement TaskEntryType;
				
				@FindBy(xpath = "//*[@id=\"3032_anchor\"]/i[1]")
				WebElement Simple;
				
				@FindBy(xpath = "//*[@id=\"3033_anchor\"]/i[1]")
				WebElement Detail;
				
				@FindBy(xpath = "//*[@id=\"3068\"]/i")
				WebElement BtnCovidSurvey;
				
				@FindBy(xpath = "//*[@id=\"3068_anchor\"]/i[1]")
				WebElement CovidSurvey;
				
				@FindBy(xpath = "//*[@id=\"3069_anchor\"]/i[1]")
				WebElement Mandatory;
				
				@FindBy(xpath = "//*[@id=\"3070_anchor\"]/i[1]")
				WebElement NotRequired;
				
				@FindBy(xpath = "//*[@id=\"3071_anchor\"]/i[1]")
				WebElement Optional;
				
				@FindBy(xpath = "//*[@id=\"3001_anchor\"]/i[1]")
				WebElement Home;

				@FindBy(xpath = "//*[@id=\"3002\"]/i")
				WebElement BtnMessages;
				
				@FindBy(xpath = "//*[@id=\"3002_anchor\"]/i[1]")
				WebElement Messages;
				
				@FindBy(xpath = "//*[@id=\"3003_anchor\"]/i[1]")
				WebElement Received_msgs;
				
				@FindBy(xpath = "//*[@id=\"3004_anchor\"]/i[1]")
				WebElement Sent_msgs;
				
				@FindBy(xpath = "//*[@id=\"3005_anchor\"]/i[1]")
				WebElement Create_msgs;
				
				@FindBy(xpath = "//*[@id=\"3006_anchor\"]/i[1]")
				WebElement Clockin_Clockout_Time_Update;
				
				@FindBy(xpath = "//*[@id=\"3007_anchor\"]/i[1]")
				WebElement Patient_Service_Denied;

				@FindBy(xpath = "//*[@id=\"3016\"]/i")
				WebElement Btn_Auto_approved_Bypass_Permissions;
				
				@FindBy(xpath = "//*[@id=\"3016_anchor\"]/i[1]")
				WebElement Auto_approved_Bypass_Permissions;
				
				@FindBy(xpath = "//*[@id=\"3035_anchor\"]/i[1]")
				WebElement Early_Clockin_Auto_Approved;
				
				@FindBy(xpath = "//*[@id=\"3008_anchor\"]/i[1]")
				WebElement Auto_Approved_Bypass_Clockin_Clockout;
				
				@FindBy(xpath = "//*[@id=\"3012_anchor\"]/i[1]")
				WebElement Auto_Approved_IVR_Bypass_Clockin_Clockout;
				
				@FindBy(xpath = "//*[@id=\"3017\"]/i")
				WebElement Btn_Approval_Required_For_Bypass_Permissions;
				
				@FindBy(xpath = "//*[@id=\"3017_anchor\"]/i[1]")
				WebElement Approval_Required_For_Bypass_Permissions;

				@FindBy(xpath = "//*[@id=\"3018_anchor\"]/i[1]")
				WebElement Approval_Required_For_IVR_Bypass_Clockin_Clockout;
				
				@FindBy(xpath = "//*[@id=\"3015_anchor\"]/i[1]")
				WebElement Approval_Required_For_Bypass_Clockin_Clockout;
				
				@FindBy(xpath = "//*[@id=\"3036_anchor\"]/i[1]")
				WebElement Early_Clockin_Approval_Required;
				
				@FindBy(xpath = "//*[@id=\"3013_anchor\"]/i[1]")
				WebElement IVR_Instant_No_Schedule_Clockin_Clockout;
				
				@FindBy(xpath = "//*[@id=\"3009\"]/i")
				WebElement BtnVisitHistory;
				
				@FindBy(xpath = "//*[@id=\"3009_anchor\"]/i[1]")
				WebElement VisitHistory;
				
				@FindBy(xpath = "//*[@id=\"3010_anchor\"]/i[1]")
				WebElement Views_only;
				
				@FindBy(xpath = "//*[@id=\"3011_anchor\"]/i[1]")
				WebElement Update_tasks;
				
				@FindBy(xpath = "//*[@id=\"3014_anchor\"]/i[1]")
				WebElement can_update_location_coordinates;
				
				
					
				//Actions
					public void mobilePermission() 
					{
						BtnEarlyClockin.click();
						
						WebElement EarlyClockin = driver.findElement(By.xpath("//*[@id=\\\"3024_anchor\\\"]/i[1]"));
						EarlyClockin.click();
						
						WebElement fifteen_min = driver.findElement(By.xpath("//*[@id=\\\"3025_anchor\\\"]/i[1]"));
						fifteen_min.click();
						
						WebElement thirty_min = driver.findElement(By.xpath("//*[@id=\\\"3026_anchor\\\"]/i[1]"));
						thirty_min.click();
						
						WebElement fortyfive_min = driver.findElement(By.xpath("//*[@id=\\\"3027_anchor\\\"]/i[1]"));
						fortyfive_min.click();
						
						WebElement sixty_min = driver.findElement(By.xpath("//*[@id=\\\"3028_anchor\\\"]/i[1]"));
						sixty_min.click();
						
						WebElement ninety_min = driver.findElement(By.xpath("//*[@id=\\\"3029_anchor\\\"]/i[1]"));
						ninety_min.click();
						
						WebElement onetwenty_min = driver.findElement(By.xpath("//*[@id=\\\"3030_anchor\\\"]/i[1]"));
						onetwenty_min.click();
						
						WebElement NoEarlyClockin = driver.findElement(By.xpath("//*[@id=\\\"3037_anchor\\\"]/i[1]"));
						NoEarlyClockin.click();
						
						WebElement NoTimeLimit = driver.findElement(By.xpath("//*[@id=\\\"3034_anchor\\\"]/i[1]"));
						NoTimeLimit.click();
						
						BtnTaskEntryType.click();
						
						WebElement TaskEntryType = driver.findElement(By.xpath("//*[@id=\\\"3031_anchor\\\"]/i[1]"));
						TaskEntryType.click();
						
						WebElement Simple = driver.findElement(By.xpath("//*[@id=\\\"3032_anchor\\\"]/i[1]"));
						Simple.click();
						
						WebElement Detail = driver.findElement(By.xpath("//*[@id=\\\"3033_anchor\\\"]/i[1]"));
						Detail.click();
						
						BtnCovidSurvey.click();
						
						WebElement CovidSurvey = driver.findElement(By.xpath("//*[@id=\\\"3068_anchor\\\"]/i[1]"));
						CovidSurvey.click();
						
						WebElement Mandatory = driver.findElement(By.xpath("//*[@id=\\\"3069_anchor\\\"]/i[1]"));
						Mandatory.click();
						
						WebElement NotRequired = driver.findElement(By.xpath("//*[@id=\\\"3070_anchor\\\"]/i[1]"));
						NotRequired.click();
						
						WebElement Optional = driver.findElement(By.xpath("//*[@id=\\\"3071_anchor\\\"]/i[1]"));
						Optional.click();
				
						WebElement Home = driver.findElement(By.xpath("//*[@id=\\\"3001_anchor\\\"]/i[1]"));
						Home.click();
						
						BtnMessages.click();
						
						WebElement Messages = driver.findElement(By.xpath("//*[@id=\\\"3002_anchor\\\"]/i[1]"));
						Messages.click();
						
						WebElement Received_msgs = driver.findElement(By.xpath("//*[@id=\\\"3003_anchor\\\"]/i[1]"));
						Received_msgs.click();
						
						WebElement Sent_msgs = driver.findElement(By.xpath("//*[@id=\\\"3004_anchor\\\"]/i[1]"));
						Sent_msgs.click();
						
						WebElement Create_msgs = driver.findElement(By.xpath("//*[@id=\\\"3005_anchor\\\"]/i[1]"));
						Create_msgs.click();
						
						WebElement Clockin_Clockout_Time_Update = driver.findElement(By.xpath("//*[@id=\\\"3006_anchor\\\"]/i[1]"));
						Clockin_Clockout_Time_Update.click();
						
						WebElement Patient_Service_Denied = driver.findElement(By.xpath("//*[@id=\\\"3007_anchor\\\"]/i[1]"));
						Patient_Service_Denied.click();
				
						Btn_Auto_approved_Bypass_Permissions.click();
						
						WebElement Auto_approved_Bypass_Permissions = driver.findElement(By.xpath("//*[@id=\\\"3016_anchor\\\"]/i[1]"));
						Auto_approved_Bypass_Permissions.click();
						
						WebElement Early_Clockin_Auto_Approved = driver.findElement(By.xpath("//*[@id=\\\"3035_anchor\\\"]/i[1]"));
						Early_Clockin_Auto_Approved.click();
						
						WebElement Auto_Approved_Bypass_Clockin_Clockout = driver.findElement(By.xpath("//*[@id=\\\"3008_anchor\\\"]/i[1]"));
						Auto_Approved_Bypass_Clockin_Clockout.click();
						
						WebElement Auto_Approved_IVR_Bypass_Clockin_Clockout = driver.findElement(By.xpath("//*[@id=\\\"3012_anchor\\\"]/i[1]"));
						Auto_Approved_IVR_Bypass_Clockin_Clockout.click();
				
						Btn_Approval_Required_For_Bypass_Permissions.click();
						
						WebElement Approval_Required_For_Bypass_Permissions = driver.findElement(By.xpath("//*[@id=\\\"3017_anchor\\\"]/i[1]"));
						Approval_Required_For_Bypass_Permissions.click();
						
						WebElement Approval_Required_For_IVR_Bypass_Clockin_Clockout = driver.findElement(By.xpath("//*[@id=\\\"3018_anchor\\\"]/i[1]"));
						Approval_Required_For_IVR_Bypass_Clockin_Clockout.click();
						
						WebElement Approval_Required_For_Bypass_Clockin_Clockout = driver.findElement(By.xpath("//*[@id=\\\"3015_anchor\\\"]/i[1]"));
						Approval_Required_For_Bypass_Clockin_Clockout.click();
						
						WebElement Early_Clockin_Approval_Required = driver.findElement(By.xpath("//*[@id=\\\"3036_anchor\\\"]/i[1]"));
						Early_Clockin_Approval_Required.click();
						
						WebElement IVR_Instant_No_Schedule_Clockin_Clockout = driver.findElement(By.xpath("//*[@id=\\\"3013_anchor\\\"]/i[1]"));
						IVR_Instant_No_Schedule_Clockin_Clockout.click();
						
						BtnVisitHistory.click();
						
						WebElement VisitHistory = driver.findElement(By.xpath("//*[@id=\\\"3009_anchor\\\"]/i[1]"));
						VisitHistory.click();
						
						WebElement Views_only = driver.findElement(By.xpath("//*[@id=\\\"3010_anchor\\\"]/i[1]"));
						Views_only.click();
						
						WebElement Update_tasks = driver.findElement(By.xpath("//*[@id=\\\"3011_anchor\\\"]/i[1]"));
						Update_tasks.click();
						
						WebElement can_update_location_coordinates = driver.findElement(By.xpath("//*[@id=\\\"3014_anchor\\\"]/i[1]"));
						can_update_location_coordinates.click();
					
					}		

}
