package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class BillingPage extends TestBase 
{
	@FindBy(xpath="/html/body/div[1]/div[5]/div[2]/div/div/div[1]/h3")//selector":"/html/body/div[1]/div[5]/div[2]/div/div/div[1]/h3
	WebElement InvoiceListLabel;
	// Initializing the Page Objects:
	public BillingPage() 
	{
		PageFactory.initElements(driver, this);
	}

	public static String validateInvoiceListPageTitle()
	{
		return driver.getTitle();
	}

}
