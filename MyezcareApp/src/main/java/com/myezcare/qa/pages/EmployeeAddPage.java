package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class EmployeeAddPage extends TestBase
{
	@FindBy(xpath="/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[4]/a")
	WebElement employeeInformationLabel;
	
	@FindBy(xpath="//*[@id='Employee_EmployeeUniqueID']")
	WebElement employeeID;
	
	@FindBy(xpath="//*[@id='Employee_FirstName']")
	WebElement employeeFirtName;
	
	@FindBy(xpath="//*[@id='Employee_LastName']")
	WebElement employeeLastName;

	@FindBy(xpath="//*[@id='Employee_Email']")
	WebElement employeeEmailID;
	
	@FindBy(xpath="//*[(text()='Select Organizations')]")
	WebElement associatedWith;
	
	@FindBy(xpath="//*[@id='Employee_Address']")
	WebElement employeeAddress;
	
	@FindBy(xpath="//*[@id='Employee_City']")
	WebElement employeeCity;
	
	@FindBy(xpath="//*[@id='Employee_ZipCode']")
	WebElement employeeZipcode;
	
	@FindBy(xpath="//*[@id='Employee_StateCode']")
	WebElement employeeStatecode;
	
	@FindBy(xpath="//*[@id=\"addEmployee_EmployeeDetail\"]/input")////*[@id="addEmployee_EmployeeDetail"]/input
	WebElement nextBtn;
	
	@FindBy(xpath="//*[@id=\"tab_EmployeeInformation\"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/ul/li[1]/a/span[1]")
	WebElement homecare;
	
	@FindBy(xpath="//*[@id=\"addEmployee_EmployeeDetail\"]")
	WebElement employeeDetails;
	
	@FindBy(xpath="//*[@id=\"tab_EmployeeInformation\"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/div/input")
	WebElement associateTypeSearch;
	
	@FindBy(xpath="//*[@id=\"tab_EmployeeInformation\"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/ul/li[1]/a/span[1]")
	WebElement homecaresearch;
	
	// Initializing the Page Objects:
	public EmployeeAddPage() 
	{
		PageFactory.initElements(driver, this);
		
	}
	//Actions
	public static String validateAddEmployeePageTitle(){
		return driver.getTitle();
	}

	public boolean verifyEmployeeInformationLabel()
	{
		return employeeInformationLabel.isDisplayed();
		
	}
	public void createNewContact() throws InterruptedException
	{
		employeeID.sendKeys("111");
		employeeFirtName.sendKeys("Amit");
		employeeLastName.sendKeys("Singh");
		employeeEmailID.sendKeys("abc@gmail.com");
		Thread.sleep(7000);
		associatedWith.click();
		Thread.sleep(7000);
		associateTypeSearch.sendKeys("No");
		/*Thread.sleep(6000);
		associateTypeSearch.click();*/
		
		
		
		/*Select select = new Select(driver.findElement(By.xpath("//*[@id=\"tab_EmployeeInformation\"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/div/input")));
		select.selectByVisibleText(assowith);*/
		homecaresearch.click();
		////*[@id="tab_EmployeeInformation"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/ul/li[1]/a/span[1]
		////*[@id="tab_EmployeeInformation"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/ul/li[2]/a/span[1]
		////*[@id="tab_EmployeeInformation"]/div[1]/div/div/div[2]/div[2]/div[1]/div[4]/div/div/div/div/div/input

		
		
		employeeAddress.sendKeys("Bihar");
		employeeCity.sendKeys("Patna");
		employeeZipcode.sendKeys("123456");
		
		Select select1 = new Select(driver.findElement(By.xpath("//*[@id='Employee_StateCode']")));
		select1.selectByVisibleText("st");
		Thread.sleep(8000);
		//nextBtn.click();
		Actions action = new Actions(driver);
		action.moveToElement(employeeDetails).build().perform();
		Thread.sleep(6000);
	
		employeeDetails.click();
		
	}



}
