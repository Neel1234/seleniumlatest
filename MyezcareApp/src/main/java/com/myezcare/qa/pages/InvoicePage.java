package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class InvoicePage extends TestBase
{
	public InvoicePage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateInvoicePageTitle()
	{
		return driver.getTitle();
	}
}
