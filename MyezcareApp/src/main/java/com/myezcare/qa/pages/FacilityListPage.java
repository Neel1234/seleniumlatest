package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class FacilityListPage extends TestBase
{
	public FacilityListPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[3]/a/span[1]")
	WebElement FacilityHouse;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[3]/ul/li[2]/a")
	WebElement List;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[1]/button")
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]")
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[3]/a")
	WebElement FacilityHousebtn;
	
	@FindBy(xpath = "//*[@id=\"SelectAllFacilityHouse\"]")
	WebElement CheckBox;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_FacilityName\"]")
	WebElement Name;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_ListOfIdsInCsv\"]")
	WebElement Licensure;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_StartDate\"]")
	WebElement LRDate;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_NPI\"]")
	WebElement NPI;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_AHCCCSID\"]")
	WebElement AHCCCSID;
	
	@FindBy(xpath = "//*[@id=\"SearchFacilityHouseModel_EIN\"]")
	WebElement EIN;
	
	@FindBy(xpath = "//*[@id=\"frmfaclilityhouselist\"]/table/tbody/tr[1]/td[8]/input")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmfaclilityhouselist\"]/table/tbody/tr[1]/td[8]/a")
	WebElement Reset;
	
	
	public static String validateFacilityListPageTitle()
	{
		return driver.getTitle();
	}
}
