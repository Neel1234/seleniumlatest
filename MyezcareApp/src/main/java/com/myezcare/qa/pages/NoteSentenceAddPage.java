package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class NoteSentenceAddPage extends TestBase
{
	public NoteSentenceAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateNoteSentenceAddPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[17]/a/span[1]")
	WebElement NoteSentence;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[17]/ul/li[1]/a/span")
	WebElement Add;
	
	@FindBy(xpath = "//*[@id=\"NoteSentenceTitle\"]")
	WebElement NoteSentenceTitle;
	
	@FindBy(xpath = "//*[@id=\"NoteSentenceDetails\"]")
	WebElement NoteSentenceDetails;
	
	@FindBy(xpath = "//*[@id=\"frmNoteSentence\"]/div[2]/div/div[2]/input[1]")
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmNoteSentence\"]/div[2]/div/div[2]/input[2]")
	WebElement Cancel;
	
	
	public void addNoteSentences(String NSTitle, String NSDetail)throws InterruptedException
	{
		NoteSentenceTitle.sendKeys(NSTitle);
		NoteSentenceDetails.sendKeys(NSDetail);
		Thread.sleep(7000);
		Save.click(); 
		Thread.sleep(7000);
		Cancel.click();
	}
	
}
