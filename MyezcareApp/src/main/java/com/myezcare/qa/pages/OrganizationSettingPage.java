package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class OrganizationSettingPage extends TestBase
{
	
	@FindBy(xpath="//*[@id=\"org_Settings\"]")
	WebElement organizationSettingLabel;
	
	// Initializing the Page Objects:
				public OrganizationSettingPage() 
				{
					PageFactory.initElements(driver, this);
				}
				public boolean verifyOrganizationSettingLabel()
				{
					return organizationSettingLabel.isDisplayed();
				}
		
}
