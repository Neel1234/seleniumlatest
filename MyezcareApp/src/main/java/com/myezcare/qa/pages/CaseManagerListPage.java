package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class CaseManagerListPage extends TestBase 
{
	public CaseManagerListPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[2]/a" )
	WebElement CaseManager;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[2]/ul/li[2]/a" )
	WebElement List;
	
	//Form Section
	
	@FindBy(xpath = "//*[@id=\"SelectAllCaseManager\"]" )
	WebElement Checkbox;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[2]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/a" )
	WebElement CaseManager1;
	
	@FindBy(xpath = "//*[@id=\"SearchCaseManagerListPage_Name\"]" )
	WebElement Name;
	
	@FindBy(xpath = "//*[@id=\"SearchCaseManagerListPage_Email\"]" )
	WebElement Email;
	
	@FindBy(xpath = "//*[@id=\"SearchCaseManagerListPage_AgencyID\"]" )
	WebElement Agency;
	
	@FindBy(xpath = "//*[@id=\"SearchCaseManagerListPage_Phone\"]" )
	WebElement Phone;
	
	@FindBy(xpath = "//*[@id=\"frmCaseManagerList\"]/table/tbody/tr[1]/td[7]/input" )
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmCaseManagerList\"]/table/tbody/tr[1]/td[7]/a" )
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"frmCaseManagerList\"]/table/tbody/tr[2]/td[7]/a[1]/i" )
	WebElement Edit;
	
	//Actions

	public static String validateCaseManagerListPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyCaseManagerListLabel()
	{
		return List.isDisplayed();
	}
	public void selectMultipleselectCaseManager() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String Agency_Location) throws Exception
	{
		Select location=new Select(Agency);
		location.selectByVisibleText(Agency_Location);
		Thread.sleep(5000);
		Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
		
	}
	public CaseManagerAddPage validateAddCaseManagerBtn() throws Exception
	{
		Thread.sleep(5000);
		CaseManager1.click();
		return new CaseManagerAddPage();
	}
	
	public void validateResetBtn(String name, String email, String phone) throws Exception
	{
		Name.sendKeys(name);
		Thread.sleep(3000);
		Email.sendKeys(email);
		Thread.sleep(3000);
		Phone.sendKeys(phone);
		Thread.sleep(5000);
		Reset.click();			
	}
	public void validateEditBtn(String name) throws Exception 
	{
		Name.sendKeys(name);
		Thread.sleep(5000);
		Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
