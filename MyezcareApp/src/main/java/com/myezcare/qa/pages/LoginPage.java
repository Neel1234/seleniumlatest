package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class LoginPage extends TestBase
{
	//page factory = OR;
	
	@FindBy(name="UserName")
	WebElement username;
	
	@FindBy(name="Password")
	WebElement password;

	@FindBy(xpath="//button[@class='btn uppercase btn-block btn-cstm']")
	WebElement loginBtn;
	
	@FindBy(xpath="/html/body/div[1]/div/section/div/div[2]/div[1]/img")
	WebElement myzLogo;
	
	//Initializing page objects
	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	//Actions
	public boolean validatemyzcarelogo()
	{
		return myzLogo.isDisplayed();
	}
	public HomePage login(String un,String pwd)
	{
		username.sendKeys(un);
		password.sendKeys(pwd);
		loginBtn.click();
		return new HomePage();
	}
	
	
}
