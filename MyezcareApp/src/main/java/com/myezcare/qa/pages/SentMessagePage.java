package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class SentMessagePage extends TestBase 
{
	public SentMessagePage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateSentMessagePageTitle()
	{
		return driver.getTitle();
	}
}
