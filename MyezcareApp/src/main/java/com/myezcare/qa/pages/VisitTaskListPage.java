package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class VisitTaskListPage extends TestBase
{
	public VisitTaskListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateVisitTaskListPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[13]/a/span[1]")
	WebElement VisitTask;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[13]/ul/li[2]/a")
	WebElement List;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[1]/button")
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]")
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[3]/button/span")
	WebElement CloneTask;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[4]/a")
	WebElement AddVisitTask;
	
	@FindBy(xpath = "//*[@id=\"uniform-SelectAllDxCode\"]/span")
	WebElement CheckBox;
	
	@FindBy(xpath = "//*[@id=\"SearchVisitTaskListPage_VisitTaskDetail\"]")
	WebElement TaskDetail;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTaskList \"]/div/table/tbody/tr[1]/td[3]")
	WebElement Category;
	
	@FindBy(xpath = "//*[@id=\"SearchVisitTaskListPage_VisitTaskType\"]")
	WebElement TaskType;
	
	@FindBy(xpath = "//*[@id=\"SearchVisitTaskListPage_VisitTaskVisitTypeID\"]")
	WebElement VisitType;
	
	@FindBy(xpath = "//*[@id=\"SearchVisitTaskListPage_VisitTaskCareTypeID\"]")
	WebElement CareType;
	
	@FindBy(xpath = "//*[@id=\"SearchVisitTaskListPage_ServiceCode\"]")
	WebElement ServiceCode;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTaskList \"]/div/table/tbody/tr[1]/td[11]/input")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTaskList \"]/div/table/tbody/tr[1]/td[11]/a")
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTaskList \"]/div/table/tbody/tr[2]/td[11]/a[1]/i")
	WebElement Update;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTaskList \"]/div/table/tbody/tr[2]/td[11]/a[3]")
	WebElement Delete;
	
	public void visitTaskList(String taskDetail, String category, String visitType, String taskType, String careType, String serviceCode)throws InterruptedException
	{
		Refresh.click();
		Thread.sleep(7000);
		Active.isSelected();
		CheckBox.isSelected();
		TaskDetail.sendKeys(taskDetail);
		Category.sendKeys(category);
		TaskType.sendKeys(taskType);
		CareType.sendKeys(careType);
		ServiceCode.sendKeys(serviceCode);
		Search.click();
		Thread.sleep(7000);
		Update.click();
	}
}
