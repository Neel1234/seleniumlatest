package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PatientAddPage extends TestBase 
{

	@FindBy(xpath="/html/body/div[1]/div[5]/div[2]/div/div/div[1]/h3")
	WebElement patientAddLabel;
	
	// Initializing the Page Objects:
	public PatientAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public boolean verifyAddpatientLabel()
	{
		return patientAddLabel.isDisplayed();
	}
}
