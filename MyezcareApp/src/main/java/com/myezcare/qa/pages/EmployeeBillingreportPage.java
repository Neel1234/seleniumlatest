package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class EmployeeBillingreportPage extends TestBase
{
	public EmployeeBillingreportPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateEmployeeBillingreportPageTitle()
	{
		return driver.getTitle();
	}
}
