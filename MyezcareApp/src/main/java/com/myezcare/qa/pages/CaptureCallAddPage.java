package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class CaptureCallAddPage extends TestBase
{
	public CaptureCallAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateCaptureCallAddPageTitle()
	{
		return driver.getTitle();
	}
}
