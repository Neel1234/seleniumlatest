package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PatientCalendarPage extends TestBase
{
	public PatientCalendarPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validatePatientCalendarTitle()
	{
		return driver.getTitle();
	}

}
