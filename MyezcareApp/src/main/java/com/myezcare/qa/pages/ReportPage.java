package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class ReportPage extends TestBase{
	public ReportPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateReportPageTitle()
	{
		return driver.getTitle();
	}
}
