package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class PreferenceAddPage extends TestBase
{
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[11]/a" )
	WebElement Preference;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[11]/ul/li[1]/a" )
	WebElement Add;
	
	//Form Section
	
	@FindBy(xpath = "//*[@id=\"Preference_KeyType\"]" )
	WebElement Type;
	
	@FindBy(xpath = "//*[@id=\"Preference_PreferenceName\"]" )
	WebElement Name;
	
	@FindBy(xpath = "//*[@id=\"frmPreference\"]/div[2]/div/div[2]/input" )
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmPreference\"]/div[2]/div/div[2]/a/input" )
	WebElement Cancel;
	
	// Initializing the Page Objects:
	
	public PreferenceAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	
//Actions
	public static String validatePreferenceAddPageTitle(){
		return driver.getTitle();
	}
	public void PreferenceAdd(String type, String name) throws InterruptedException
	{
		Select select1 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Preference_KeyType\\\"]")));
		select1.selectByVisibleText(type);
		Name.sendKeys(name);
		Save.click();
		Thread.sleep(6000);
		Cancel.click();
	}
}
