package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PhysicianAddPage extends TestBase
{
	public PhysicianAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validatePhysicianAddPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[10]/a/span[1]")
	WebElement Physician;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[10]/ul/li[1]/a")
	WebElement Add;
	
	@FindBy(xpath = "//*[@id=\"display\"]")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"Physician_FirstName\"]")
	WebElement FirstName;
	
	@FindBy(xpath = "//*[@id=\"Physician_MiddleName\"]")
	WebElement MiddleName;
	
	@FindBy(xpath = "//*[@id=\"Physician_LastName\"]")
	WebElement LastName;
	
	@FindBy(xpath = "//*[@id=\"Physician_Address\"]")
	WebElement Address;
	
	@FindBy(xpath = "//*[@id=\"Physician_City\"]")
	WebElement City;
	
	@FindBy(xpath = "//*[@id=\"Physician_StateCode\"]")
	WebElement State;
	
	@FindBy(xpath = "//*[@id=\"Physician_ZipCode\"]")
	WebElement ZipCode;
	
	@FindBy(xpath = "//*[@id=\"Physician_Email\"]")
	WebElement Email;
	
	@FindBy(xpath = "//*[@id=\"Physician_Phone\"]")
	WebElement Phone;
	
	@FindBy(xpath = "//*[@id=\"Physician_Mobile\"]")
	WebElement Mobile;
	
	@FindBy(xpath = "//*[@id=\"NPI\"]")
	WebElement NPI;
	
	@FindBy(xpath = "//*[@id=\"display\"]")
	WebElement Specialist;
	
	@FindBy(xpath = "//*[@id=\"frmAddPhysician\"]/div[2]/input")
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmAddPhysician\"]/div[2]/a/input")
	WebElement Cancel;
	
	public void physicianAdd()throws InterruptedException
	{
		FirstName.sendKeys("Amit");
		MiddleName.sendKeys("Kumar");
		LastName.sendKeys("upadhyay");
		Address.sendKeys("Mirzapur");
		City.sendKeys("Mirzapur");
		State.sendKeys("UP");
		ZipCode.sendKeys("123456");
		Email.sendKeys("abvc@gmail.com");
		Phone.sendKeys("1234567");
		Mobile.sendKeys("0987654321");
		NPI.sendKeys("NO");
		Specialist.sendKeys("Yes");
		Save.click();
		Thread.sleep(7000);
		Cancel.click();
	}
	
}
