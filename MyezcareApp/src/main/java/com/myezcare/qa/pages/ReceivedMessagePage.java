package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class ReceivedMessagePage extends TestBase
{
	public ReceivedMessagePage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateReceivedMessagePageTitle()
	{
		return driver.getTitle();
	}
}
