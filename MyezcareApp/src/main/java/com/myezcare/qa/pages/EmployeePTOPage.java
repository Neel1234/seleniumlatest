package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class EmployeePTOPage extends TestBase
{
	
	@FindBy(xpath="//button[@class='letter-space btn btn-sm green-jungle']")
	WebElement personalTimeOffBtn;
	
	@FindBy(xpath="//input[@id='SelectAllEmployeeDayOff' and @type='checkbox']")
	WebElement multiselectBtn;
	
	@FindBy(xpath="//select[@name='ETSModelEmployee']")
	WebElement selectEmp;
	
	@FindBy(xpath="//input[@type='submit' and @value='Search']")
	WebElement searchBtn;
	
	@FindBy(xpath="//a[@class='btn btn-default grey border-grey-cascade btn-xs' and text()='Reset']")
	WebElement resetBtn;

	// Initializing the Page Objects:
	public EmployeePTOPage() 
	{
		PageFactory.initElements(driver, this);
		
	}
	//Actions
	public static String validateEmployeePTOpageTitle()
	{
		return driver.getTitle();
	}
	public PersonalTimeOffPage validatePTOBtn() throws Exception
	{
		Thread.sleep(5000);
		personalTimeOffBtn.click();
		return new PersonalTimeOffPage();
	}
	public void validatemultiselectionBtn() throws Exception
	{
		Thread.sleep(5000);
		multiselectBtn.click();
		
	}
	public void validatesearchBtn(int empName) throws Exception
	{
		Select emp=new Select(selectEmp);
		emp.selectByIndex(empName);
		Thread.sleep(5000);
		searchBtn.click();
	}
	public void validateresetBtn(int empName) throws Exception
	{
		Select emp=new Select(selectEmp);
		emp.selectByIndex(empName);
		Thread.sleep(5000);
		searchBtn.click();
		Thread.sleep(5000);
		resetBtn.click();
	}
	
}
