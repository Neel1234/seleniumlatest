package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class MyprofilePage extends TestBase
{
	@FindBy(xpath="//*[@id=\"frmEditProfile\"]/div[2]/div/div[1]/div/div/div[1]")
	WebElement myProfileLabel;
	
	@FindBy(xpath="//a[@class='btn btn-sm red-mint pull-right letter-space margin-right-10' and contains(text(),'Cancel')]")
	WebElement myprofileCancelBtn;
	
	// Initializing the Page Objects:
			public MyprofilePage() 
			{
				PageFactory.initElements(driver, this);
			}
			
			public boolean verifyEditProfileLabel()
			{
				return myProfileLabel.isDisplayed();
			}
			public HomePage validateCancelBtn() throws InterruptedException
			{
				Thread.sleep(4000);
				myprofileCancelBtn.click();
				return new HomePage();
			}
			
		@FindBy(xpath = "/html/body/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/a/span")
		WebElement Master;
		
		@FindBy(xpath = "/html/body/div[1]/div[1]/div[1]/div/div[3]/ul/li[2]/ul/li[1]/a")
		WebElement MyProfile;

		@FindBy(xpath = "//*[@id=\"frmEditProfile\"]/div[1]/span/a")
		WebElement btnCancel;

		
		@FindBy(xpath = "//*[@id=\"frmEditProfile\"]/div[1]/span/input")
		WebElement btnSave;

		
		@FindBy(xpath = "//*[@id=\"ProfileImageUpload\"]")
		WebElement btnUploadImage;

		
		@FindBy(xpath = "//*[@id=\"Employee_FirstName\"]")
		WebElement FirstName;

		
		@FindBy(xpath = "//*[@id=\"Employee_MiddleName\"]")
		WebElement MiddleName;

		
		@FindBy(xpath = "//*[@id=\"Employee_LastName\"]")
		WebElement LastName;

		
		@FindBy(xpath = "//*[@id=\"Employee_Email\"]")
		WebElement Email;

		
		@FindBy(xpath = "//*[@id=\"Employee_PhoneWork\"]")
		WebElement WorkPhone;

		
		@FindBy(xpath = "//*[@id=\"Employee_PhoneHome\"]")
		WebElement MobilePhone;

		
		@FindBy(xpath = "//*[@id=\"Employee_UserName\"]")
		WebElement Username;
		
		@FindBy(xpath = "//*[@id=\"Employee_NewPassword\"]")
		WebElement Password;
		
		@FindBy(xpath = "//*[@id=\"Employee_TempConfirmPassword\"]")
		WebElement ConfirmPassword;
		
		@FindBy(xpath = "//*[@id=\"Employee_SecurityQuestionID\"]")
		WebElement SecurityQuestion;
		
		@FindBy(xpath = "//*[@id=\"Employee_SecurityAnswer\"]")
		WebElement SecurityAnswer;
		
		@FindBy(xpath = "//*[@id=\"frmEditProfile\"]/div[2]/div/div[2]/input")
		WebElement Save;
		
		@FindBy(xpath = "//*[@id=\"frmEditProfile\"]/div[2]/div/div[2]/a")
		WebElement Cancel;
		
		public void myProfilePage(String fName, String mName, String lName, String email, String wPhone, String mobilePhone,
				String username, String password, String confirmPassword, String securityQuestion, String securityAnswer)throws InterruptedException
		{
			Master.click();
			MyProfile.click();
			Thread.sleep(7000);
			btnUploadImage.click();
			FirstName.sendKeys(fName);
			MiddleName.sendKeys(mName);
			LastName.sendKeys(lName);
			Email.sendKeys(email);
			WorkPhone.sendKeys(wPhone);
			MobilePhone.sendKeys(mobilePhone);
			Username.sendKeys(username);
			Password.sendKeys(password);
			ConfirmPassword.sendKeys(confirmPassword);
			
			Select select1 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Employee_SecurityQuestionID\\\"]")));
			select1.selectByVisibleText(securityQuestion);
			
			SecurityAnswer.sendKeys(securityAnswer);
			Save.click();
			Thread.sleep(7000);
			Cancel.click();
		}

}
