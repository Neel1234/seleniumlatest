package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PhysicianListPage extends TestBase
{
	public PhysicianListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validatePhysicianListPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[10]/a/span[1]")
	WebElement Physician;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[10]/ul/li[2]/a/span")
	WebElement List;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[1]/button")
	WebElement Refresh;
	
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]")
	WebElement Status ;
	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[3]/a")
	WebElement AddPhysician;
	
	@FindBy(xpath = "//*[@id=\"uniform-SelectAllDxCode\"]/span")
	WebElement CheckBox;
	
	@FindBy(xpath = "//*[@id=\"SearchPhysicianListPage_PhysicianName\"]")
	WebElement PhysicianName;
	
	@FindBy(xpath = "//*[@id=\"SearchPhysicianListPage_PhysicianTypeID\"]")
	WebElement PhysicianType;
	
	@FindBy(xpath = "//*[@id=\"SearchPhysicianListPage_NPINumber\"]")
	WebElement NPINumber;
	
	@FindBy(xpath = "//*[@id=\"SearchPhysicianListPage_Address\"]")
	WebElement Address;
	
	@FindBy(xpath = "//*[@id=\"SearchPhysicianListPage_Email\"]")
	WebElement Email;
	
	@FindBy(xpath = "//*[@id=\"frmDxCodeList \"]/div/table/tbody/tr/td[7]/input")
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmDxCodeList \"]/div/table/tbody/tr/td[7]/a")
	WebElement Reset;
	
	
	//Action..
	public void physicianListPage(String pName, String pType, String nPINumber,String email, String address)throws InterruptedException
	{
		CheckBox.isSelected();
		PhysicianName.sendKeys(pName);
		PhysicianType.sendKeys(pType);
		NPINumber.sendKeys(nPINumber);
		Email.sendKeys(email);
		Address.sendKeys(address);
		Search.click();
		Thread.sleep(7000);
		Reset.click();
		
	}
	
	}
