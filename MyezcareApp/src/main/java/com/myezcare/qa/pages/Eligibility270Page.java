package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class Eligibility270Page extends TestBase {
	public Eligibility270Page()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateEligibility270PageTitle()
	{
		return driver.getTitle();
	}
}
