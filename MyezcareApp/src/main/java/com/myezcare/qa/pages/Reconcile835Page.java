package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class Reconcile835Page extends TestBase
{
	public Reconcile835Page()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateReconcile835PageTitle()
	{
		return driver.getTitle();
	}
}
