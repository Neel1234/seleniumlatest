package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class EmployeeVisitreportPage extends TestBase
{
	public EmployeeVisitreportPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateEmployeeVisitreportPageTitle()
	{
		return driver.getTitle();
	}
}
