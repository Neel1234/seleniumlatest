package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class PayorListPage extends TestBase
{
	public PayorListPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[9]/a" )
	WebElement Payor1;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[9]/ul/li[2]/a" )
	WebElement List;
	
	//Inside Page
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div[1]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div/div[3]/a" )
	WebElement Payor;
	
	@FindBy(xpath = "//*[@id=\"uniform-SelectAllPayor\"]/span" )
	WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_PayorName\"]" )
	WebElement PayorName;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_ShortName\"]" )
	WebElement ShortName;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_Address\"]" )
	WebElement Address;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_PayorIdentificationNumber\"]" )
	WebElement PayorIdentificationNumber;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_NPINumber\"]" )
	WebElement NPINumber;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_PayerGroup\"]" )
	WebElement PayerGroup;
	
	@FindBy(xpath = "//*[@id=\"SearchPayorListPage_PayorBillingType\"]" )
	WebElement PayorBillingType;
	
	@FindBy(xpath = "//*[@id=\"frmPayorList\"]/table/tbody/tr[1]/td[9]/input" )
	WebElement Action_Search;
	
	@FindBy(xpath = "//*[@id=\"frmPayorList\"]/table/tbody/tr[1]/td[9]/a" )
	WebElement Action_Reset;
	
	@FindBy(xpath = "//*[@id=\"frmPayorList\"]/table/tbody/tr[2]/td[9]/a[1]/i" )
	WebElement Edit;
	
	//Actions
	public static String validatePayorListPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyPayorListLabel()
	{
		return List.isDisplayed();
	}
	public void selectMultipleselectPayor() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String Billing_Type) throws Exception
	{
		Select type=new Select(PayorBillingType);
		type.selectByVisibleText(Billing_Type);
		Thread.sleep(5000);
		Action_Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
		
	}
	public PayorAddPage validateAddPayorBtn() throws Exception
	{
		Thread.sleep(5000);
		Payor.click();
		return new PayorAddPage();
	}
	
	public void validateResetBtn(String name, String sName, String add, String idNo, String npiNo) throws Exception
	{
		PayorName.sendKeys(name);
		Thread.sleep(3000);
		ShortName.sendKeys(sName);
		Thread.sleep(3000);
		Address.sendKeys(add);
		Thread.sleep(3000);
		PayorIdentificationNumber.sendKeys(idNo);
		Thread.sleep(3000);
		NPINumber.sendKeys(npiNo);
		Thread.sleep(5000);
		Action_Reset.click();			
	}
	public void validateEditBtn(String name) throws Exception 
	{
		PayorName.sendKeys(name);
		Thread.sleep(5000);
		Action_Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
