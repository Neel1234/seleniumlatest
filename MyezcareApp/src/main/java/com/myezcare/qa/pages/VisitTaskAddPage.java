package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class VisitTaskAddPage extends TestBase
{
	public VisitTaskAddPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateVisitTaskAddPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]")
	WebElement Settings;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[13]/a/span[1]")
	WebElement VisitTask;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[13]/ul/li[1]/a/span")
	WebElement Add;
	
	@FindBy(xpath = "//*[@id=\"VisitTask_VisitTaskType\"]")
	WebElement TaskType;
	
	@FindBy(xpath = "//*[@id=\"VisitTask_TaskCode\"]")
	WebElement TaskCode;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTask\"]/div[2]/div/div[1]/div/div/div[2]/div/div[3]/div[1]/select")
	WebElement Category;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTask\"]/div[2]/div/div[1]/div/div/div[2]/div/div[3]/div[1]/select")
	WebElement SubCategry;
	
	@FindBy(xpath = "//*[@id=\"VisitTask_VisitTaskDetail\"]")
	WebElement TaskDetails;
	
	@FindBy(xpath = "//*[@id=\"IsRequired\"]")
	WebElement IsRequired;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTask\"]/div[2]/div/div[1]/div/div/div[2]/div/div[8]/div/select")
	WebElement DefaultFrequency;
	
	@FindBy(xpath = "//*[@id=\"IsDefault\"]")
	WebElement IsDefault;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTask\"]/div[2]/div/div[2]/input")
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmVisitTask\"]/div[2]/div/div[2]/a/input")
	WebElement Cancel;
	
	
	public void visitTaskAction( String TCode, String TCategory,String TSubCategory,String TDetail,
			String isReqd,String isDefault )throws InterruptedException
	{
		TaskCode.sendKeys(TCode);
		//Category.click();
		Select select1 = new Select(driver.findElement(By.xpath("//*[@id=\\\"frmVisitTask\\\"]/div[2]/div/div[1]/div/div/div[2]/div/div[3]/div[1]/select")));
		select1.selectByVisibleText(TCategory);
		//SubCategry.click();
		Select select2 = new Select(driver.findElement(By.xpath("//*[@id=\\\"frmVisitTask\\\"]/div[2]/div/div[1]/div/div/div[2]/div/div[3]/div[1]/select")));
		select2.selectByVisibleText(TSubCategory);
		TaskDetails.sendKeys(TDetail);
		
		//IsRequired.click();
		Select select3 = new Select(driver.findElement(By.xpath("//*[@id=\\\"IsRequired\\\"]")));
		select3.selectByVisibleText(isReqd);
		Select select4 = new Select(driver.findElement(By.xpath("//*[@id=\\\"IsDefault\\\"]")));
		select4.selectByVisibleText(isDefault);
		Save.click();
		Thread.sleep(7000);
		Cancel.click();
		
	}
}
