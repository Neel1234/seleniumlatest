package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class CaptureCallListPage extends TestBase
{
	
	@FindBy(xpath="/html/body/div[1]/div[2]/div/div/div/div[1]/h3")
	WebElement captureCallListLabel;
	public CaptureCallListPage()
	{
		PageFactory.initElements(driver, this);
	}
	public boolean verifyCaptureCallListLabel()
	{
		return captureCallListLabel.isDisplayed();
	}
}
