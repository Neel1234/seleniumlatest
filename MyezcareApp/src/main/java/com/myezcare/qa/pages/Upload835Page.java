package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class Upload835Page extends TestBase 
{

	public Upload835Page()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateUpload835PageTitle()
	{
		return driver.getTitle();
	}
}
