package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PatientSchedulePage extends TestBase
{
	public PatientSchedulePage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validatePatientScheduleTitle()
	{
		return driver.getTitle();
	}
}
