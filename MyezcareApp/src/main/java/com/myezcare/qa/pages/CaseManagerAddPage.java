package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import com.myezcare.qa.base.TestBase;

public class CaseManagerAddPage extends TestBase
{
	public static String validateCaseManagerAddPageTitle()
	{
		return driver.getTitle();
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[2]/a" )
	WebElement CaseManager;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[2]/ul/li[1]/a" )
	WebElement Add;
	
	//Form Section
	
	@FindBy(xpath = "//*[@id=\"CaseManager_AgencyID\"]" )
	WebElement Agency;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_FirstName\"]" )
	WebElement FirstName;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_LastName\"]" )
	WebElement LastName;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Email\"]" )
	WebElement Email;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Extension\"]" )
	WebElement Extension;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Phone\"]" )
	WebElement Phone;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Cell\"]" )
	WebElement Cell;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Fax\"]" )
	WebElement Fax;
	
	@FindBy(xpath = "//*[@id=\"CaseManager_Notes\"]" )
	WebElement Notes;
	
	@FindBy(xpath = "//*[@id=\"frmCaseManager\"]/div[2]/div/div[2]/input" )
	WebElement Save;
	
	@FindBy(xpath = "//*[@id=\"frmCaseManager\"]/div[2]/div/div[2]/a/input" )
	WebElement Cancel;
	
	// Initializing the Page Objects:
	
			public CaseManagerAddPage()
			{
				PageFactory.initElements(driver, this);
			}
			
	//Actions
			
			public void CaseManagerAdd(String agency, String fName, String lName, String email, String extension, String phone, 
					String cell, String fax, String notes) throws InterruptedException
			{
				Agency.sendKeys(agency);
				FirstName.sendKeys(fName);
				LastName.sendKeys(lName);
				Email.sendKeys(email);
				Extension.sendKeys(extension);
				Phone.sendKeys(phone);
				Cell.sendKeys(cell);
				Fax.sendKeys(fax);
				Notes.sendKeys(notes);
				Save.click();
				Thread.sleep(6000);
				Cancel.click();
			}
}
