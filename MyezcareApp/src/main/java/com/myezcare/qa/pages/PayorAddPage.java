package com.myezcare.qa.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class PayorAddPage extends TestBase
{
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[9]/a" )
	WebElement Payor1;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[9]/ul/li[1]/a" )
	WebElement Add;
	
	//Form Section
	
	@FindBy(xpath = "//*[@id=\"frmPayor\"]/div/div/div/div[2]/input" )
	WebElement Save;	
	
	@FindBy(xpath = "//*[@id=\"frmPayor\"]/div/div/div/div[2]/a/input" )
	WebElement Cancel;
	
	@FindBy(xpath = "//*[@id=\"Payor_PayorIdentificationNumber\"]")
			WebElement PayorId;
	
	@FindBy(xpath = "//*[@id=\"Payor_PayorName\"]")
			WebElement PayorName;
	
	@FindBy(xpath = "//*[@id=\"Payor_ShortName\"]")
			WebElement ShortName;
	
	@FindBy(xpath = "//*[@id=\"Payor_Address\"]")
			WebElement Address;
	
	@FindBy(xpath =	"//*[@id=\"Payor_City\"]")
			WebElement City;
	
	@FindBy(xpath = "//*[@id=\"Payor_StateCode\"]")
			WebElement State;
	
	@FindBy(xpath = "//*[@id=\"Payor_ZipCode\"]")
			WebElement ZipCode;
	
	@FindBy(xpath = "//*[@id=\"PayorBillingType\"]")
			WebElement PayorInvoiceType;
	
	@FindBy(xpath = "//*[@id=\"Payor_AgencyTaxNumber\"]")
			WebElement AgencyTaxNumber;
	
	@FindBy(xpath = "//*[@id=\"Payor_NPIOption\"]")
			WebElement NPIOption;
	
	@FindBy(xpath = "//*[@id=\"Payor_PayerGroup\"]")
			WebElement PayerGroup;
	
	@FindBy(xpath = "//*[@id=\"Payor_BussinessLine\"]")
			WebElement BusinessLine;
	
	@FindBy(xpath = "//*[@id=\"Payor_NPINumber\"]")
			WebElement NPINumber;
	
	@FindBy(xpath = "//*[@id=\"Payor_ClaimProcessor\"]")
			WebElement Aggregator;
	
	@FindBy(xpath = "//*[@id=\"Payor_VisitBilledBy\"]")
			WebElement VisitBilledBy;
	
	// Initializing the Page Objects:
	
				public PayorAddPage()
				{
					PageFactory.initElements(driver, this);
				}
				
		//Actions
				public static String validatePayorAddPageTitle(){
					return driver.getTitle();
				}
				
				public void PayorAdd(String id, String name, String sName, String add, String city, String state, 
						String zCode, String invoiceType, String ageTaxNo, String npiOption, String payerGrp, String businessLine,
						String npiNo, String aggregator, String vistedBill) throws InterruptedException
				{
					PayorId.sendKeys(id);
					PayorName.sendKeys(name);
					ShortName.sendKeys(sName);
					Address.sendKeys(add);
					City.sendKeys(city);
					Select select1 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_StateCode\\\"]")));
					select1.selectByVisibleText(state);
					ZipCode.sendKeys(zCode);
					Select select2 = new Select(driver.findElement(By.xpath("//*[@id=\\\"PayorBillingType\\\"]")));
					select2.selectByVisibleText(invoiceType);
					AgencyTaxNumber.sendKeys(ageTaxNo);
					Select select3 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_NPIOption\\\"]")));
					select3.selectByVisibleText(npiOption);
					Select select4 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_PayerGroup\\\"]")));
					select4.selectByVisibleText(payerGrp);
					Select select5 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_BussinessLine\\\"]")));
					select5.selectByVisibleText(businessLine);
					Select select6 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_NPINumber\\\"]")));
					select6.selectByVisibleText(npiNo);
					Select select7 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_ClaimProcessor\\\"]")));
					select7.selectByVisibleText(aggregator);
					Select select8 = new Select(driver.findElement(By.xpath("//*[@id=\\\"Payor_VisitBilledBy\\\"]")));
					select8.selectByVisibleText(vistedBill);
					Save.click();
					Thread.sleep(6000);
					Cancel.click();
				}
	
}


