package com.myezcare.qa.pages;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class EmployeeCalendarPage extends TestBase 
{

	@FindBy(xpath="//button[@data-toggle='dropdown' and @title='Select Employee']")
	WebElement selectempdropdown;
	
	@FindBy(xpath="//input[@type='text' and @placeholder='Type in a search term']")
	WebElement typesearchterm;
	
	@FindBy(xpath="//button[@class='btn btn-sm btn-primary' and @data-ng-click='SeachAndGenerateCalenders()']")
	WebElement searchBtn;
	
	@FindBy(xpath="//button[@class='fc-DayBtn-button fc-button fc-state-default fc-corner-left' and text()='Day']")
	WebElement dayBtn;
	
	
	@FindBy(xpath="//button[@class='fc-WeekBtn-button fc-button fc-state-default' and text()='Week']")
	WebElement weekBtn;
	
	@FindBy(xpath="//button[@class='fc-MonthBtn-button fc-button fc-state-default fc-corner-right' and text()='Month']")
	WebElement monthBtn;

	// Initializing the Page Objects:
	public EmployeeCalendarPage() 
	{
		PageFactory.initElements(driver, this);
		
	}
	//Actions
	public static String validateEmployeeCalendarTitle()
	{
		return driver.getTitle();
		
	}
	
	public void validateSearchBtn(String empName) throws Exception
	{
		selectempdropdown.click();
		Thread.sleep(2000);
		typesearchterm.sendKeys(empName);
		searchBtn.click();
		
	}
	public void validateDayBtn()
	{
		dayBtn.click();
		
	}

	public void validateWeekBtn()
	{
		weekBtn.click();
		
	}
	
	public void validateMonthBtn()
	{
		monthBtn.click();
		
	}
}
