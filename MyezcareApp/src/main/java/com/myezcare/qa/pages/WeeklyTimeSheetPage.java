package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class WeeklyTimeSheetPage extends TestBase{
	public WeeklyTimeSheetPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateWeeklyTimeSheetPageTitle()
	{
		return driver.getTitle();
	}
}
