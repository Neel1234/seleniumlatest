package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class EmployeeSchedulePage extends TestBase
{
	
	@FindBy(xpath="//button[@class='btn btn-sm green-jungle pull-right' and @title='Add Employee Schedule']")
	WebElement addEmpschBtn;
	
	@FindBy(xpath="//select[@name='ETSModelEmployee' and @id='ets_emp']")
	WebElement empdropdownBtn;
	
	@FindBy(xpath="//a[ @class='btn btn-xs blue']//i[@class='fa fa-edit']")
	WebElement empeditBtn;
	
	// Initializing the Page Objects:
		public EmployeeSchedulePage() 
		{
			PageFactory.initElements(driver, this);
			
		}
		//Actions
		public static String validateEmployeeScheduleTitle()
		{
			return driver.getTitle();
		}
		public void validateEmpDropdown(int ind) throws Exception
		{
			//empdropdownBtn.click();
			Select empname=new Select(empdropdownBtn);
			empname.selectByIndex(ind);
			Thread.sleep(4000);
		}
		public UpdateEmpSchedulePage validateaddEmployeeScheduleBtn() throws Exception
		{
			
			empeditBtn.click();
			Thread.sleep(4000);
			return new UpdateEmpSchedulePage();
		}

}
