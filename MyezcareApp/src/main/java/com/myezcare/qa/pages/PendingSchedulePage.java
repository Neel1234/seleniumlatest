package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class PendingSchedulePage extends TestBase
{
	public PendingSchedulePage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validatePendingSchedulePageTitle()
	{
		return driver.getTitle();
	}
}
