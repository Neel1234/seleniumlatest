package com.myezcare.qa.pages;

import org.openqa.selenium.support.PageFactory;

import com.myezcare.qa.base.TestBase;

public class BroadcastNotoficationPage extends TestBase 
{
	public BroadcastNotoficationPage()
	{
		PageFactory.initElements(driver, this);
	}
	public static String validateBroadcastNotoficationPageTitle()
	{
		return driver.getTitle();
	}
}
