package com.myezcare.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;

import com.myezcare.qa.base.TestBase;

public class PreferenceListPage extends TestBase
{
	public PreferenceListPage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/a/span[1]" )
	WebElement Settings;	
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[11]/a" )
	WebElement Preference;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[1]/div/ul/li[10]/ul/li[11]/ul/li[2]/a" )
	WebElement List;
	
	//Form Section
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[1]/button" )
	WebElement Refresh;
	
	@FindBy(xpath = "//*[@id=\"IsDeleted\"]" )
	WebElement Active;
	
	@FindBy(xpath = "/html/body/div[1]/div[1]/div[5]/div[2]/div/div/div[2]/div[1]/div[3]/a" )
	WebElement Preference_Skill;
	
	@FindBy(xpath = "//*[@id=\"uniform-SelectAllDxCode\"]" )
	WebElement Checkbox;
	
	@FindBy(xpath = "//*[@id=\"SearchPreferenceListPage_PreferenceName\"]" )
	WebElement Name;
	
	@FindBy(xpath = "//*[@id=\"SearchPreferenceListPage_PreferenceType\"]" )
	WebElement Type;
	
	@FindBy(xpath = "//*[@id=\"frmPreferenceList \"]/div/table/tbody/tr[1]/td[4]/input" )
	WebElement Search;
	
	@FindBy(xpath = "//*[@id=\"frmPreferenceList \"]/div/table/tbody/tr[1]/td[4]/a" )
	WebElement Reset;
	
	@FindBy(xpath = "//*[@id=\"frmPreferenceList \"]/div/table/tbody/tr[2]/td[4]/a[1]/i" )
	WebElement Edit;
	
	//Actions
	public static String validatePreferenceListPageTitle()
	{
		return driver.getTitle();
	}
	public boolean verifyPreferenceListLabel()
	{
		return List.isDisplayed();
	}
	public void selectMultipleselectPreference() throws Exception
	{
		Thread.sleep(5000);
		Checkbox.click();
	}
	public void verifySearchBtn(String ps_type) throws Exception
	{
		Select type=new Select(Type);
		type.selectByVisibleText(ps_type);
		Thread.sleep(5000);
		Search.click();
	}
	public void validateDeleteActiveRecord(String record)
	{
		Select active=new Select(Active);
		active.selectByVisibleText(record);
	}
	public PreferenceAddPage validateAddPreferenceBtn() throws Exception
	{
		Thread.sleep(5000);
		Preference_Skill.click();
		return new PreferenceAddPage();
	}
	
	public void validateResetBtn(String name) throws Exception
	{
		Name.sendKeys(name);
		Thread.sleep(5000);
		Reset.click();			
	}
	public void validateEditBtn(String name) throws Exception 
	{
		Name.sendKeys(name);
		Thread.sleep(5000);
		Search.click();
		Thread.sleep(5000);
		Edit.click();
	}
}
