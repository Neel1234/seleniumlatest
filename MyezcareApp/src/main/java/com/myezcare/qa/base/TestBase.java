package com.myezcare.qa.base;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.reporters.HtmlHelper;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.myzcare.qa.util.Helper;
import com.myzcare.qa.util.TestUtil;
import com.myzcare.qa.util.WebEventListener;


public class TestBase 
{

	public static WebDriver driver;
	public static Properties prop;
	public  static EventFiringWebDriver e_driver;
	public static WebEventListener eventListener;
	public static ExtentReports report;
	public ExtentTest logger;
	public static ExtentHtmlReporter htmlReporter;
	//ExtentReports ext;

	
	public TestBase()
	{
		try {
			prop = new Properties();
			FileInputStream ip = new FileInputStream("C:\\Users\\Web\\git\\repository\\MyezcareApp\\src\\main\\java\\com\\myezcare\\qa\\config\\config.properties");
			prop.load(ip);
		} 
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	@BeforeSuite
	public void setUpSuite()
	{	
		Reporter.log("Setting Up the report and Test is getting ready",true);
		ExtentHtmlReporter ext=new ExtentHtmlReporter(new File(System.getProperty("user.dir")+"/Report/Myzcare_"+Helper.getCurrentDateTime()+".html"));
		
		ext.config().setReportName("Production Environment-beta");
		//ExtentHtmlReporter ext=new ExtentHtmlReporter("./Report/Myzcare.html");
		report=new ExtentReports();
		report.attachReporter(ext);
		//htmlReporter.config().setDocumentTitle("Test Environment");
		Reporter.log("Setting is done-Test can be started",true);
		
		
	}
	public static void initialization()
	{
		System.out.println("66666666666666666666666666666");
		String browserName=prop.getProperty("browser");
		
		Reporter.log("Trying to start browser and getting application ready",true);
		try {
			if(browserName.equals("chrome"))
			{
				System.setProperty("webdriver.chrome.driver","C:\\Users\\Web\\git\\repository\\MyezcareApp\\drivers\\chromedriver.exe");
				ChromeOptions options = new ChromeOptions();
				options.addArguments("--no-sandbox");
				
				// Initialize browser
				driver=new ChromeDriver(options);
			}
			else if(browserName.equals("IE"))
			{
				System.setProperty("webdriver.ie.driver","C:\\Users\\Web\\git\\repository\\MyezcareApp\\drivers\\IEDriverServer.exe");	
				driver = new InternetExplorerDriver(); 
			}
			else if(browserName.equals("Firefox"))
			{
				System.setProperty("webdriver.ie.driver","C:\\Users\\Web\\git\\repository\\MyezcareApp\\drivers\\geckodriver.exe");
				driver = new FirefoxDriver(); 
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		Reporter.log("Browser and application is Up and running.",true);
		
/*		e_driver = new EventFiringWebDriver(driver);
		// Now create object of EventListerHandler to register it with EventFiringWebDriver
		eventListener = new WebEventListener();
		e_driver.register(eventListener);
		driver = e_driver;*/
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().pageLoadTimeout(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);

		
		driver.get(prop.getProperty("url"));


	}
	@AfterTest
	public void getResult(ITestResult result) throws IOException
	{
		System.out.println("22222222222222222");
		Reporter.log("Test is about to end",true);
		if(result.getStatus()==ITestResult.FAILURE)
		{
			logger.fail("Test Failed", MediaEntityBuilder.createScreenCaptureFromPath(Helper.captureScreenshot(driver)).build());
			
			//logger.fail("Test Failed");
		}
		else if(result.getStatus()==ITestResult.SUCCESS)
		{
			logger.pass("Test pass", MediaEntityBuilder.createScreenCaptureFromPath(Helper.captureScreenshot(driver)).build());
		}
		
		
		
		Reporter.log("Test is completed>>>>Reports Generated",true);
		driver.quit();
		driver.quit();
	}
	@AfterSuite
	public void endReport()
	{
		System.out.println("3333333333333333333333333");
		report.flush();
		
	}
	
}
