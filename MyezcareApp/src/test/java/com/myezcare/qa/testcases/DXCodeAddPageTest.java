package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.AgencyAddPage;
import com.myezcare.qa.pages.AgencyListPage;
import com.myezcare.qa.pages.DXCodeAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class DXCodeAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	DXCodeAddPage dxCodeAddPage;
	TestUtil testUtil;
	String sheetName ="DXCodeAdd";
	public DXCodeAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		dxCodeAddPage = new DXCodeAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dxCodeAddPage = homePage.clickonDXcodeAddLink();
		
	}
	@Test(priority=1)
	public void DXCodeAddPageTitleTest()
	{
		logger=report.createTest("Verify DX Code AddPage Page Title");
		String title = DXCodeAddPage.validateAgencyListPageTitle();
		Assert.assertEquals(title, "Add DX Code");

		String dxCodeType = DXCodeAddPage.validateAgencyListPageTitle();
		Assert.assertEquals(dxCodeType, "Add DX Code");
		
		String dxCodeTypeWithoutDot = DXCodeAddPage.validateAgencyListPageTitle();
		Assert.assertEquals(dxCodeTypeWithoutDot, "Add DX Code");
		
		String dxcode1 = DXCodeAddPage.validateAgencyListPageTitle();
		Assert.assertEquals(dxcode1, "Add DX Code");
		
		String description = DXCodeAddPage.validateAgencyListPageTitle();
		Assert.assertEquals(description, "Add DX Code");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewAgency(String dxCodeType, String dxCodeTypeWithoutDot, String dxcode1, String description) 
			throws Exception 
	{
		dxCodeAddPage.DXCodeAdd(dxCodeType, dxCodeTypeWithoutDot, dxcode1, description);
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
