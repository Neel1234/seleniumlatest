package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ReportPage;
import com.myezcare.qa.pages.WeeklyTimeSheetPage;

public class ReportPageTest extends TestBase{
	LoginPage loginPage;
	HomePage homePage;
	ReportPage reportPage;
	public ReportPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		reportPage = new ReportPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		reportPage = homePage.clickonReportLink();
		
	}
	@Test(priority=1)
	public void ReportPageTitleTest()
	{
		logger=report.createTest("Verify Report Page Title");
		String title =ReportPage.validateReportPageTitle();
		Assert.assertEquals(title, "ReportMaster");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
