package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeBillingreportPage;
import com.myezcare.qa.pages.EmployeeVisitreportPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class EmployeeVisitreportPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	EmployeeVisitreportPage employeeVisitreportPage;
	public EmployeeVisitreportPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeeVisitreportPage = new EmployeeVisitreportPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeeVisitreportPage = homePage.clickonEmployeeVisitreportLink();
		
	}
	@Test(priority=1)
	public void EmployeeVisitreportPageTitleTest()
	{
		logger=report.createTest("Verify Employee Visit report Page Title");
		String title =EmployeeVisitreportPage.validateEmployeeVisitreportPageTitle();
		Assert.assertEquals(title, "Time Sheet");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
