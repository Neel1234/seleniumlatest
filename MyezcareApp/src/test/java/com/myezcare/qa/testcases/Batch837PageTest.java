package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.Batch837Page;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PendingSchedulePage;

public class Batch837PageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	Batch837Page batch837Page;
	public Batch837PageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		batch837Page = new Batch837Page();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		batch837Page = homePage.clickonBatch837Link();
		
	}
	@Test(priority=1)
	public void Batch837PageTitleTest()
	{
		logger=report.createTest("Verify Batch 837 Page Title");
		String title = Batch837Page.validateBatch837PageTitle();
		Assert.assertEquals(title, "Batch");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}

