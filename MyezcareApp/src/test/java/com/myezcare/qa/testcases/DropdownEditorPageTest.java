package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DropdownEditorPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class DropdownEditorPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	DropdownEditorPage dropdownEditorPage;
	TestUtil testUtil;
	String sheetName ="DropdownEditorForm";
	public DropdownEditorPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		dropdownEditorPage = new DropdownEditorPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dropdownEditorPage = homePage.clickonDropdownEditorLink();
		
	}
	@Test(priority=1)
	public void DropdownEditorPageTitleTest()
	{
		logger=report.createTest("Verify Dropdown Editor Page Page Title");
		String item_type = DropdownEditorPage.validateDropdownEditorPageTitle();
		Assert.assertEquals(item_type, "Dropdown Editor");
		
		String title = DropdownEditorPage.validateDropdownEditorPageTitle();
		Assert.assertEquals(title, "Dropdown Editor");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewDropdownEditor(String item_type, String title) throws Exception 
	{
		dropdownEditorPage.DropdownnForm(item_type, title);
	}
	@Test(priority=3)
	public void verifyDropdownEditorListLabel()
	{
		logger=report.createTest("verify Dropdown Editor List Page Label");
		Assert.assertTrue(dropdownEditorPage.verifyDropdownEditorListLabel(),"Dropdown Editor List page not present");
	}
	@Test(priority=4)
	public void selectMultipleselectDropdownEditor() throws Exception
	{
		logger=report.createTest("verify Multiple select Dropdown Editor checkbox.");
		logger.info("To verify functionality of multiple select checkbox on Dropdown Editor list page.");
		dropdownEditorPage.selectMultipleselectDropdownEditor();
	}
	@Test(priority=5)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on Dropdown Editor list page.");
		logger.info("To check functionality of search button by entering type on Dropdown Editor list page.");
		dropdownEditorPage.verifySearchBtn("Gender");
	}
	@Test(priority=6)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for Dropdown Editor list page");
		dropdownEditorPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		dropdownEditorPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		dropdownEditorPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on Dropdown Editor list page");
		dropdownEditorPage.validateResetBtn("Caregiver", "UnSkilled");
	}
	
	@Test(priority=8)
	public void validateEditDropdownEditorBtn() throws Exception
	{
		logger=report.createTest("Verify Edit Dropdown Editor page");
		logger.info("To check functionality of edit Dropdown Editor page.Edit Dropdown Editor page should be open and display on screen.");
		dropdownEditorPage.validateEditBtn("selenium");
	}
//	@AfterMethod
//	public void tearDown()
//	{
//		driver.quit();
//	}
}
