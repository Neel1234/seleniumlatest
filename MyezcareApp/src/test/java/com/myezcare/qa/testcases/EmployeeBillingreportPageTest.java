package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.BroadcastNotoficationPage;
import com.myezcare.qa.pages.EmployeeBillingreportPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class EmployeeBillingreportPageTest extends TestBase{

	LoginPage loginPage;
	HomePage homePage;
	EmployeeBillingreportPage employeeBillingreportPage;
	String sheetName = "EmployeeAdd";
	public EmployeeBillingreportPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeeBillingreportPage = new EmployeeBillingreportPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeeBillingreportPage = homePage.clickonEmployeeBillingreportLink();
		
	}
	@Test(priority=1)
	public void EmployeeBillingreportPageTitleTest()
	{
		logger=report.createTest("Verify Employee Billing report Page Title");
		String title =EmployeeBillingreportPage.validateEmployeeBillingreportPageTitle();
		Assert.assertEquals(title, "Employee Billing Report");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	
	@Test(priority = 2, dataProvider = "getMyzcareTestData")
	public void TestMethod()
	{
		
	}
	
}
