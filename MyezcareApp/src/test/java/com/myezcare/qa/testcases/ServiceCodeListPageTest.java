package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ServiceCodeAddPage;
import com.myezcare.qa.pages.ServiceCodeListPage;
import com.myzcare.qa.util.TestUtil;

public class ServiceCodeListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	ServiceCodeListPage serviceCodeListPage;
	String sheetName ="ServiceCodeList";
	public ServiceCodeListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		serviceCodeListPage = new ServiceCodeListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		serviceCodeListPage = homePage.clickonServiceCodeListPageLink();
	}
	@Test(priority=1)
	public void ServiceCodeListPageTitleTest()
	{
		logger=report.createTest("Verify Service Code List Page Title");
		String title = ServiceCodeListPage.validateServiceCodeListPageTitle();
		Assert.assertEquals(title, "Service Code List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateServiceCodeList(String serviceCode, String modifier, String serviceName, String billable)throws Exception
	{
		serviceCodeListPage.serviceCodeList(serviceCode, modifier, serviceName);
	}
}
