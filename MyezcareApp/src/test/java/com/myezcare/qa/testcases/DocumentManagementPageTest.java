package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DocumentManagementPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class DocumentManagementPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	DocumentManagementPage documentManagementPage;
	TestUtil testUtil;
	String sheetName ="DocumentManagementForm";
	public DocumentManagementPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		documentManagementPage = new DocumentManagementPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		documentManagementPage = homePage.clickonDocumentManagementLink();
		
	}
	@Test(priority=1)
	public void DocumentManagementPageTitleTest()
	{
		logger=report.createTest("Verify Document Management Page Title");
		String user_type = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(user_type, "Document Management");
		
		String documentation_type = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(documentation_type, "Document Management");
		
		String folder = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(folder, "Document Management");
		
		String name = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(name, "Document Management");
		
		String role = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(role, "Document Management");
		
		String timeBased = DocumentManagementPage.validateDocumentManagementPageTitle();
		Assert.assertEquals(timeBased, "Document Management");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewDocumentManagement(String user_type, String documentation_type, String folder, String name, 
			String role, String timeBased) throws Exception 
	{
		documentManagementPage.DocumentManagementForm(user_type, documentation_type, folder, name, role, timeBased);
	}
	@Test(priority=3)
	public void verifyDocumentManagementListLabel()
	{
		logger=report.createTest("verify Document Management List Page Label");
		Assert.assertTrue(documentManagementPage.verifyDocumentManagementListLabel(),"Document Management List page not present");
	}
	@Test(priority=4)
	public void selectMultipleselectDocumentManagement() throws Exception
	{
		logger=report.createTest("verify Multiple select Document Management checkbox.");
		logger.info("To verify functionality of multiple select checkbox on Document Management list page.");
		documentManagementPage.selectMultipleselectDocumentManagement();
	}
	@Test(priority=5)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on Document Management list page.");
		logger.info("To check functionality of search button by entering type on Document Management list page.");
		documentManagementPage.verifySearchBtn("Internal");
	}
	@Test(priority=6)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for Document Management list page");
		documentManagementPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		documentManagementPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		documentManagementPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on Document Management list page");
		documentManagementPage.validateResetBtn("Visits", "No");
	}
	
	@Test(priority=8)
	public void validateEditDocumentManagementBtn() throws Exception
	{
		logger=report.createTest("Verify Edit Document Management page");
		logger.info("To check functionality of edit Document Management page.Edit Document Management page should be open and display on screen.");
		documentManagementPage.validateEditBtn("selenium");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
