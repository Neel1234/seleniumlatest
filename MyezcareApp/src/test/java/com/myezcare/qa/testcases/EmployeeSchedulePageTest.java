package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;

import com.myezcare.qa.pages.EmployeeSchedulePage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class EmployeeSchedulePageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	EmployeeSchedulePage employeeSchedulePage;
	public EmployeeSchedulePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeeSchedulePage = new EmployeeSchedulePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeeSchedulePage = homePage.clickOnEmployeeScheduleLink();
		
	}
	/*@Test(priority=1)
	public void employeeScheduleTitleTest()
	{
		logger=report.createTest("Verify Employee Schedule Title");
		String title = EmployeeSchedulePage.validateEmployeeScheduleTitle();
		Assert.assertEquals(title, "Employee Schedule");
	}
	
	@Test(priority=2)
	public void validateEmpnameDropdown() throws Exception
	{
		logger=report.createTest("Validate employee schedule list");
		logger.info("To check employee schedule list display on screen");
		employeeSchedulePage.validateEmpDropdown(6);
		
	}
	@Test(priority=3)
	public void validateaddEmployeeScheduleBtn() throws Exception
	{
		logger=report.createTest("Validate Add employee schedule button on employee schedule page.");
		logger.info("To check add employee schedule page is display on screen.");
		employeeSchedulePage.validateEmpDropdown(6);
		employeeSchedulePage.validateaddEmployeeScheduleBtn();
		
	}*/
	@Test(priority=4)
	public void validateEditBtn() throws Exception
	{
		logger=report.createTest("Validate edit button on employee schedule page.");
		logger.info("To check edit button functionality on employee schedule page");
		employeeSchedulePage.validateEmpDropdown(6);
		employeeSchedulePage.validateaddEmployeeScheduleBtn();
		
	}
}
