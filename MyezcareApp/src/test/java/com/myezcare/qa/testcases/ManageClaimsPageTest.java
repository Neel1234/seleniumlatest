package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ManageClaimsPage;
import com.myezcare.qa.pages.Upload835Page;

public class ManageClaimsPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	ManageClaimsPage manageClaimsPage;
	public ManageClaimsPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		manageClaimsPage = new ManageClaimsPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		manageClaimsPage = homePage.clickonManageClaimsPageLink();
		
	}
	@Test(priority=1)
	public void ManageClaimsTitleTest()
	{
		logger=report.createTest("Verify Manage Claims Page Title");
		String title = ManageClaimsPage.validateManageClaimsPageTitle();
		Assert.assertEquals(title, "ManageClaims");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
