package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.CaptureCallAddPage;
import com.myezcare.qa.pages.CaptureCallListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class CaptureCallListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	CaptureCallListPage captureCallListPage;
	public CaptureCallListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		captureCallListPage = new CaptureCallListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		captureCallListPage = homePage.clickonCaptureCallListPageLink();
		
	}
	@Test(priority=1)
	public void CaptureCallListPageTitleTest()
	{
		logger=report.createTest("Verify Capture Call List Page Title");
		Assert.assertTrue(captureCallListPage.verifyCaptureCallListLabel());
	}
}
