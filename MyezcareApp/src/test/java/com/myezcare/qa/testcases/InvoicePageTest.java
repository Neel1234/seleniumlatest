package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeVisitreportPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.InvoicePage;
import com.myezcare.qa.pages.LoginPage;

public class InvoicePageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	InvoicePage invoicePage;
	public InvoicePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		invoicePage = new InvoicePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		invoicePage = homePage.clickonInvoiceoLink();
		
	}
	@Test(priority=1)
	public void InvoicePageTitleTest()
	{
		logger=report.createTest("Verify Invoice Page Title");
		String title =InvoicePage.validateInvoicePageTitle();
		Assert.assertEquals(title, "Invoice List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
