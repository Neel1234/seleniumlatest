package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.GroupSMSPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;


public class GroupSMSPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	GroupSMSPage groupSMSPage;
	public GroupSMSPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		groupSMSPage = new GroupSMSPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		groupSMSPage = homePage.clickonGroupSMSLink();
		
	}
	@Test(priority=1)
	public void GroupSMSPageTitleTest()
	{
		logger=report.createTest("Verify Group SMS Page Title");
		String title =GroupSMSPage.validateGroupSMSPageTitle(); 
		Assert.assertEquals(title, "Group SMS");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
