package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DXCodeListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class DXCodeListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	DXCodeListPage dxCodeListPage;
	public DXCodeListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		dxCodeListPage = new DXCodeListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dxCodeListPage = homePage.clickonDXcodeListLink();
		
	}
	@Test(priority=1)
	public void DXCodelistPageTitleTest()
	{
		logger=report.createTest("Verify DX Code list Page Title");
		String title =DXCodeListPage.validateDXCodeListPageTitle();
		Assert.assertEquals(title, "DX Code List");
	}
	@Test(priority=2)
	public void verifyDXCodeListLabel()
	{
		logger=report.createTest("verify DX Code List Page Label");
		Assert.assertTrue(dxCodeListPage.verifyDXCodeListLabel(),"DX Code List page not present");
	}
	@Test(priority=3)
	public void selectMultipleselectDXCode() throws Exception
	{
		logger=report.createTest("verify Multiple select DX Code checkbox.");
		logger.info("To verify functionality of multiple select checkbox on DX Code list page.");
		dxCodeListPage.selectMultipleselectDXCode();
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on DX Code list page.");
		logger.info("To check functionality of search button by entering type on DX Code list page.");
		dxCodeListPage.verifySearchBtn("ReferralSource");
	}
	@Test(priority=5)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for DX Code list page");
		dxCodeListPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		dxCodeListPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		dxCodeListPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=6)
	public void validateAddDXCodeBtn() throws Exception
	{
		logger=report.createTest("Verify Add DX Code page");
		logger.info("To check functionality of Add DX Code page");
		dxCodeListPage.validateAddDXCodeBtn();
	}
		
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on DX Code list page");
		dxCodeListPage.validateResetBtn("ICD-9", "2001200", "abd hgjgj jfj");
	}
	
	@Test(priority=8)
	public void validateEditBtn() throws Exception
	{
		logger=report.createTest("Verify Edit DX Code page");
		logger.info("To check functionality of edit DX Code page.Edit DX Code page should be open and display on screen.");
		dxCodeListPage.validateEditBtn("2001200");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
