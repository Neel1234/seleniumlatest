package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PendingSchedulePage;
import com.myezcare.qa.pages.ScheduleLogPage;

public class PendingSchedulePageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PendingSchedulePage pendingschedulePage;
	public PendingSchedulePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		pendingschedulePage = new PendingSchedulePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		pendingschedulePage = homePage.clickOnPendingScheduleLink();
		
	}
	@Test(priority=1)
	public void PendingSchedulePageTitleTest()
	{
		logger=report.createTest("Verify Pending Schedule Page Title");
		String title = PendingSchedulePage.validatePendingSchedulePageTitle();
		Assert.assertEquals(title, "Pending Schedules");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
