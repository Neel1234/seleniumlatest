package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

import com.myezcare.qa.pages.OrganizationSettingPage;

public class OrganizationSettingPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	OrganizationSettingPage organizationSettingPage;


	public OrganizationSettingPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		organizationSettingPage = new OrganizationSettingPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		organizationSettingPage = homePage.clickOnOrganizationSettingLink();
	}
	@Test(priority=1)
	public void verifyOrganizationSettingLabel()
	{
		logger=report.createTest("verify Organization Setting page Label","To check whether Organization Setting label present on screen or not.");
		Assert.assertTrue(organizationSettingPage.verifyOrganizationSettingLabel(),"Organization Setting page not present");
	}
	
	
}
