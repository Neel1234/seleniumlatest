package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.CaptureCallAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.OrganizationFormsPage;

public class CaptureCallAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	CaptureCallAddPage captureCallAddPage;
	public CaptureCallAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		captureCallAddPage = new CaptureCallAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		captureCallAddPage = homePage.clickonCaptureCallAddPageLink();
		
	}
	@Test(priority=1)
	public void CaptureCallAddPageTitleTest()
	{
		logger=report.createTest("Verify Capture Call Add Page Title");
		String title = CaptureCallAddPage.validateCaptureCallAddPageTitle();
		Assert.assertEquals(title, "CaptureCallAdd");
	}

}
