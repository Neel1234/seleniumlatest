package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.NoteSentenceAddPage;
import com.myzcare.qa.util.TestUtil;

public class NoteSentenceAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	NoteSentenceAddPage noteSentenceAddPage;
	String sheetName ="NoteSentencesAdd";
	public NoteSentenceAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		noteSentenceAddPage = new NoteSentenceAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		noteSentenceAddPage = homePage.clickonNoteSentenceAddPageLink();
	}
	@Test(priority=1)
	public void NoteSentenceAddPageTitleTest()
	{
		logger=report.createTest("Verify Note Sentence Add Page Title");
		String title = 	NoteSentenceAddPage.validateNoteSentenceAddPageTitle();
		Assert.assertEquals(title, "Add Note Sentence");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateNoteSentencesAdd(String noteTitle, String details)throws Exception
	{
		noteSentenceAddPage.addNoteSentences(noteTitle, details);
	}
}
