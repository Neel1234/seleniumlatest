package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.Eligibility270Page;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.Upload835Page;

public class Eligibility270PageTest extends TestBase{
	LoginPage loginPage;
	HomePage homePage;
	Eligibility270Page eligibility270Page;
	public Eligibility270PageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		eligibility270Page = new Eligibility270Page();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		eligibility270Page = homePage.clickonEligibility270PageLink();
	}
	@Test(priority=1)
	public void Eligibility270PageTitleTest()
	{
		logger=report.createTest("Verify Eligibility 270 Page Title");
		String title = Eligibility270Page.validateEligibility270PageTitle();
		Assert.assertEquals(title, "Eligibility - 270 / 271");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
