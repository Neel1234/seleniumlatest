package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.FacilityAddPage;
import com.myezcare.qa.pages.FacilityListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class FacilityListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	FacilityListPage facilityListPage;
	public FacilityListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		facilityListPage = new FacilityListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		facilityListPage = homePage.clickonFacilityListLink();
		
	}
	@Test(priority=1)
	public void FacilityListPageTitleTest()
	{
		logger=report.createTest("Verify Facility List Page Title");
		String title = FacilityListPage.validateFacilityListPageTitle();
		Assert.assertEquals(title, "Facility House List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
