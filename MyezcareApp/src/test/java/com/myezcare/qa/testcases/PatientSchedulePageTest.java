package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PatientCalendarPage;
import com.myezcare.qa.pages.PatientSchedulePage;

public class PatientSchedulePageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PatientSchedulePage patientSchedulePage;
	public PatientSchedulePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		patientSchedulePage = new PatientSchedulePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		patientSchedulePage = homePage.clickOnPatientScheduleLink();
		
	}
	@Test(priority=1)
	public void PatientScheduleTitleTest()
	{
		logger=report.createTest("Verify Employee Schedule Page Title");
		String title = PatientSchedulePage.validatePatientScheduleTitle();
		Assert.assertEquals(title, "Patient Schedule");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
