package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeCalendarPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PatientCalendarPage;

public class PatientCalendarPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PatientCalendarPage patientCalendarPage;
	public PatientCalendarPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		patientCalendarPage = new PatientCalendarPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		patientCalendarPage = homePage.clickOnPatientCalendarLink();
		
	}
	@Test(priority=1)
	public void PatientCalendarTitleTest()
	{
		logger=report.createTest("Verify Employee Calendar Page Title");
		String title = PatientCalendarPage.validatePatientCalendarTitle();
		Assert.assertEquals(title, "Patient Calendar");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
