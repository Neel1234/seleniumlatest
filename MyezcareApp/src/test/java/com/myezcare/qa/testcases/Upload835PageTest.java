package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.Batch837Page;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.Upload835Page;

public class Upload835PageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	Upload835Page upload835Page;
	public Upload835PageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		upload835Page = new Upload835Page();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		upload835Page = homePage.clickonUpload835Link();
		
	}
	@Test(priority=1)
	public void Upload835TitleTest()
	{
		logger=report.createTest("Verify Upload 835 Page Title");
		String title = Upload835Page.validateUpload835PageTitle();
		Assert.assertEquals(title, "Upload 835");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
