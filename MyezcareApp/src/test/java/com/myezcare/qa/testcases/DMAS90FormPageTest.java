package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DMAS90FormPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.InvoicePage;
import com.myezcare.qa.pages.LoginPage;

public class DMAS90FormPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	DMAS90FormPage dmas90FormPage;
	public DMAS90FormPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		dmas90FormPage = new DMAS90FormPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		dmas90FormPage = homePage.clickonDMAS90FormLink();
		
	}
	@Test(priority=1)
	public void DMAS90FormPageTitleTest()
	{
		logger=report.createTest("Verify DMAS_90 Form Page Title");
		String title =DMAS90FormPage.validateDMAS90FormPageTitle();
		Assert.assertEquals(title, "DMAS_90 Form");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
