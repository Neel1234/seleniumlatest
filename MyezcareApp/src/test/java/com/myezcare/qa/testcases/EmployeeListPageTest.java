package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;

import com.myezcare.qa.pages.EmployeeListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class EmployeeListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	EmployeeListPage employeelistPage;
	public EmployeeListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeelistPage = new EmployeeListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeelistPage = homePage.clickOnEmployeeListLink();
	}
	
	@Test(priority=1)
	public void employeelistTitleTest()
	{
		logger=report.createTest("Validate Add Employee page Title","Validate Employee List page title");
		String title = EmployeeListPage.validateEmployeeListpageTitle();
		Assert.assertEquals(title, "Employee List");
	}
	@Test(priority=2)
	public void verifyEmployeeListLabel()
	{
		logger=report.createTest("verify Employee List Page Label");
		Assert.assertTrue(employeelistPage.verifyEmployeeListLabel(),"Employee List page not present");
	}
	@Test(priority=3)
	public void selectMultipleselectEmp() throws Exception
	{
		logger=report.createTest("verify Multiple select employee checkbox.");
		logger.info("To verify functionality of multiple select checkbox on employee list page.");
		employeelistPage.selectMultipleselectEmp();
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on employee list page.");
		logger.info("To check functionality of search button by entering role on employee list page.");
		employeelistPage.verifySearchBtn("Nurse");
	}
	@Test(priority=5)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for employee list page");
		employeelistPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		employeelistPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		employeelistPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=6)
	public void validateBulkScheduleDropdown() throws Exception
	{
		logger=report.createTest("Verify bulk schedule dropdown box");
		logger.info("To check functionality of bulk schedule drop down functionality.");
		employeelistPage.selectBulkschDropdown("Bulk Schedule");
	}
	@Test(priority=7)
	public void validateAddEmpBtn() throws Exception
	{
		logger=report.createTest("Verify Add employee page");
		logger.info("To check functionality of Add employee page");
		employeelistPage.validateAddEmpBtn();
	}
		
	@Test(priority=8)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on employee list page");
		employeelistPage.validateResetBtn("WB222", "abs", "abc@gmail.com", "2222222222", "abd hgjgj jfj");
	}
	
	@Test(priority=9)
	public void validateEditEmpBtn() throws Exception
	{
		logger=report.createTest("Verify Edit employee page");
		logger.info("To check functionality of edit employee page.Edit employee page should be open and display on screen.");
		employeelistPage.validaetEditBtn("selenium");
	}
	
	
}
