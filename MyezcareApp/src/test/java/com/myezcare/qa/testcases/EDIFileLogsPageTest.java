package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EDIFileLogsPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.Upload835Page;

public class EDIFileLogsPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	EDIFileLogsPage ediifleLogsPage;
	public EDIFileLogsPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		ediifleLogsPage = new EDIFileLogsPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		ediifleLogsPage = homePage.clickonEdiFileLogsLink();
		
	}
	@Test(priority=1)
	public void EDIFileLogsPageTitleTest()
	{
		logger=report.createTest("Verify EDI File Logs Page Title");
		String title = EDIFileLogsPage.validateEDIFileLogsPageTitle();
		Assert.assertEquals(title, "EDI File Logs List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
