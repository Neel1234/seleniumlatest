package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ScheduleLogPage;
import com.myezcare.qa.pages.ScheduleMasterPage;

public class ScheduleLogPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	ScheduleLogPage schedulelogPage;
	public ScheduleLogPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		schedulelogPage = new ScheduleLogPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		schedulelogPage = homePage.clickOnScheduleLogLink();
		
	}
	@Test(priority=1)
	public void ScheduleLogTitleTest()
	{
		logger=report.createTest("Verify Schedule Log Page Title");
		String title = ScheduleLogPage.validateScheduleLogTitle();
		Assert.assertEquals(title, "Schedule Master List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
