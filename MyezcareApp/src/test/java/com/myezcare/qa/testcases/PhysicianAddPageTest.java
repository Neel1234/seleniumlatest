package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PayorListPage;
import com.myezcare.qa.pages.PhysicianAddPage;
import com.myzcare.qa.util.TestUtil;

public class PhysicianAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PhysicianAddPage physicianAddPage;
	String sheetName ="PhysicianAdd";
	public PhysicianAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		physicianAddPage = new PhysicianAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		physicianAddPage = homePage.clickonPhysicianAddPageLink();
		
	}
	@Test(priority=1)
	public void PhysicianAddPageTitleTest()
	{
		logger=report.createTest("Verify Physician Add Page Title");
		String title = PhysicianAddPage.validatePhysicianAddPageTitle();
		Assert.assertEquals(title, "Add Physician");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validatePhysicianAdd()throws Exception
	{
		physicianAddPage.physicianAdd();
	}
}
