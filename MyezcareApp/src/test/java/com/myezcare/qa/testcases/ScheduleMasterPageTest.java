package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PatientSchedulePage;
import com.myezcare.qa.pages.ScheduleMasterPage;

public class ScheduleMasterPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	ScheduleMasterPage scheduleMasterPage;
	public ScheduleMasterPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		scheduleMasterPage = new ScheduleMasterPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		scheduleMasterPage = homePage.clickOnSchedulingMasterLink();
		
	}
	@Test(priority=1)
	public void ScheduleMasterTitleTest()
	{
		logger=report.createTest("Verify Schedule Master Page Title");
		String title = ScheduleMasterPage.validateScheduleMasterTitle();
		Assert.assertEquals(title, "Assignment");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
