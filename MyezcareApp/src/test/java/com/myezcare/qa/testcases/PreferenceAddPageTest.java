package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PreferenceAddPage;
import com.myzcare.qa.util.TestUtil;

public class PreferenceAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PreferenceAddPage preferenceAddPage;
	TestUtil testUtil;
	String sheetName ="PreferenceAdd";
	public PreferenceAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		preferenceAddPage = new PreferenceAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		preferenceAddPage = homePage.clickonPreferenceAddPageLink();
		
	}
	@Test(priority=1)
	public void PreferenceAddPageTitleTest()
	{
		logger=report.createTest("Verify Preference Add Page Title");
		String title = PreferenceAddPage.validatePreferenceAddPageTitle();
		Assert.assertEquals(title, "Add Preference/Skill");

		String type = PreferenceAddPage.validatePreferenceAddPageTitle();
		Assert.assertEquals(type, "Add Preference/Skill");

		String name = PreferenceAddPage.validatePreferenceAddPageTitle();
		Assert.assertEquals(name, "Add Preference/Skill");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewAgency(String type, String name) throws Exception 
	{
		preferenceAddPage.PreferenceAdd(type, name);
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
