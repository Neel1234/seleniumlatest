package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PatientAddPage;

public class PatientAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PatientAddPage patientAddPage;
	public PatientAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		patientAddPage = new PatientAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		patientAddPage = homePage.clickOnPatientAddLink();
	}
	@Test(priority=1)
	public void verifyPatientAddLabel()
	{
		logger=report.createTest("Verify Employee Add Page Label");
		Assert.assertTrue(patientAddPage.verifyAddpatientLabel(), "Patient Add page not present");
	}
	/*@AfterMethod
	public void tearDown(){
		driver.quit();
	}*/

}
