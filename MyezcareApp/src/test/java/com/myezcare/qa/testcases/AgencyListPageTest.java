package com.myezcare.qa.testcases;

 

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

 

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.AgencyListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

 

public class AgencyListPageTest extends TestBase
{
    LoginPage loginPage;
    HomePage homePage;
    AgencyListPage agencyListPage;
    public AgencyListPageTest()
    {
        super();
    }
    @BeforeMethod
    public void setUp() throws InterruptedException
    {
        initialization();
        loginPage=new LoginPage();
        agencyListPage = new AgencyListPage();
        homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
        agencyListPage = homePage.clickonAgencyListLink();
        
    }
    @Test(priority=1)
    public void AgencyListPageTitleTest()
    {
        logger=report.createTest("Verify Agency List Title");
        String title = AgencyListPage.validateAgencyListPageTitle();
        Assert.assertEquals(title, "Agency List");
    }
    @Test(priority=2)
    public void verifyAgencyListLabel()
    {
        logger=report.createTest("verify Agency List Page Label");
        Assert.assertTrue(agencyListPage.verifyAgencyListLabel(),"Agency List page not present");
    }
    @Test(priority=3)
    public void selectMultipleselectAgency() throws Exception
    {
        logger=report.createTest("verify Multiple select agency checkbox.");
        logger.info("To verify functionality of multiple select checkbox on agency list page.");
        agencyListPage.selectMultipleselectAgency();
    }
    @Test(priority=4)
    public void validateSearchBtn() throws Exception
    {
        logger=report.createTest("Verify search button functionality on agency list page.");
        logger.info("To check functionality of search button by entering type on agency list page.");
        agencyListPage.verifySearchBtn("ReferralSource");
    }
    @Test(priority=5)
    public void validateDeleteActiveBtn() throws Exception
    {
        logger=report.createTest("Check Active and Deleted record.");
        logger.info("To check Active and deleted record for agency list page");
        agencyListPage.validateDeleteActiveRecord("Active");
        Thread.sleep(3000);
        agencyListPage.validateDeleteActiveRecord("Deleted");
        Thread.sleep(3000);
        agencyListPage.validateDeleteActiveRecord("All Records");
    }
    @Test(priority=6)
    public void validateAddAgencyBtn() throws Exception
    {
        logger=report.createTest("Verify Add agency page");
        logger.info("To check functionality of Add agency page");
        agencyListPage.validateAddAgencyBtn();
    }
        
    @Test(priority=7)
    public void validateresetBtn() throws Exception
    {
        logger=report.createTest("Validate Reset button functionality");
        logger.info("To check functionality of reset button on agency list page");
        agencyListPage.validateResetBtn("Ravi", "abc", "xyz", "2222222222", "852147", "abd hgjgj jfj");
    }
    
    @Test(priority=8)
    public void validateEditAgencyBtn() throws Exception
    {
        logger=report.createTest("Verify Edit agency page");
        logger.info("To check functionality of edit agency page.Edit agency page should be open and display on screen.");
        agencyListPage.validateEditBtn("selenium");
    }
    /*@AfterMethod
    public void tearDown()
    {
        driver.quit();
    }*/
}