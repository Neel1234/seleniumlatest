package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.CaseManagerListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class CaseManagerListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	CaseManagerListPage caseManagerListPage;
	public CaseManagerListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		caseManagerListPage = new CaseManagerListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		caseManagerListPage = homePage.clickonCaseManagerListPageLink();
	}
	@Test(priority=1)
	public void CaseManagerListPageTitleTest()
	{
		logger=report.createTest("Verify Case Manager List Page Title");
		String title = CaseManagerListPage.validateCaseManagerListPageTitle();
		Assert.assertEquals(title, "Case Manager List");
	}
	@Test(priority=2)
	public void verifyCaseManagerListLabel()
	{
		logger=report.createTest("verify Case Manager List Page Label");
		Assert.assertTrue(caseManagerListPage.verifyCaseManagerListLabel(),"Case Manager List page not present");
	}
	@Test(priority=3)
	public void selectMultipleselectCaseManager() throws Exception
	{
		logger=report.createTest("verify Multiple select Case Manager checkbox.");
		logger.info("To verify functionality of multiple select checkbox on Case Manager list page.");
		caseManagerListPage.selectMultipleselectCaseManager();
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on Case Manager list page.");
		logger.info("To check functionality of search button by entering type on Case Manager list page.");
		caseManagerListPage.verifySearchBtn("ABC");
	}
	@Test(priority=5)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for Case Manager list page");
		caseManagerListPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		caseManagerListPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		caseManagerListPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=6)
	public void validateAddCaseManagerBtn() throws Exception
	{
		logger=report.createTest("Verify Add Case Manager page");
		logger.info("To check functionality of Add Case Manager page");
		caseManagerListPage.validateAddCaseManagerBtn();
	}
		
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on Case Manager list page");
		caseManagerListPage.validateResetBtn("Ravi", "abc@gmail.com", "2222222222");
	}
	
	@Test(priority=8)
	public void validateEditBtn() throws Exception
	{
		logger=report.createTest("Verify Edit Case Manager page");
		logger.info("To check functionality of edit Case Manager page.Edit Case Manager page should be open and display on screen.");
		caseManagerListPage.validateEditBtn("selenium");
	}
	

	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
