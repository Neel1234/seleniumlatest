package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

import com.myezcare.qa.pages.PatientListPage;

public class PatientListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PatientListPage patientListPage;
	public PatientListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		patientListPage = new PatientListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		patientListPage = homePage.clickOnPatientListLink();
	}
	@Test(priority=1)
	public void verifyPatientListLabel()
	{
		logger=report.createTest("Verify Employee List Page Label");
		Assert.assertTrue(patientListPage.verifyPatientListLabel(), "Patient List page not present");
	}
	/*@AfterMethod
	public void tearDown(){
		driver.quit();
	}*/

}
