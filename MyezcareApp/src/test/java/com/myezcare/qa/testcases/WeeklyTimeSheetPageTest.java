package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DMAS90FormPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.WeeklyTimeSheetPage;

public class WeeklyTimeSheetPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	WeeklyTimeSheetPage weeklyTimeSheetPage;
	public WeeklyTimeSheetPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		weeklyTimeSheetPage = new WeeklyTimeSheetPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		weeklyTimeSheetPage = homePage.clickonWeeklyTimeSheetLink();
		
	}
	@Test(priority=1)
	public void WeeklyTimeSheetPageTitleTest()
	{
		logger=report.createTest("Verify Weekly Time Sheet Page Title");
		String title =WeeklyTimeSheetPage.validateWeeklyTimeSheetPageTitle();
		Assert.assertEquals(title, "DMAS_90 Form");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
