package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.BroadcastNotoficationPage;
import com.myezcare.qa.pages.GroupSMSPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class BroadcastNotoficationPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	BroadcastNotoficationPage broadcastNotoficationPage;
	public BroadcastNotoficationPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		broadcastNotoficationPage = new BroadcastNotoficationPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		broadcastNotoficationPage = homePage.clickonBroadcastNotoficationLink();
		
	}
	@Test(priority=1)
	public void BroadcastNotoficationPageTitleTest()
	{
		logger=report.createTest("Verify Broadcast Notofication Page Title");
		String title =BroadcastNotoficationPage.validateBroadcastNotoficationPageTitle();
		Assert.assertEquals(title, "Broadcast Notifications");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
