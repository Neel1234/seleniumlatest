package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.VisitTaskAddPage;
import com.myezcare.qa.pages.VisitTaskListPage;
import com.myzcare.qa.util.TestUtil;

public class VisitTaskListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	VisitTaskListPage visitTaskListPage;
	String sheetName ="VisitTaskList";
	public VisitTaskListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		visitTaskListPage = new VisitTaskListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		visitTaskListPage = homePage.clickonVisitTaskListPageLink();
	}
	@Test(priority=1)
	public void VisitTaskListPageTitleTest()
	{
		logger=report.createTest("Verify Visit Task List Page Title");
		String title = VisitTaskListPage.validateVisitTaskListPageTitle();
		Assert.assertEquals(title, "Visit Task List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateVisitTaskList(String taskDetail, String category, String taskType, String  visitType, String careType, String serviceCode)throws Exception
	{
		visitTaskListPage.visitTaskList(taskDetail, category, visitType, taskType, careType, serviceCode);
	}

}
