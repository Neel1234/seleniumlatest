package com.myezcare.qa.testcases;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PreferenceAddPage;
import com.myezcare.qa.pages.PreferenceListPage;

public class PreferenceListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PreferenceListPage preferenceListPage;
	public PreferenceListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		preferenceListPage = new PreferenceListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		preferenceListPage = homePage.clickonPreferenceListPageLink();
		
	}
	@Test(priority=1)
	public void PreferenceListPageTitleTest()
	{
		logger=report.createTest("Verify Preference List Page Title");
		String title = PreferenceListPage.validatePreferenceListPageTitle();
		Assert.assertEquals(title, "Preference/Skill List");
	}
	@Test(priority=2)
	public void verifyPreferenceListLabel()
	{
		logger=report.createTest("verify Preference List Page Label");
		Assert.assertTrue(preferenceListPage.verifyPreferenceListLabel(),"Preference List page not present");
	}
	@Test(priority=3)
	public void selectMultipleselectPreference() throws Exception
	{
		logger=report.createTest("verify Multiple select Preference checkbox.");
		logger.info("To verify functionality of multiple select checkbox on Preference list page.");
		preferenceListPage.selectMultipleselectPreference();
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on Preference list page.");
		logger.info("To check functionality of search button by entering type on Preference list page.");
		preferenceListPage.verifySearchBtn("ReferralSource");
	}
	@Test(priority=5)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for Preference list page");
		preferenceListPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		preferenceListPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		preferenceListPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=6)
	public void validateAddPreferenceBtn() throws Exception
	{
		logger=report.createTest("Verify Add Preference page");
		logger.info("To check functionality of Add Preference page");
		preferenceListPage.validateAddPreferenceBtn();
	}
		
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on Preference list page");
		preferenceListPage.validateResetBtn("Ravi");
	}
	@Test(priority=8)
	public void validateEditPreferenceBtn() throws Exception
	{
		logger=report.createTest("Verify Edit Preference page");
		logger.info("To check functionality of edit Preference page.Edit Preference page should be open and display on screen.");
		preferenceListPage.validateEditBtn("selenium");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/	
}