package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

import com.myezcare.qa.pages.RolesAndPermissionPage;
import com.myzcare.qa.util.TestUtil;

public class RolesPermissionPageTest extends TestBase
{

	LoginPage loginPage;
	HomePage homePage;
	RolesAndPermissionPage rolePermissionPage;
	String sheetName ="RolesAndPermission";
	public RolesPermissionPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		rolePermissionPage = new RolesAndPermissionPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		rolePermissionPage = homePage.clickOnRolePermissionLink();
		
	}
	@Test(priority=1)
	public void verifyRolePermiisionLabel()
	{
		logger=report.createTest("Verify Roles and Permiision Label");
		Assert.assertTrue(rolePermissionPage.verifyRolePermiisionLabel(),"Roles & Permission page not present");
	}
	/*@AfterMethod
	public void tearDown(){
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateRoleAndPermission()throws Exception
	{
		
	}
}
