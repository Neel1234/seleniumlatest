package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;



public class EmployeeAddTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	EmployeeAddPage employeeAddPage;
	TestUtil testUtil;
	String sheetName ="Employee Add";
	public EmployeeAddTest()
	{
		super();
	}
	@Test
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeeAddPage = new EmployeeAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeeAddPage = homePage.clickOnEmployeeAddLink();
	}
	
	/*@Test(priority=1)
	public void employeeaddTitleTest()
	{
		logger=report.createTest("Validate Add Employee page Title","Validate Add Employee page title.");
		String title = EmployeeAddPage.validateAddEmployeePageTitle();
		Assert.assertEquals(title, "Add Employee");
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	//@Test(priority=2)
	public void validateCreateNewEmployee(String empID, String firstname, String lastname, String email,String assowith,String address,String city,String zipcode,String state) throws Exception 
	{
	
		//employeeAddPage.createNewContact("EM789", "Test222", "Tset344", "Test@gmail.com", "USA", "Verginia", "89888", "Guam");
		employeeAddPage.createNewContact();
	}


	
	
}
