package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PayorAddPage;
import com.myzcare.qa.util.TestUtil;

public class PayorAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PayorAddPage payorAddPage;
	TestUtil testUtil;
	String sheetName ="PayorAdd";
	public PayorAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		payorAddPage = new PayorAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		payorAddPage = homePage.clickonPayorAddPageLink();
		
	}
	@Test(priority=1)
	public void PayorAddPageTitleTest()
	{
		logger=report.createTest("Verify Payor Add Page Title");
		String title = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(title, "Add Payor");
		
		String id = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(id, "Add Payor");
		
		String name = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(name, "Add Payor");
		
		String sName = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(sName, "Add Payor");
		
		String add = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(add, "Add Payor");
		
		String city = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(city, "Add Payor");
		
		String state = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(state, "Add Payor");
		
		String zCode = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(zCode, "Add Payor");
		
		String invoiceType = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(invoiceType, "Add Payor");
		
		String ageTaxNo = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(ageTaxNo, "Add Payor");
		
		String npiOption = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(npiOption, "Add Payor");
		
		String payerGrp = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(payerGrp, "Add Payor");
		
		String businessLine = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(businessLine, "Add Payor");
		
		String npiNo = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(npiNo, "Add Payor");
		
		String aggregator = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(aggregator, "Add Payor");
		
		String vistedBill = PayorAddPage.validatePayorAddPageTitle();
		Assert.assertEquals(vistedBill, "Add Payor");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewAgency(String id, String name, String sName, String add, String city, String state, 
			String zCode, String invoiceType, String ageTaxNo, String npiOption, String payerGrp, String businessLine, 
			String npiNo, String aggregator, String vistedBill) throws Exception 
	{
		payorAddPage.PayorAdd(id, name, sName, add, city, state, zCode, invoiceType, ageTaxNo, npiOption, payerGrp, businessLine ,
				npiNo, aggregator, vistedBill);
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
