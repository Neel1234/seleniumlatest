package com.myezcare.qa.testcases;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.FacilityAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class FacilityAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	FacilityAddPage facilityAddPage;
	String sheetName ="FacilityAdd";
	public FacilityAddPageTest()
	{
		super();
	}
	@Test
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		facilityAddPage = new FacilityAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		facilityAddPage = homePage.clickonFacilityAddLink();
		
	}
	/*@Test(priority=1)
	public void FacilityAddPageTitleTest()
	{
		logger=report.createTest("Verify Facility Add Page Title");
		String title = FacilityAddPage.validateFacilityAddPageTitle();
		Assert.assertEquals(title, "Add Facility House");
	}*/
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateFacilityAdd(String facilityName, String address, String city, String state, String zip, String phone, String cell,
			String region, String country, String gsa, String equipment, String houseCapacity, String privateRoom, 
			String siteType, String providerType, String licensure, String licensureRenewalDate, String firePermitDate,
			String NPI, String AHCCCSID, String EIN)throws Exception
	{
		facilityAddPage.facilityAdd(facilityName, address, city, zip, phone, cell, country, gsa, equipment, houseCapacity, privateRoom, siteType, providerType, licensure, licensureRenewalDate, firePermitDate, NPI, AHCCCSID, EIN);
	}
}
