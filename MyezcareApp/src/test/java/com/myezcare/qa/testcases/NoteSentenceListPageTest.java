package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.NoteSentenceAddPage;
import com.myezcare.qa.pages.NoteSentenceListPage;
import com.myzcare.qa.util.TestUtil;

public class NoteSentenceListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	NoteSentenceListPage noteSentenceListPage;
	String sheetName ="NoteSentenceList";
	public NoteSentenceListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		noteSentenceListPage = new NoteSentenceListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		noteSentenceListPage = homePage.clickonNoteSentenceListPageLink();
	}
	@Test(priority=1)
	public void NoteSentenceListPageTitleTest()
	{
		logger=report.createTest("Verify Note Sentence List Page Title");
		String title = NoteSentenceListPage.validateNoteSentenceListPageTitle();
		Assert.assertEquals(title, "Note Sentence List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateNoteSentenceList(String title, String details, String active)throws Exception
	{
		noteSentenceListPage.noteSentenceList(title, details, active);
	}
}
