package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ServiceCodeListPage;
import com.myezcare.qa.pages.VisitTaskAddPage;
import com.myzcare.qa.util.TestUtil;

public class VisitTaskAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	VisitTaskAddPage visitTaskAddPage;
	String sheetName ="VisitTaskAdd";
	public VisitTaskAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		visitTaskAddPage = new VisitTaskAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		visitTaskAddPage = homePage.clickonVisitTaskAddPageLink();
	}
	@Test(priority=1)
	public void VisitTaskAddPageTitleTest()
	{
		logger=report.createTest("Verify Visit Task Add Page Title");
		String title = VisitTaskAddPage.validateVisitTaskAddPageTitle();
		Assert.assertEquals(title, "Add Visit Task");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateVisitTaskAdd(String taskCode, String category, String subCategory, String taskDetail, String isRqd, String isDft)throws Exception
	{
		visitTaskAddPage.visitTaskAction(taskCode, category, subCategory, taskDetail, isRqd, isDft);
	}
}
