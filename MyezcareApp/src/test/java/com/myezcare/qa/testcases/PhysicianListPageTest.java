package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PhysicianAddPage;
import com.myezcare.qa.pages.PhysicianListPage;
import com.myzcare.qa.util.TestUtil;

public class PhysicianListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PhysicianListPage physicianListPage;
	String sheetName ="PhysicianList";
	public PhysicianListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		physicianListPage = new PhysicianListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		physicianListPage = homePage.clickonPhysicianListPageLink();
		
	}
	@Test(priority=1)
	public void PhysicianListPageTitleTest()
	{
		logger=report.createTest("Verify Physician List Page Title");
		String title = PhysicianListPage.validatePhysicianListPageTitle();
		Assert.assertEquals(title, "Physician List");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validatePhysicianList(String physicianName, String specility, String npi, String address, String email)throws Exception
	{
		physicianListPage.physicianListPage(physicianName, specility, npi, address, email);
	}
}
