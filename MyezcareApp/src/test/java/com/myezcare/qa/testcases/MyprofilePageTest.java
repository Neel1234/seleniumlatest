package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.MyprofilePage;

public class MyprofilePageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	MyprofilePage myprofilePage;

	public MyprofilePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		System.out.println("11111111111111111111");
		initialization();
		loginPage=new LoginPage();
		myprofilePage = new MyprofilePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		myprofilePage = homePage.clickOnAdminMyprofileLink();
		
	}
	
	@Test(priority=1)
	public void verifyMyprofilePageLabel()
	{
		logger=report.createTest("verify My profile Page Label");
		Assert.assertTrue(myprofilePage.verifyEditProfileLabel(),"My profile page is not present");
		logger.info("Page verfied");
		logger.pass("Test Pass");
		
	}
	@Test(priority=2)
	public void validateCancelBtn() throws InterruptedException
	{
		logger=report.createTest("Validate cancel button","Validate the cancel button functionality on My profile Page.");
		myprofilePage.validateCancelBtn();
		
	}
	/*@AfterMethod
	public void tearDown(){
		
		driver.quit();
	}*/

}
