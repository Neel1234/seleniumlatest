package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

import com.myezcare.qa.pages.ReceivedMessagePage;

public class ReceivedMessagePageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	ReceivedMessagePage receivedMessagePage;
	public ReceivedMessagePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		receivedMessagePage = new ReceivedMessagePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		receivedMessagePage = homePage.clickonReceivedMessageLink();
		
	}
	@Test(priority=1)
	public void ReceivedMessagePageTitleTest()
	{
		logger=report.createTest("Verify Received Message Page Title");
		String title =ReceivedMessagePage.validateReceivedMessagePageTitle(); 
		Assert.assertEquals(title, "Received Messages");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
