package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.CaseManagerAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myzcare.qa.util.TestUtil;

public class CaseManagerAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	CaseManagerAddPage caseManagerAddPage;
	TestUtil testUtil;
	String sheetName ="CaseManagerAdd";
	public CaseManagerAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		caseManagerAddPage = new CaseManagerAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		caseManagerAddPage = homePage.clickonCaseManagerAddPageLink();
	}
	@Test(priority=1)
	public void CaseManagerAddPageTitleTest()
	{
		logger=report.createTest("Verify Case Manager Add Page Title");
		String title = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(title, "Add Case Manager");
		
		String agency = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(agency, "Add Case Manager");
		
		String fName = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(fName, "Add Case Manager");
		
		String lName = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(lName, "Add Case Manager");
		
		String email = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(email, "Add Case Manager");
		
		String extension = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(extension, "Add Case Manager");
		
		String phone = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(phone, "Add Case Manager");
		
		String cell = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(cell, "Add Case Manager");
		
		String fax = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(fax, "Add Case Manager");
		
		String notes = CaseManagerAddPage.validateCaseManagerAddPageTitle();
		Assert.assertEquals(notes, "Add Case Manager");
	}
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateCreateNewCaseManager(String agency, String fName, String lName, String email, String extension, 
			String phone, String cell, String fax, String notes) throws Exception 
	{
		caseManagerAddPage.CaseManagerAdd(agency, fName, lName, email, extension, phone, cell, fax, notes);
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
