package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PayorListPage;

public class PayorListPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	PayorListPage payorListPage;
	public PayorListPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		payorListPage = new PayorListPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		payorListPage = homePage.clickonPayorListPageLink();
		
	}
	@Test(priority=1)
	public void PayorListPageTitleTest()
	{
		logger=report.createTest("Verify Payor List Page Title");
		String title = PayorListPage.validatePayorListPageTitle();
		Assert.assertEquals(title, "Payor List");
	}
	@Test(priority=2)
	public void verifyPayorListLabel()
	{
		logger=report.createTest("verify Payor List Page Label");
		Assert.assertTrue(payorListPage.verifyPayorListLabel(),"Payor List page not present");
	}
	@Test(priority=3)
	public void selectMultipleselectPayor() throws Exception
	{
		logger=report.createTest("verify Multiple select Payor checkbox.");
		logger.info("To verify functionality of multiple select checkbox on Payor list page.");
		payorListPage.selectMultipleselectPayor();
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("Verify search button functionality on Payor list page.");
		logger.info("To check functionality of search button by entering type on Payor list page.");
		payorListPage.verifySearchBtn("ReferralSource");
	}
	@Test(priority=5)
	public void validateDeleteActiveBtn() throws Exception
	{
		logger=report.createTest("Check Active and Deleted record.");
		logger.info("To check Active and deleted record for Payor list page");
		payorListPage.validateDeleteActiveRecord("Active");
		Thread.sleep(3000);
		payorListPage.validateDeleteActiveRecord("Deleted");
		Thread.sleep(3000);
		payorListPage.validateDeleteActiveRecord("All Records");
	}
	@Test(priority=6)
	public void validateAddPayorBtn() throws Exception
	{
		logger=report.createTest("Verify Add Payor page");
		logger.info("To check functionality of Add Payor page");
		payorListPage.validateAddPayorBtn();
	}
		
	@Test(priority=7)
	public void validateresetBtn() throws Exception
	{
		logger=report.createTest("Validate Reset button functionality");
		logger.info("To check functionality of reset button on Payor list page");
		payorListPage.validateResetBtn("Ravi Sharma", "rsharma", "abd hgjgj jfj", "2222222222", "852147");
	}
	
	@Test(priority=8)
	public void validateEditPayorBtn() throws Exception
	{
		logger=report.createTest("Verify Edit Payor page");
		logger.info("To check functionality of edit Payor page.Edit Payor page should be open and display on screen.");
		payorListPage.validateEditBtn("selenium");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
