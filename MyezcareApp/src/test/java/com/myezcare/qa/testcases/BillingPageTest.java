package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.AgencyListPage;
import com.myezcare.qa.pages.BillingPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.OrganizationSettingPage;

public class BillingPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	BillingPage billingPage;

	public BillingPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		billingPage = new BillingPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		billingPage = homePage.clickOnBillingLink();
	}
	@Test(priority=1)
	public void verifyInvoiceListTitle()
	{
		logger=report.createTest("Verify Invoice List Title");
		String title = BillingPage.validateInvoiceListPageTitle();
		Assert.assertEquals(title, "nvoice List");
	}
	
}
