package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.EmployeeCalendarPage;
import com.myezcare.qa.pages.EmployeeListPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class EmployeeCalendarPageTest extends TestBase
{

	LoginPage loginPage;
	HomePage homePage;
	EmployeeCalendarPage employeeCalendarPage;
	public EmployeeCalendarPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeeCalendarPage = new EmployeeCalendarPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeeCalendarPage = homePage.clickOnEmployeeCalendarLink();
		
	}
	@Test(priority=1)
	public void employeeCalendarTitleTest()
	{
		logger=report.createTest("Validate Employee Calendar page Title","Validate Employee Calendar page title.");
		String title = EmployeeCalendarPage.validateEmployeeCalendarTitle();
		Assert.assertEquals(title, "Employee Calendar");
	}
	@Test(priority=2)
	public void validatesearchBtn() throws Exception
	{
		logger=report.createTest("Validate search button functionality on employee calendar page.");
		logger.info("To check functionality of search button by entering employee name.");
		employeeCalendarPage.validateSearchBtn("Selenium");
	}
	
	@Test(priority=3)
	public void validateDayBtn() 
	{
		logger=report.createTest("Validate Day button functionality on employee calendar page.");
		logger.info("To check day button functionality on employee calendar page.");
		employeeCalendarPage.validateDayBtn();
	}
	
	@Test(priority=4)
	public void validateWeekBtn() 
	{
		logger=report.createTest("Validate week button functionality on employee calendar page.");
		logger.info("To check week button functionality on employee calendar page.");
		employeeCalendarPage.validateWeekBtn();
	}
	
	@Test(priority=5)
	public void validateMonthBtn() 
	{
		logger=report.createTest("Validate month button functionality on employee calendar page.");
		logger.info("To check month button functionality on employee calendar page");
		employeeCalendarPage.validateMonthBtn();
	}
	
	
	
/*	@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
