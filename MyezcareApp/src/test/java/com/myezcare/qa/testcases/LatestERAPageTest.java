package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LatestERAPage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.Upload835Page;

public class LatestERAPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	LatestERAPage latestERAPage;
	public LatestERAPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		latestERAPage = new LatestERAPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		latestERAPage = homePage.clickonLatestERALink();
		
	}
	@Test(priority=1)
	public void LatestERAPageTitleTest()
	{
		logger=report.createTest("Verify Latest ERA Page Title");
		String title = LatestERAPage.validateLatestERAPageTitle();
		Assert.assertEquals(title, "Latest ERA");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
