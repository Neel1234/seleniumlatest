package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.PreferenceListPage;
import com.myezcare.qa.pages.ServiceCodeAddPage;
import com.myzcare.qa.util.TestUtil;

import net.bytebuddy.implementation.bytecode.Throw;

public class ServiceCodeAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	ServiceCodeAddPage serviceCodeAddPage;
	String sheetName ="ServiceCodeAdd";
	public ServiceCodeAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		serviceCodeAddPage = new ServiceCodeAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		serviceCodeAddPage = homePage.clickonServiceCodeAddPageLink();
		
	}
	@Test(priority=1)
	public void ServiceCodeAddPageTitleTest()
	{
		logger=report.createTest("Verify Service Code Add Page Title");
		String title = ServiceCodeAddPage.validateServiceCodeAddPageTitle();
		Assert.assertEquals(title, "Add Service Code Details");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateServiceCodeAdd(String serviceCode, String modifier, String serviceName, String description, String isBillable)throws Exception
	{
		serviceCodeAddPage.serviceCodeAdd(serviceCode, modifier, modifier, serviceName, description);
	}
}
