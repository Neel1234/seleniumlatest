package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.Reconcile835Page;
import com.myezcare.qa.pages.Upload835Page;

public class Reconcile835PageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	Reconcile835Page reconcile835Page;
	public Reconcile835PageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		reconcile835Page = new Reconcile835Page();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		reconcile835Page = homePage.clickonReconcile835Link();
		
	}
	@Test(priority=1)
	public void Reconcile835PageTitleTest()
	{
		logger=report.createTest("Verify Reconcile 835 Page Title");
		String title = Reconcile835Page.validateReconcile835PageTitle();
		Assert.assertEquals(title, "Reconcile 835 / EOB");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
}
