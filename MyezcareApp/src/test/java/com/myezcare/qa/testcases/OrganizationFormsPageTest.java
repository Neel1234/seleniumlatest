package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.DropdownEditorPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.OrganizationFormsPage;
import com.myzcare.qa.util.TestUtil;

public class OrganizationFormsPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	OrganizationFormsPage organizationFormsPage;
	String sheetName = "OrganizationForms";
	public OrganizationFormsPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		organizationFormsPage = new OrganizationFormsPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		organizationFormsPage = homePage.clickonOrganiztionFormsLink();
		
	}
	@Test(priority=1)
	public void OrganizationFormsPageTitleTest()
	{
		logger=report.createTest("Verify Organization Forms Page Title");
		String title = OrganizationFormsPage.validateOrganizationFormsPageTitle();
		Assert.assertEquals(title, "Organization Forms");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/
	@DataProvider
	public Object[][] getMyzcareTestData(){
		Object data[][] = TestUtil.getTestData(sheetName);
		return data;
	}
	@Test(priority=2, dataProvider="getMyzcareTestData")
	public void validateOrganizationForm(String aFormNumber, String aFormName, String sFormNumber, String sFormName)throws Exception
	{
		organizationFormsPage.orgForm(aFormNumber, aFormName, sFormNumber, sFormName);
	}
}
