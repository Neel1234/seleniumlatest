package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

import com.myezcare.qa.pages.SentMessagePage;

public class SentMessagePageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	SentMessagePage sentMessagePage;
	public SentMessagePageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		sentMessagePage = new SentMessagePage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		sentMessagePage = homePage.clickonSentMessageLink();
		
	}
	@Test(priority=1)
	public void SentMessagePageTitleTest()
	{
		logger=report.createTest("Verify Sent Message Page Title");
		String title =SentMessagePage.validateSentMessagePageTitle(); 
		Assert.assertEquals(title, "Sent Messages");
	}
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}*/

}
