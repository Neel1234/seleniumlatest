package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;

import com.myezcare.qa.pages.EmployeePTOPage;

import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class EmployeePTOPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	EmployeePTOPage employeePtoPage;
	public EmployeePTOPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		employeePtoPage = new EmployeePTOPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		employeePtoPage = homePage.clickOnEmployeePTOLink();
		
	}
	@Test(priority=1)
	public void verifyEmployeePTOTitle()
	{
		logger=report.createTest("verify Employee PTO Title");
		String title = EmployeePTOPage.validateEmployeePTOpageTitle();
		Assert.assertEquals(title, "Employee PTO List");
	}
	@Test(priority=2)
	public void validateEmployeePTObutton() throws Exception
	{
		logger=report.createTest("validate Employee PTO Button");
		logger.info("Verify Employee PTO Button display on screen");
		employeePtoPage.validatePTOBtn();
	}
	@Test(priority=3)
	public void validateMultiSelectBtn() throws Exception
	{
		logger=report.createTest("validate Multiselect button");
		logger.info("To check multiselect button on employee PTO page functioning properly or not.");
		employeePtoPage.validatemultiselectionBtn();
		
	}
	@Test(priority=4)
	public void validateSearchBtn() throws Exception
	{
		logger=report.createTest("validate Search button");
		logger.info("To check search button functionality on empoyee PTO page");
		employeePtoPage.validatesearchBtn(4);
		
	}
	@Test(priority=5)
	public void validateResetBtn() throws Exception
	{
		logger=report.createTest("validate Reset button");
		logger.info("To check reste button functionality on empoyee PTO page");
		employeePtoPage.validateresetBtn(3);
		
	}
}
