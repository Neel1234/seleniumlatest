package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;

public class LoginPageTest extends TestBase 
{
	LoginPage loginPage;
	HomePage homePage;
	public LoginPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp()
	{
		initialization();
		loginPage=new LoginPage();
	}
	
	@Test(priority=1)
	public void myzcareLogoImageTest()
	{
		logger=report.createTest("Validate Myzcare Logo on login page.");
		boolean flag=loginPage.validatemyzcarelogo();
		Assert.assertTrue(flag);
	}
	@Test(priority=2)
	public void loginTest()
	{
		logger=report.createTest("Login Done successfully");
		homePage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
	}
	
	/*@AfterMethod
	public void tearDown()
	{
		
		driver.quit();
	}*/
}
