package com.myezcare.qa.testcases;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.myezcare.qa.base.TestBase;
import com.myezcare.qa.pages.AgencyAddPage;
import com.myezcare.qa.pages.HomePage;
import com.myezcare.qa.pages.LoginPage;
import com.myezcare.qa.pages.ManageClaimsPage;

public class AgencyAddPageTest extends TestBase
{
	LoginPage loginPage;
	HomePage homePage;
	AgencyAddPage agencyAddPage;
	public AgencyAddPageTest()
	{
		super();
	}
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialization();
		loginPage=new LoginPage();
		agencyAddPage = new AgencyAddPage();
		homePage = loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		agencyAddPage = homePage.clickonAgencyAddLink();
		
	}
	/*@Test(priority=1)
	public void AgencyAddPageTitleTest()
	{
		logger=report.createTest("Verify Agency Add Title");
		String title = AgencyAddPage.validateAgencyAddPageTitle();
		Assert.assertEquals(title, "Add Agency");
	}*/
	/*@AfterMethod
	public void tearDown()
	{
		driver.quit();
	}
*/
}
